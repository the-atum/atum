# Atum

Generates programming language code and database scripts by reading meta-data from a source database into a language of choice and target database.
Check Command-Line based [Build](#build-steps) and [Run](#run-steps) or [Run with args](#run-with-arguments-steps) steps below, or visit [Output format](#output-format) below.

## Build Steps

1. Go to Atum `repo` dir.
    - `cd ~/Atum/`
    ---
2. Remove any `bin` dir in `repo`.
    - `rm -rf bin/`
    ---
3. Create a new `bin` dir in `repo`.
    - `mkdir bin`
    ---
4. Compile java source file, using fully qualified name appended via find command(linux).
    - `javac -d bin $(find ./src/ -name "*.java")`
    ---
5. Copy manifest text file from `repo` to `bin/` dir.
    - `cp AtumManifest.txt bin/AtumManifest.txt`
    ---
6. Copy property xml/json file from `repo` to `bin/` dir.
    - `cp AtumClassesPropertyFile.xml bin/AtumClassesPropertyFile.xml`.
    ---
7. Go into `bin/` dir
    - `cd bin`
    ---
8. Create a jar-file using manifest file and appending fully qualified name for class files via find command(linux).
    - `jar cfm Atum.jar AtumManifest.txt $(find . -name "*.class")`
    ---

## Run Steps

1. Go to Atum repo dir.
    - `cd ~/Atum/bin`
    ---
2. Run `Atum.jar` file and enter input if asked.
    - `java -jar Atum.jar`

## Run With Arguments Steps

1. Alternatively, can be run by passing params for sourceDb(currently `mysql`), targetDb(currently `mysql, sqllite`) and targetLanguage(currently `csharp, java`), along with url, username, password and specific-db-name for sourceDb.
    - `java -jar Atum.jar mysql mssql csharp localhost:3306 user password mydb`
    ---

## Output Format

1. Generated output is stored in current(present) working dir under following format(subject to change) (run using example from [above arguments method](#run-with-arguments-steps)).
    - `AtumOutputDir_T1_D2020-10-20_21-30-07/`
      - `mydb/`
        - `CSharp/`
            - `DAL/`
              - `MytableDAL.cs`
            - `DataObjects/`
              - `MytableDO.cs`
        - `MySql/`
            - `mytable/`
                - `mytable_SP_CREATE.sql`
                - `mytable_SP_DELETE.sql`
                - `mytable_SP_READALL.sql`
                - `mytable_SP_READBYID.sql`
                - `mytable_SP_UPDATE.sql`
                - `mytable_TABLE_DROP_AND_CREATE.sql`
    ---
