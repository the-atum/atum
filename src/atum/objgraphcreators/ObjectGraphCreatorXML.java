
/** *****************************************************************************
 * Author:        Indresh Rawat (IVR)
 * Email:         indreshrawat.gitlab@gmail.com
 *
 * Project:       Atum/ObjectGraphCreators
 * File:          ObjectGraphCreatorXML.java
 *
 * Description:
 * 		ObjectGraphCreatorXML reads a Map which contains the information of
 * 		databases present in user machine and parse the information
 * 		into XML format.
 *
 * Purpose:
 * 		It will be used to generate code of specified database.
 *
 * Revision History:
 *      2019-Aug-13 (IVNR): File created.
 *      2019-Aug-15 (IVNR): Updated createObjectGraphFile method.
 *      2019-Aug-18 (IVNR): Updated createObjectGraphFile
 * 							and getMapDocument method to suit recursion
 * 							of updated HashMap.
 * 		2019-Aug-24 (IVNR): Updated getMapDocument method to add a custom
 * 							node which will be used by ObjectGraphReader
 * 							class.
 * 		2019-Nov-07 (IVNR): Updated createObjectGraphFile method to receive
 * 							file path from it's formal parameters.
 * 		2019-Nov-08 (IVNR): Updated createObjectGraphFile method where the
 * 							rootNode name is changed.
 *      2019-Dec-15 (IVNR): Updated getMapDocument() method.
 *      2020-May-25 (PBMV): Standard code auto-formatting using RedHat's Java
 *                          Code-Formatter extension added with the Java
 *                          Language pack.
 *      2020-May-25 (PBMV): Updated by implementing newly created
 *                          IObjectGraphCreator and renamed this file to
 *                          ObjectGraphCreatorXML.
 *      2020-May-25 (PBMV): Updated as per signature change in interface to
 *                          return the name of the generated file as well.
 *      2020-May-30 (PBMV): Moved to atum.objgraphcreators package.
 *      2020-May-30 (PBMV): Fixed import errors due to package restructuring.
 ****************************************************************************** */

package atum.objgraphcreators;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import atum.interfaces.IObjectGraphCreator;

/**
 * ObjectGraphCreatorXML reads a Map which contains the information of databases
 * present in user machine and parse the information into XML format.
 */
@SuppressWarnings("unchecked")
public class ObjectGraphCreatorXML implements IObjectGraphCreator {

    /**
     * This method is used to create a XML document based on the map. Here, document
     * is a DOM Tree structure created based on nodes where nodes is the key present
     * in the map.
     *
     * @param rawDbReaderMap      is a map which contains databases information.
     * @throws Exception
     */
    @Override
    public String createObjectGraphFile(HashMap<?, ?> rawDbReaderMap) throws Exception {

        // Name of the root node.
        String rootNodeName = "schemaMetaData";

        // Initializing it with the DocumentBuilderFactory instance.
        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

        // Initializing it with the DocumentBuilder where the it will be used to create
        // a new document in DOM Tree.
        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

        // Initializing it with the new document which is in a DOM Tree structure.
        Document document = documentBuilder.newDocument();

        // Creating node of the document which specified node name.
        Element rootNode = document.createElement(rootNodeName);

        // Adding the node to the document.
        document.appendChild(rootNode);

        // Calling the below method to iterate through the schemaMap recursively and
        // appending nodes to the document based on the schemaMap to form a DOM Tree.
        document = getMapDocument(rootNode, document, rawDbReaderMap);

        // Initializing it with the TransformerFactory instance.
        TransformerFactory transformerFactory = TransformerFactory.newInstance();

        // Initializing it with a transformer which will be able to transform
        // DOM Tree structure to particular result.
        Transformer transformer = transformerFactory.newTransformer();

        // The document will be in a DOM Tree structure.
        DOMSource domSource = new DOMSource(document);

        // Defining name of the output directory where the generated object graph
        // document file will be created.
        String objGraphDirName = "AtumObjectGraph/";

        // Creating the output dir in the file-system.
        File objGraphDir = new File(objGraphDirName);
        objGraphDir.mkdirs();

        // Setting up the name of the document file as per current date time in long
        // ticks along with current thread's id in an attempt to keep it unique.
        String objectGraphFileName = "AtumObjectGraph" + "_T" + Thread.currentThread().getId() + "_D"
                + new Date().getTime() + ".xml";

        // Setting up the fullName of the output file.
        String fullObjGraphFilePath = objGraphDirName + objectGraphFileName;

        // Creating a new file based on the path created above.
        File objGraphFile = new File(fullObjGraphFilePath);

        // The result of transformation will be here.
        StreamResult streamResult = new StreamResult(objGraphFile);

        // Transforming the DOM Tree to the specified result.
        transformer.transform(domSource, streamResult);

        // Finally returning the name of the generated object graph file to the caller.
        return fullObjGraphFilePath;
    }

    /**
     * This method is used to recursively traverse the map where the key of the map
     * will be considered as nodes and would be appended to the document. If the map
     * value is type of String the value of map will be appended as text to their
     * respective keys.
     *
     * @param currentNode     is the current node of the document.
     *
     * @param currentDocument is the current document where the nodes to be added.
     *
     * @param currentMap      is the map to be traversed.
     *
     * @return the document.
     */
    private Document getMapDocument(Element currentNode, Document currentDocument, HashMap<?, ?> currentMap) {

        // Setting the iterator for the map which is a entry type of String, Object.
        Iterator<?> currentMapIterator = currentMap.entrySet().iterator();

        // Checking if elements is present and iterate through the map.
        while (currentMapIterator.hasNext()) {

            // The current element of the map.
            Entry<String, Object> currentEntry = (Entry<String, Object>) currentMapIterator.next();
            try {
                // Creating a node based on the key of the entry.
                Element childNodes = currentDocument.createElement(currentEntry.getKey());

                // Checking if the entry value is type of string and if the value is null.
                if (currentEntry.getValue() instanceof String || null == currentEntry.getValue()) {
                    // Storing the custom node name
                    String customNodeName = "valueNode";

                    // Creating the custom node which will store the text node.
                    Element customNode = currentDocument.createElement(customNodeName);

                    // Defining the text variable.
                    Text nodeValue;

                    // Checking if the value is null.
                    if (null == currentEntry.getValue())
                        // Creating a text element with empty string.
                        nodeValue = currentDocument.createTextNode("");
                    else
                        // Creating a text element based on the value of map.
                        nodeValue = currentDocument.createTextNode((String) currentEntry.getValue());

                    // Appending the text node to the custom node.
                    customNode.appendChild(nodeValue);

                    // Appending it to the particular nodes.
                    childNodes.appendChild(customNode);
                }

                // Appending the child nodes to their current node.
                currentNode.appendChild(childNodes);

                // Checking if the entry value is type of HashMap.
                if (currentEntry.getValue() instanceof HashMap<?, ?>)
                    // Calling the method recursively to iterate through the map.
                    currentDocument = getMapDocument(childNodes, currentDocument,
                            (HashMap<String, Object>) currentEntry.getValue());
            } catch (Exception exObj) {
                System.err.println(exObj.getMessage());
                System.err.println(currentEntry.getKey());
                System.err.println(currentEntry.getValue());
                exObj.printStackTrace();
            }
        }

        // Returning the current document.
        return currentDocument;
    }

}
