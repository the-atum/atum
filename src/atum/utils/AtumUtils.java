/*******************************************************************************
 * Author:        Pawan Vyas (PBMV)
 * Email:         pawan.vyas.gitlab@gmail.com
 * 
 * Project:       Atum/Utils
 * File:          AtumUtils.java
 * 
 * Description:
 *      This class contains various commonly used utility functions across Atum.
 * 
 * Purpose:
 *      To serve as a simple Utility class for commonly used extension methods
 *      like capitalizing a string, etc.
 * 
 * Revision History:
 *      2019-Sep-27 (PBMV): File Created.
 *      2020-May-24 (PBMV): Standard code auto-formatting using RedHat's Java
 *                          Code-Formatter extension added with the Java
 *                          Language pack.
 *      2020-May-30 (PBMV): Moved to atum.utils package.
 ****************************************************************************** */

package atum.utils;

/**
 * This class contains various commonly used utility functions across Atum.
 */
public class AtumUtils {

    /**
     * Private ctor so that this class can't be instantiated.
     */
    private AtumUtils() {
    }

    /**
     * Function which capitalizes the passed text.
     * 
     * @param text is the text which is passed that needs to be capitalized.
     * @return the capitalized text.
     */
    public static String properCase(String text) {

        // Check if passed string is null, and if not null is it empty or blank,
        // then returning the text back as null, empty or white-space string can't
        // be capitalized.
        if (null == text || text.isEmpty())
            return text;

        // From the passed text, take the first character and make it uppercase.
        // then from the 2nd character onwards make all characters till the end as
        // lowercase and return the string.
        return text.substring(0, 1).toUpperCase() + text.substring(1).toLowerCase();
    }
}