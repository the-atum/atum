
/*******************************************************************************
 * Author:        Pawan Vyas (PBMV)
 * Email:         pawan.vyas.gitlab@gmail.com
 * 
 * Project:       Atum/Utils
 * File:          AtumMapRetriever.java
 * 
 * Description:
 *      Utility class which provides functionality for getting nested maps
 *      from the generated AtumMap.
 * 
 * Purpose:
 *      This utility is used by factory and abstract factory classes to get 
 *      proper maps from AtumMap using their specific component and/or db-type
 *      keys.
 * 
 * Revision History:
 *      2019-Sep-27 (PBMV): File Created.
 *      2019-Nov-08 (IVNR): Update the xml property file path.
 *      2020-May-24 (PBMV): Standard code auto-formatting using RedHat's Java
 *                          Code-Formatter extension added with the Java
 *                          Language pack.
 *      2020-May-30 (PBMV): Added method GetDefaultObjGraphDocFormat.
 *      2020-May-30 (PBMV): Moved to atum.utils package.
 * ***************************************************************************** */

package atum.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

/**
 * Utility class which provides functionality for getting nested maps from the
 * generated AtumMap. This utility is used by factory and abstract factory
 * classes to get proper maps from AtumMap using their specific component and/or
 * db-type keys.
 */
@SuppressWarnings("unchecked")
public class AtumMapRetriever {

    // Constant to hold the path of the XML Property file.
    private static final String XML_PROPERTY_FILE_PATH = "AtumClassesPropertyFile.xml";

    // Constant to hold the default key name for "atum" key in the PropertyFile.
    private static final String ATUM_KEY = "atum";

    // Constant to hold the default key name in atum-map for getting the
    private static final String DEFAULT_OBJGRAPH_DOCFORMAT = "defaultobjgraphdocformat";

    // Instantiating xmlMap Map which will convert all DOM elements from xml file to
    // map.
    private static HashMap<String, Object> xmlMap;

    // Instantiating atumMap which will select the map which has atum key in the
    // xmlMap.
    private static HashMap<String, Object> atumMap;

    // Defining local which will be used to store the retrieved name of the default
    // doc format.
    private static String defaultDocFormat = null;

    /**
     * Static constructor to initialize the xmlMap and atumMap.
     */
    static {

        // Populating XML map from the PropertyFile at the current Location.
        xmlMap = XmlReader.GetPropertyMap(XML_PROPERTY_FILE_PATH);

        // Initializing atum map as new empty HashMap.
        atumMap = new HashMap<String, Object>();

        // Checking if ATUM_KEY exists or not in the populated xmlMap.
        boolean atumKeyExists = xmlMap.containsKey(ATUM_KEY);

        // If atumKey exists, it means we will cast the value of ATUM_KEY
        // to a HashMap<String, Object> type and reassign it to atumMap.
        if (atumKeyExists)
            atumMap = (HashMap<String, Object>) xmlMap.get(ATUM_KEY);
    }

    /**
     * Function which returns the map of the requested component(if it exists) in
     * the private atumMap.
     * 
     * @param componentKey is the name of the component whose map is to be
     *                     retrieved.
     * @return a HashMap[String, Object] type if the component exists, else a new
     *         empty HashMap[String, Object] is returned.
     */
    public static HashMap<String, Object> GetUncastedComponentMap(String componentKey) {

        // Check if the passed component exists in the atumMap.
        boolean componentKeyExists = atumMap.containsKey(componentKey);

        // If componentKey does not exists, then simply return a new empty HashMap.
        if (!componentKeyExists)
            return new HashMap<String, Object>();
        else
            // Otherwise return the casted value of the componentKey.
            return (HashMap<String, Object>) atumMap.get(componentKey);
    }

    /**
     * Function that returns a properly type-casted map of the passed componentKey.
     * 
     * @param componentKey is the name of the component whose casted map is to be
     *                     retrieved.
     * @return a HashMap[String, String] type if the passed componentKey exists,
     *         otherwise an empty HashMap[String, String] type is returned.
     */
    public static HashMap<String, String> GetCastedComponentMap(String componentKey) {

        // Check if the passed component exists in the atumMap.
        boolean componentKeyExists = atumMap.containsKey(componentKey);

        // Defining a new HashMap which will hold the properly type-casted Entries from
        // the atumMap for the passed componentKey.
        HashMap<String, String> castedMap = new HashMap<String, String>();

        // If componentKey does not exists, then simply return castedMap, which will
        // be an Empty HashMap.
        if (!componentKeyExists)
            return castedMap;
        else {
            // If componentKey existed, then getting its value from atumMap.
            HashMap<String, Object> uncastedComponentMap = (HashMap<String, Object>) atumMap.get(componentKey);

            // Getting an Iterator of the uncastedComponentMap which will be used
            // to iterate through all the Entries in the uncastedComponentMap.
            Iterator<Entry<String, Object>> uncastedComponentMapIterator = uncastedComponentMap.entrySet().iterator();

            // Iterating through all the Entries until all of them are iterated, i.e.
            // iterator.HasNext() returns false.
            while (uncastedComponentMapIterator.hasNext()) {

                // For as long as the Iterator has Entries left,
                // getting a reference to the currentEntry.
                Entry<String, Object> currentEntry = uncastedComponentMapIterator.next();

                // Checking if the value of the currentEntry is of type String, if it is,
                // then we will add it to the castedMap and move to the next Entry.
                if (currentEntry.getValue() instanceof String)
                    castedMap.put(currentEntry.getKey(), (String) currentEntry.getValue());
            }

            // Finally once all the Entries are iterated, we will return the castedMap.
            return castedMap;
        }
    }

    /**
     * Function which returns a type-casted map of the passed component's dbtypeKey
     * from the AtumMap.
     * 
     * @param componentKey is the name of the component whose dbtypeKey's map is to
     *                     be retrieved.
     * @param dbtypeKey    is the name of the database type that belongs to the
     *                     component.
     * @return a HashMap[String, String] type if the passed exists, otherwise an
     *         empty HashMap[String, String] type is returned.
     */
    public static HashMap<String, String> GetCastedComponentDbTypeMap(String componentKey, String dbtypeKey) {

        // Getting uncasted component map for the passed componentKey.
        HashMap<String, Object> uncastedComponentMap = AtumMapRetriever.GetUncastedComponentMap(componentKey);

        // Checking if uncastedComponentMap is empty.
        // Or if it is not empty, then the passed dbtypeKey doesn't exist in the
        // uncastedComponentMap.
        // If either are true, we will simply return a new HashMap<String, String> type.
        if (0 >= uncastedComponentMap.size() || !uncastedComponentMap.containsKey(dbtypeKey))
            return new HashMap<String, String>();

        // If uncastedComponentMap was not empty, and also contained a key with the
        // passed dbtypeKey,
        // then defining a new uncastedDbTypeMap, which will hold the reference
        // to the value of dbtypeKey from the uncastedComponentMap.
        HashMap<String, Object> uncastedDbTypeMap = (HashMap<String, Object>) uncastedComponentMap.get(dbtypeKey);

        // Defining an Iterator for the uncastedDbTypeMap which will be used to iterate
        // through all
        // the Entries in the uncastedDbTypeMap.
        Iterator<Entry<String, Object>> uncastedDbMapIterator = uncastedDbTypeMap.entrySet().iterator();

        // Defining a castedMap which will hold all the typecasted Entries from the
        // uncastedDbTypeMap.
        HashMap<String, String> castedMap = new HashMap<String, String>();

        // Iterating through the uncastedDbTypeMap until all its Entries are iterated.
        while (uncastedDbMapIterator.hasNext()) {

            // For the current iteration, getting a reference to the currentEntry in the
            // uncastedDbTypeMap.
            Entry<String, Object> currentEntry = uncastedDbMapIterator.next();

            // Checking if the value of the currentEntry is of type String.
            // If it is, then we will add it to the castedMap and move on to the next Entry.
            if (currentEntry.getValue() instanceof String)
                castedMap.put(currentEntry.getKey(), (String) currentEntry.getValue());
        }

        // Finally once the iteration of uncastedDbTypeMap is complete,
        // or it didn't contain any Entry, then returning castedMap.
        return castedMap;
    }

    /**
     * Method which returns the default document format mentioned in the
     * AtumClassesPropertyFile. If no document format node is found in the file,
     * Then this throws an exception.
     * 
     * @return name of the default document format to be used for creating
     *         ObjectGraph meta-data file.
     * 
     * @throws Exception If no default document format was found in the
     *                   AtumClassesPropertyFile.
     */
    public static String GetDefaultObjGraphDocFormat() throws Exception {

        // If this is the first call to get the defaultDocFormat, then
        // from the atum-map, lookup and store the name of the defaultDocFormat
        // specified in the map.
        if (null == defaultDocFormat) {

            // If populated atum-map doesn't contains a default
            // doc format then throwing and exception.
            if (!atumMap.containsKey(DEFAULT_OBJGRAPH_DOCFORMAT))
                throw new Exception(
                        "FATAL: No default document format for object-graph was specified in AtumClassesPropertyFile.");

            // If atum map contains a default document format key, then getting its value as
            // an object type.
            Object docFormatUncasted = atumMap.get(DEFAULT_OBJGRAPH_DOCFORMAT);

            // Checking if underlying type of read object is String
            if (docFormatUncasted instanceof String)
                // If it is String, then typecasting and saving the value to the
                // defaultDocFormat field of this class for future calls.
                defaultDocFormat = (String) docFormatUncasted;
        }

        // Simply returning the defaultDocFormat to the caller.
        return defaultDocFormat;
    }
}
