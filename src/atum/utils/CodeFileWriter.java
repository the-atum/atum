
/*******************************************************************************
 * Author:        Pawan Vyas (PBMV)
 * Email:         pawan.vyas.gitlab@gmail.com
 * 
 * Project:       Atum/Utils
 * File:          CodeFileWriter.java
 * 
 * Description:
 *      Utility class which allows a common method for code to be written to files.
 * 
 * Purpose:
 *      This class will be used to write generated DO, DAL and DB-Scripts to 
 *      separate files based on the passed parameters.
 * 
 * Revision History:
 *      2019-Sep-26 (PBMV): File Created.
 *      2019-Dec-15 (IVNR): Updated generateFile() method to directly throw 
 *                          exception from here.
 *      2020-May-24 (PBMV): Standard code auto-formatting using RedHat's Java
 *                          Code-Formatter extension added with the Java
 *                          Language pack.
 *      2020-May-30 (PBMV): Moved to atum.utils package.
 *      2020-May-30 (PBMV): Fixed import errors due to package restructuring.
 *      2020-Jun-09 (PBMV): Updated to handle FileWrite related errors more
 *                          accurately with proper error messages.
 *      2020-Dec-29 (PBMV): Added table-name directory to segregate output if
 *                          needed, defaults to no table-name directory created.
 ****************************************************************************** */

package atum.utils;

import java.io.File;
import java.io.FileWriter;
import atum.dataobjects.CodeFileInfo;

/**
 * Utility class which allows a common method for code to be written to files.
 * This class will be used to write generated DO, DAL and DB-Scripts to separate
 * files based on the passed parameters.
 */
public class CodeFileWriter {

    /**
     * Default private constructor to prevent this class from being instantiated.
     */
    private CodeFileWriter() {
    }

    /**
     * Function which writes the passed content to a file using the provided
     * fileInfo parameter-object.
     * 
     * @param fileInfo is the passed pure-data-object which contains all the
     *                 relevant file info needed to generate the file.
     */
    public static void generateFile(CodeFileInfo fileInfo) {

        try {

            // Defining a of directory structure where this file will be stored.
            String fileDirectoryName = CodeFileInfo.outputDirName + fileInfo.dbDirName + fileInfo.langDirName
                    + fileInfo.subTypeDirName + fileInfo.tableDirName;

            File codePath = new File(fileDirectoryName);

            // Now creating the above directory structure.
            codePath.mkdirs();

            // Defining the name of the file which will be generated in the fileDirectory.
            String fileName = fileInfo.tableName + fileInfo.fileType + fileInfo.fileExtension;

            // Defining the absolute path of the file with its name.
            String fullFileName = fileDirectoryName + fileName;

            // Defining DataObject file which will contain generated code.
            File codeFile = new File(fullFileName);

            // Flag to check if the current codeFile exists or not.
            boolean fileExists = codeFile.exists();

            // If this file already exists then delete it so that for
            // each run a new file is created and old is deleted.
            if (fileExists)
                codeFile.delete();

            // Checking if file was actually created.
            if (codeFile.createNewFile()) {

                // If file was actually created, then creating a FileWriter on the codeFile.
                FileWriter fileWriter = new FileWriter(codeFile);
                try {

                    // Trying to write the file contents, i.e. code to the file.
                    fileWriter.write(fileInfo.content);
                } catch (Exception fwExObj) {

                    // In case of exceptions, displaying appropriate message on the ErrorStream.
                    System.err.println("Could not write " + fileInfo.fileType + " code for: " + fileInfo.tableName
                            + fileInfo.fileType + fileInfo.fileExtension + " to a file. Please check exception stack below:");

                    fwExObj.printStackTrace();
                } finally {
                    
                    // Closing the fileWriter resource to avoid resource/memory leaks.
                    fileWriter.close();
                }
            }
        } catch (Exception exObj) {

            System.err.println("Could not create directory: " + fileInfo.fileType + "/ or source-code file: " + fileInfo.tableName
                    + fileInfo.fileType + fileInfo.fileExtension + ". Please check exception stack below:");
            exObj.printStackTrace();
        }
    }
}
