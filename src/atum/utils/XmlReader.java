
/*******************************************************************************
 * Author:        Indresh Rawat (IVR)
 * Email:         indreshrawat.gitlab@gmail.com
 * 
 * Project:		  Atum/Utils
 * File:          XmlReader.java
 * 
 * Description:
 *      XmlReader reads the xml file and populates the Map which is heavily
 *      used in the Atum project.
 * 
 * Purpose:
 * 		To read the given XML Property file which consists of the 
 * 
 * Revision History:
 *      2019-Jul-22 (IVNR): File Created.
 *      2019-Jul-23 (IVNR): Updated populateAtum method.
 * 		2019-Jul-27 (PBMV): Removed package name and formatted the header
 * 							comment block as per current standard. Also
 * 							removed redundant catch blocks.
 * 		2019-Jul-27 (PBMV): Resolved merge conflict on 'experimental' branch
 * 							after pulling 'master'.
 * 		2019-Jul-27 (PBMV): Bug fix for post-refactoring crash when trying to 
 * 							access Node methods of a non-Element Node.
 * 		2019-Jul-27 (PBMV): Updated GetPropertyMap to now return the entire
 * 							XML Map instead of returning just the map of the 1st
 * 							element, in this case <atum></atum>. 
 * 		2019-Dec-17 (IVNR): Updated populateMap() method.
 *      2020-May-25 (PBMV): Standard code auto-formatting using RedHat's Java
 *                          Code-Formatter extension added with the Java
 *                          Language pack.
 *      2020-May-30 (PBMV): Moved to atum.utils package.
 ****************************************************************************** */

package atum.utils;

import java.io.File;
import java.util.HashMap;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * XmlReader class which reads an XML file and creates a map as per the file
 * structure. This class will be used by Factory and AbstractFactory classes to
 * populate their own maps.
 */

public class XmlReader {

	/**
	 * Function which will return a Map that is populated using the passed XML file.
	 * This is primarily for retrieving class names for the Atum Factories and
	 * AbstractFactories.
	 * 
	 * @param propertyFilePath is the path of the property file to be parsed.
	 * @return a HashMap[String, Object] type which was populated using the passed
	 *         propertyFilePath.
	 */
	public static HashMap<String, Object> GetPropertyMap(String propertyFilePath) {

		// Creating the file which stores the XML.
		File file = new File(propertyFilePath);

		// Creating the HashMap representation of the XML file which will store all the
		// DOM objects and their values recursively.
		HashMap<String, Object> xmlMap = new HashMap<>();

		// It is used to create DOM instance.
		DocumentBuilder domBuilder;

		try {

			// After creating DOM instance it obtains the parser.
			domBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

			// Parsing the contents of file in DOM representation.
			Document doc = domBuilder.parse(file);

			// It is used to remove comments, unwanted text from DOM.
			doc.normalize();

			// Checking if the DOM has child nodes
			if (doc.hasChildNodes())

				// Calling the populateAtum method by passing it all the child nodes
				// as a NodeList of the first nodeElement of the XMLFile.
				xmlMap = populateMap(doc.getChildNodes(), xmlMap);

		} catch (Exception exObj) {
			System.err.println(exObj.getClass().toGenericString());
			exObj.printStackTrace();
		}

		// Finally returning the populated xmlMap to the caller.
		return xmlMap;
	}

	/**
	 * This method is used to recursively populate the passed demoMap by iterating
	 * through DOM objects in the nodeList until each Node's childNode called
	 * "classname" is reached, which indicates that we have traversed to the depth
	 * of the current node and therefore this Node's name and its classname's value
	 * is to be added in the demoMap.
	 * 
	 * @param nodeList is the list of Nodes passed which are to be iterated and
	 * @param demoMap  is the map passed in which each Node's name and its value is
	 *                 to be added.
	 * @return a recursively populated HashMap[String, Object] to the caller.
	 */
	private static HashMap<String, Object> populateMap(NodeList nodeList, HashMap<String, Object> demoMap) {

		// Location of classname in DOM tree.
		int classnames = 1;

		// Getting the length of the passed nodeList.
		int nodeListLength = nodeList.getLength();

		// Iterating through the DOM objects.
		for (int count = 0; count < nodeListLength; count++) {

			// currentNode represents the node currently being iterated in the DOM Tree.
			Node currentNode = nodeList.item(count);

			// Checking if the currentCode is an Element-Node, if not we will skip
			// over the currentNode and move to the next node. as the structure
			// when parsing the XML document is:
			// [0]: text
			// [1]: node
			// [2]: text
			// [3]: node
			// The text elements in the NodeList are not Node Elements, and therefore
			// accessing certain functions from Text-Nodes will result in
			// NullPointerException being raised.
			if (Node.ELEMENT_NODE == currentNode.getNodeType()) {

				// Getting a list of all child Nodes of the currentNode.
				NodeList currentNodeChildrenList = currentNode.getChildNodes();

				// Defining a local variable to store the name of the currentNode.
				// This variable will be the key in the demoMap that is being populated.
				String currentNodeName = currentNode.getNodeName();

				// Getting the child element in the currentNodeChildrenList's
				// classname location(index).
				Node classnameNode = currentNodeChildrenList.item(classnames);

				// Defining a flag which indicates whether the classnameNode's name is actually
				// "classname" or not.
				boolean isItClassnameNode = classnameNode.getNodeName().equalsIgnoreCase("classname");

				// Checking if classnameNode's name was actually "classname" or not.
				if (isItClassnameNode)

					// Putting the key as currentNodeName and the classname value
					// of the currentNode. This is essentially a <String, String> type entry.
					// Which will be needed to be type casted from <String, Object> back to
					// <String, String> in the Factory class.
					demoMap.put(currentNodeName, currentNode.getTextContent().trim());

				// Checking if the node has child nodes and it is not classname.
				if (currentNode.hasChildNodes() && !isItClassnameNode) {

					// Now recursively calling this method again by passing it the list of
					// currentNode's children
					// and the currentNodeMap defined above as the demoMap for the next stackframe.
					HashMap<String, Object> populatedMap = populateMap(currentNodeChildrenList,
							new HashMap<String, Object>());

					// Putting the key as currentNodeName and the value as
					// HashMap<String, Object> type which will be used for iterating
					// inside DOM objects. This is essentially a
					// <String, HashMap<String, Object>> type entry, which will be
					// needed to type casted from <String, Object> to
					// <String, HashMap<String, Object>> in the AbstractFactory class.

					// Once the recursion is completed, putting the currentNodeName as key and the
					// populatedMap as the value in the demoMap.
					demoMap.put(currentNodeName, populatedMap);
				}
			}
		}

		// Returning the demoMap to the caller of this function.
		return demoMap;
	}

}
