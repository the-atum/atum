
/** *****************************************************************************
 * Author:        Indresh Rawat (IVNR)
 * Email:         indreshrawat.gitlab@gmail.com
 *
 * Project:       Atum/TestAtum
 * File:          TestAtum.java
 *
 * Description:
 *       TestAtum class used for the testing environment for Atum.
 *
 * Purpose:
 *       It is used for the testing environment for Atum.
 *
 * Revision History:
 *      2020-May-24 (IVNR): File created.
 *      2020-May-25 (PBMV): Standard code auto-formatting using RedHat's Java
 *                          Code-Formatter extension added with the Java
 *                          Language pack.
 *      2020-May-30 (PBMV): Updated instantiation of ObjGraphReader using
*                           factory instead of "new".
 *      2020-May-30 (PBMV): Moved to atum.test package.
 *      2020-May-30 (PBMV): Fixed import errors due to package restructuring.
 *      2020-Dec-28 (PBMV): Changed maps from HashMap to LinkedHashMap to
 *                          preserve insertion order.
 ****************************************************************************** */

package atum.test;

import java.util.LinkedHashMap;
import java.util.Scanner;
import java.util.TreeSet;
import atum.dataobjects.ConnectionParameter;
import atum.dataobjects.ObjectGraph;
import atum.interfaces.ICodeGenerator;
import atum.interfaces.IDBReader;
import atum.interfaces.IObjectGraphReader;
import atum.interfaces.IScriptGenerator;
import atum.factories.CodeGeneratorFactory;
import atum.factories.DBReaderFactory;
import atum.factories.ObjGraphCreatorReaderFactory;
import atum.factories.ScriptGeneratorFactory;
import atum.utils.AtumMapRetriever;

public class TestAtum {

        /**
         * The below method is a testing environment for Atum when user inputs from the
         * console.
         * 
         * @throws Exception
         */
        public void runTestAtumModulesWithUserInput() throws Exception {

                // Getting user input of dbToBeReadFrom - is the Database-type whose
                // available database information is to be read.
                Scanner inputScanner = new Scanner(System.in);
                System.out.print(
                                "Enter your \"source\" database-type from which you would like to read entire meta-data: ");
                String dbToBeReadFrom = inputScanner.nextLine().trim();
                System.out.println();

                System.out.print("Enter the \"username\" of your \"source\" database-type: ");
                String srcDbUserName = inputScanner.nextLine().trim();
                System.out.println();

                System.out.print("Enter the \"password\" of your \"source\" database-type: ");
                String srcDbUserPassword = inputScanner.nextLine().trim();
                System.out.println();

                System.out.print("Enter the \"connection URL\" of your \"source\" database-type: ");
                String srcDbConnectionURL = inputScanner.nextLine().trim();
                System.out.println();

                System.out.print("Enter the \"document format\" you wish to use for generating the meta-data file: ");
                String documentFormatType = inputScanner.nextLine().trim();
                System.out.println();

                // Getting user input of dbToBeGeneratedFor - is the target database-type
                // for which code and scripts are to be generated.
                System.out.print(
                                "Enter the \"target\" database-type for which you would like to generate language-code and scripts: ");
                String dbToBeGeneratedFor = inputScanner.nextLine().trim();
                System.out.println();

                System.out.print("Enter the \"username\" of your \"target\" database-type: ");
                String trgDbUserName = inputScanner.nextLine().trim();
                System.out.println();

                System.out.print("Enter the \"password\" of your \"target\" database-type: ");
                String trgDbUserPassword = inputScanner.nextLine().trim();
                System.out.println();

                System.out.print("Enter the \"connection URL\" of your \"target\" database-type: ");
                String trgDbConnectionURL = inputScanner.nextLine().trim();
                System.out.println();

                // Getting user input of codeLanguageType which is the language for which
                // Code is to be generated in, based on the dbToBeGeneratedFor provided.
                System.out.print("Enter the \"language\" for which you would like its code to be generated: ");
                String codeLanguageType = inputScanner.nextLine().trim();
                // String codeLanguageType = "java";
                System.out.println();

                System.out.println("\nAttempting to generate code in \"" + codeLanguageType
                                + "\"\nand database scripts(and store procedures if applicable) for \""
                                + dbToBeGeneratedFor + "\"\nusing meta-data of existing databases read from \""
                                + dbToBeReadFrom + "\"\nwith credentials given as:" + "\n\t\t\"User\": " + srcDbUserName
                                + "\n\t\t\"Password\": " + srcDbUserPassword + "\n\t\t\"Connection\": "
                                + srcDbConnectionURL + "\n\n");

                inputScanner.close();

                // Composing a ConnectionParameter object which will be injected to the
                // DBReader that is instantiated.
                ConnectionParameter srcDbConnParam = new ConnectionParameter();
                srcDbConnParam.dbUserName = srcDbUserName;
                srcDbConnParam.dbUserPassword = srcDbUserPassword;
                srcDbConnParam.dbConnectionURL = srcDbConnectionURL;

                // Instantiating a dBReader object as per the dbToBeReadFrom entered by the
                // user.
                IDBReader dbReader = DBReaderFactory.GetDBReader(dbToBeReadFrom);
                dbReader.SetConnectionParameter(srcDbConnParam);

                String objGraphFileName = dbReader.generateAndGetObjectGraphFileName(null, documentFormatType);

                // Getting an instance of the IObjectGraphReader based on the documentFormatType
                // from the factory.
                IObjectGraphReader objectGraphReader = ObjGraphCreatorReaderFactory
                                .GetObjGraphReader(documentFormatType);

                // Getting object graph from dBReader and setting its connParamObj using values
                // provided by the user for target DB.
                ObjectGraph objGraph = objectGraphReader.getObjectGraphFromFile(objGraphFileName);
                objGraph.connParamObj.dbConnectionURL = trgDbConnectionURL;
                objGraph.connParamObj.dbUserName = trgDbUserName;
                objGraph.connParamObj.dbUserPassword = trgDbUserPassword;

                // Instantiating a codeGenerator object as per the codeLanguageType entered by
                // the user.
                ICodeGenerator dbCodeGenerator = CodeGeneratorFactory.GetCodeGenerator(dbToBeGeneratedFor,
                                codeLanguageType);
                dbCodeGenerator.SetObjectGraph(objGraph);

                // If all arguments are proper, then based on what dBReader and dbCodeGenerator
                // objects that gets initialized, we are generating the code.
                dbCodeGenerator.generateCode();

                // Getting the concrete scriptGenerator instance from the
                // scriptGeneratorFactory, by passing it the dbToBeGeneratedFor concrete
                // type.
                IScriptGenerator scriptGenerator = ScriptGeneratorFactory.GetScriptGenerator(dbToBeGeneratedFor);
                scriptGenerator.SetObjectGraph(objGraph);

                // Again if ScriptGenerator was properly instantiated then executing the method
                // to generate DB scripts for the dbToBeGeneratedFor which was entered.
                scriptGenerator.generateScripts();
        }

        /**
         * The below method is the testing environment for Atum without user input.
         * 
         * @param sourceDbType is the source database server type to connect and read from.
         * @param targetDbType is the database to be targeted for code/script generation.
         * @param codeGenLanguage is the language for which code is to be generated for.
         * @param sourceUrl is the url of the source database server instance.
         * @param sourceUser is the username to be used for connecting to the source database server.
         * @param sourcePassword  is the password for the provided username.
         * @param sourceDbName is the name of the source database server's specific db-name(or schema-name) whose code and scripts
         *                     are to be generated.
         * @throws Exception
         */
        public void runTestAtumModules(String sourceDbType, String targetDbType, String codeGenLanguage, String sourceUrl, String sourceUser, String sourcePassword, String sourceDbName) throws Exception {

                // Instantiation testDbObject for hardcore inputs.
                TestDbObject testDbObject = new TestDbObject(sourceDbType, sourceUrl, sourceUser, sourcePassword);

                String dbToBeReadFrom = testDbObject.sourceDB;

                String srcDbUserName = testDbObject.sourceDbUserName;

                String srcDbUserPassword = testDbObject.sourceDbUserPassword;

                String srcDbConnectionURL = testDbObject.sourceDbConnectionURL;

                String documentFormatType = AtumMapRetriever.GetDefaultObjGraphDocFormat();

                String dbToBeGeneratedFor = targetDbType;

                String trgDbUserName = testDbObject.targetDbUserName;

                String trgDbUserPassword = testDbObject.targetUserPassword;

                String trgDbConnectionURL = testDbObject.targetDbConnectionURL;

                String codeLanguageType = codeGenLanguage;

                System.out.println("\nAttempting to generate code in \"" + codeLanguageType
                                + "\"\nand database scripts(and store procedures if applicable) for \""
                                + dbToBeGeneratedFor + "\"\nusing meta-data of existing databases read from \""
                                + dbToBeReadFrom + "\"\nwith credentials given as:" + "\n\t\t\"User\": " + srcDbUserName
                                + "\n\t\t\"Password\": " + srcDbUserPassword + "\n\t\t\"Connection\": "
                                + srcDbConnectionURL + "\n\n");

                // Composing a ConnectionParameter object which will be injected to the
                // DBReader that is instantiated.
                ConnectionParameter srcDbConnParam = new ConnectionParameter();
                srcDbConnParam.dbUserName = srcDbUserName;
                srcDbConnParam.dbUserPassword = srcDbUserPassword;
                srcDbConnParam.dbConnectionURL = srcDbConnectionURL;

                // Instantiating a dBReader object as per the dbToBeReadFrom entered by the
                // user.
                IDBReader dbReader = DBReaderFactory.GetDBReader(dbToBeReadFrom);
                dbReader.SetConnectionParameter(srcDbConnParam);

                TreeSet<String> tables = new TreeSet<>();

                LinkedHashMap<String, TreeSet<String>> dataBaseMap = new LinkedHashMap<>();
                dataBaseMap.put(sourceDbName, tables);

                String schemaFileName = dbReader.generateAndGetObjectGraphFileName(dataBaseMap, documentFormatType);

                // Getting an instance of IObjectGraphReader from the factory based on the
                // documentFormatType.
                IObjectGraphReader objectGraphReader = ObjGraphCreatorReaderFactory
                                .GetObjGraphReader(documentFormatType);

                // Getting object graph from dBReader and setting its connParamObj using values
                // provided by the user for target DB.
                ObjectGraph objGraph = objectGraphReader.getObjectGraphFromFile(schemaFileName);
                objGraph.connParamObj.dbConnectionURL = trgDbConnectionURL;
                objGraph.connParamObj.dbUserName = trgDbUserName;
                objGraph.connParamObj.dbUserPassword = trgDbUserPassword;

                // Instantiating a codeGenerator object as per the codeLanguageType entered by
                // the user.
                ICodeGenerator dbCodeGenerator = CodeGeneratorFactory.GetCodeGenerator(dbToBeGeneratedFor,
                                codeLanguageType);
                dbCodeGenerator.SetObjectGraph(objGraph);

                // If all arguments are proper, then based on what dBReader and dbCodeGenerator
                // objects that gets initialized, we are generating the code.
                dbCodeGenerator.generateCode();

                // Getting the concrete scriptGenerator instance from the
                // scriptGeneratorFactory, by passing it the dbToBeGeneratedFor concrete
                // type.
                IScriptGenerator scriptGenerator = ScriptGeneratorFactory.GetScriptGenerator(dbToBeGeneratedFor);
                scriptGenerator.SetObjectGraph(objGraph);

                // Again if ScriptGenerator was properly instantiated then executing the method
                // to generate DB scripts for the dbToBeGeneratedFor which was entered.
                scriptGenerator.generateScripts();
        }
}
