/** *****************************************************************************
 * Author:        Indresh Rawat (IVNR)
 * Email:         indreshrawat.gitlab@gmail.com
 *
 * Project:       Atum/TestDbObject
 * File:          TestDbObject.java
 *
 * Description:
 *       TestDbObject class holds the connection object of database
 *       with hardcoded inputs when testing.
 *
 * Purpose:
 *       It is used for the testing environment for Atum.
 *
 * Revision History:
 *      2020-May-24 (IVNR): File created.
 *      2020-May-25 (PBMV): Standard code auto-formatting using RedHat's Java
 *                          Code-Formatter extension added with the Java
 *                          Language pack.
 *      2020-May-30 (PBMV): Moved to atum.test package.
 ****************************************************************************** */

package atum.test;

/**
 * TestDbObject class holds the connection object of database with hardcoded
 * inputs when testing.
 */
public class TestDbObject {

    // Source Database to read from.
    String sourceDB;

    // Source Database username.
    String sourceDbUserName;

    // Source Database password.
    String sourceDbUserPassword;

    // Source Database Connection URL.
    String sourceDbConnectionURL;

    // Target Database username.
    String targetDbUserName;

    // Target Database password.
    String targetUserPassword;

    // Target Database Connection URL.
    String targetDbConnectionURL;

    public TestDbObject() {

        // Assigning the variables for testing Atum.
        sourceDB = "mysql";
        sourceDbUserName = "root";
        sourceDbUserPassword = "root";
        sourceDbConnectionURL = "localhost:3306";
        targetDbUserName = "root";
        targetUserPassword = "root";
        targetDbConnectionURL = "localhost:3306";
    }

    public TestDbObject(String sourceDbType, String sourceUrl, String sourceUser, String sourcePassword) {
        sourceDB = sourceDbType;
        targetDbConnectionURL = sourceDbConnectionURL = sourceUrl;
        targetDbUserName = sourceDbUserName = sourceUser;
        targetUserPassword = sourceDbUserPassword = sourcePassword;
    }
}
