/*******************************************************************************
 * Author:        Pawan Vyas (PBMV)
 * Email:         pawan.vyas.gitlab@gmail.com
 * 
 * Project:       AtumDemo/Interfaces
 * File:          ICodeGenerator.java
 * 
 * Description:
 *      Interface which is to be implemented by all concrete classes and
 *      extended by interfaces that fall under the CodeGenerator product
 *      family of the Atum project family.
 * 
 * Purpose:
 *      Allows generating DO code for the selected DB type.
 * 
 * Revision History:
 *      2019-Apr-30 (PBMV): File Created.
 *      2019-May-19 (PBMV): Removed generate DAL and BAL methods.
 *      2019-Sep-26 (PBMV): Updated parameter type to IDBReader.
 *      2019-Sep-26 (PBMV): Changed method from generateDO to
 *                          generateCodeAndScripts.
 *      2019-Sep-26 (PBMV): Changed method from generateCodeAndScripts to
 *                          generateCode as Scripts are not handled from
 *                          IScriptGenerator interface.
 *      2020-May-25 (PBMV): Standard code auto-formatting using RedHat's Java
 *                          Code-Formatter extension added with the Java
 *                          Language pack.
 *      2020-May-25 (PBMV): IAtum removal and subsequent changes.
 *      2020-May-25 (PBMV): Added SetObjectGraph method.
 *      2020-May-30 (PBMV): Moved to atum.interfaces package.
 *      2020-May-30 (PBMV): Fixed import errors due to package restructuring.
 ******************************************************************************/

package atum.interfaces;

import atum.dataobjects.ObjectGraph;

/**
 * Interface which is to be implemented by all concrete classes and extended by
 * interfaces that fall under the CodeGenerator product family of the Atum
 * project family.
 */
public interface ICodeGenerator {

    /**
     * Method which sets the passed objGraph to the internal ObjectGraph of the
     * concrete implementation class.
     * 
     * @param objGraph is the instance of link ObjectGraph type which will be used
     *                 to initialize the internal link ObjectGraph field.
     */
    public void SetObjectGraph(ObjectGraph objGraph);

    /**
     * Function which generates DataObject, DAL classes and DB Scripts using the
     * selected DB's objectGraph.
     * 
     * @throws Exception
     */
    public void generateCode() throws Exception;
}
