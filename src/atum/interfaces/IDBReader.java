
/** *****************************************************************************
 * Author:        Pawan Vyas (PBMV)
 * Email:         pawan.vyas.gitlab@gmail.com
 *
 * Project:       AtumDemo/Interfaces
 * File:          IDBReader.java
 *
 * Description:
 *      Interface to provide common methods which every concrete DBReader
 *      class must implement.
 *
 * Purpose:
 *      Allows reading DB information from relational-databases.
 *
 * Revision History:
 *      2019-Apr-30 (PBMV): File Created.
 *      2019-May-19 (PBMV): Removed connect method and added getSchemaMetaData
 *                          method into the interface from previous
 *                          IDBReader interface, which is now removed.
 *      2019-Sep-26 (PBMV): Renamed getSchemaMetaData to getDBMetaData.
 *      2020-May-24 (IVNR): Renamed generateSchemaFileName() method to
 *                          generateAndGetObjectGraphFileName().
 *      2020-May-25 (PBMV): Standard code auto-formatting using RedHat's Java
 *                          Code-Formatter extension added with the Java
 *                          Language pack.
 *      2020-May-25 (PBMV): Added SetConnectionParameter method.
 *      2020-May-25 (PBMV): IAtum removal and subsequent changes.
 *      2020-May-30 (PBMV): Added documentFormatType parameter to the
 *                          generateAndGetObjectGraphFileName method.
 *      2020-May-30 (PBMV): Moved to atum.interfaces package.
 *      2020-May-30 (PBMV): Fixed import errors due to package restructuring.
 *      2020-Dec-28 (PBMV): Changed maps from HashMap to LinkedHashMap to
 *                          preserve insertion order.
 ******************************************************************************/

package atum.interfaces;

import java.util.LinkedHashMap;
import java.util.TreeSet;
import atum.dataobjects.ConnectionParameter;

/**
 * Interface which provides common methods which every concrete DBReader class
 * must implement.
 */
public interface IDBReader {

    /**
     * Method which sets the passed ConnectionParameter object to the internal
     * ConnectionParameter object in the concrete implementation.
     * 
     * @param connParam is the ConnectionParameter object used to initialize the
     *                  internal ConnectionParameter object.
     */
    public void SetConnectionParameter(ConnectionParameter connParam);

    /**
     * Method which returns the name of the file in which DataBase and Tables
     * meta-data is written. This file name can be later used by ObjectGraphReader
     * to generate a Java based ObjectGraph.
     * 
     * @param dbTablesMap        is the LinkedHashMap which has String as key where they
     *                           are database name and value as TreeSet[String]
     *                           which are list of tables.
     * @param documentFormatType is the desired output type of the obj-graph
     *                           document file which will be created by reading from
     *                           the database. Eg: *.xml, or *.json, etc.
     * 
     * @return the name of the file to which all, or whichever selected DataBases
     *         and their Tables meta-data is written in custom Atum pars-able format
     *         for the ObjectGraphCreator.
     *
     * @throws Exception
     */
    public String generateAndGetObjectGraphFileName(LinkedHashMap<String, TreeSet<String>> dbTablesMap,
            String documentFormatType) throws Exception;

}
