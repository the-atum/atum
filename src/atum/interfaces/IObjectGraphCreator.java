
/*******************************************************************************
 * Author:        Pawan Vyas (PBMV)
 * Email:         pawan.vyas.gitlab@gmail.com
 * 
 * Project:       Atum/Interfaces.
 * File:          IObjectGraphCreator.java
 * 
 * Description:
 *      Interface to provide common methods for all ObjectGraphCreator
 *      implementations.
 * 
 * Purpose:
 *      To facilitate creation of an ObjectGraph file of a specific extension
 *      type such as XML, or JSON etc. based on a concrete implementation of
 *      this interface.
 * 
 * Revision History:
 *      2020-May-25 (PBMV): File created.
 *      2020-May-25 (PBMV): Updated signature to return name of the ObjectGraph
 *                          document file.
 *      2020-May-30 (PBMV): Moved to atum.interfaces package.
 ******************************************************************************/

package atum.interfaces;

import java.util.HashMap;

/**
 * Interface to provide common methods for all ObjectGraphCreator
 * implementations.
 */
public interface IObjectGraphCreator {
    /**
     * This method is used to create an ObjectGraph document based on the map.
     * Concrete implementations of this interface will determine document format.
     * Eg: XML, JSON, etc. Once the document is created, it returns the name of the
     * document file.
     *
     * @param rawDbReaderMap      is a map which contains databases information.
     * @return Name of the created document file.
     * @throws Exception If implementing Document creator API throws any exceptions,
     *                   or if ObjectGraphFilePath could not be resolved.
     */
    public String createObjectGraphFile(HashMap<?, ?> rawDbReaderMap) throws Exception;
}
