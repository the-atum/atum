/*******************************************************************************
 * Author:        Pawan Vyas (PBMV)
 * Email:         pawan.vyas.gitlab@gmail.com
 * 
 * Project:       Atum/Interfaces
 * File:          IObjectGraphReader.java
 * 
 * Description:
 *      Interface to provide common methods for all ObjectGraphReader
 *      implementations.
 * 
 * Purpose:
 *      To facilitate parsing of an already created ObjectGraph file of a specific
 *      extension type such as XML, or JSON etc. back to the defined ObjectGraph
 *      data-object format in this project.
 * 
 * Revision History:
 *      2020-May-25 (PBMV): File created.
 *      2020-May-30 (PBMV): Moved to atum.interfaces package.
 *      2020-May-30 (PBMV): Fixed import errors due to package restructuring.
 ******************************************************************************/

package atum.interfaces;
import atum.dataobjects.ObjectGraph;

/**
 * Interface to provide common methods for all ObjectGraphReader
 * implementations.
 */
public interface IObjectGraphReader {

    /**
     * Method will populate and return an ObjectGraph instance by parsing the passed
     * object graph document file. Concrete implementations of this interface
     * determine the type of document file that is parsed. Eg: XML, JSON, etc.
     *
     * @param objectGraphFilePath is the path of where the object graph document
     *                            file is saved.
     *
     * @return An instance of ObjectGraph populated by parsing the passed
     *         objectGraph document file.
     * @throws Exception If concrete document reader API fails, or if provided
     *                   objectGraphFilePath could not be resolved.
     */
    public ObjectGraph getObjectGraphFromFile(String objectGraphFilePath) throws Exception;
}
