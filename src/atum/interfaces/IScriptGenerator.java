/*******************************************************************************
 * Author:        Pawan Vyas (PBMV)
 * Email:         pawan.vyas.gitlab@gmail.com
 * 
 * Project:       Atum/Interfaces
 * File:          IScriptGenerator.java
 * 
 * Description:
 *      Interface which is to be implemented by all concrete classes and
 *      extended by interfaces that fall under the ScriptGenerator product
 *      family of the Atum project family.
 * 
 * Purpose:
 *      This interface provides common functionality for generating DB scripts
 *      for the selected DB for which code and scripts are to be generated.
 * 
 * Revision History:
 *      2019-Sep-28 (PBMV): File Created.
 *      2020-May-25 (PBMV): Standard code auto-formatting using RedHat's Java
 *                          Code-Formatter extension added with the Java
 *                          Language pack.
 *      2020-May-25 (PBMV): IAtum removal and subsequent changes.
 *      2020-May-25 (PBMV): Added SetObjectGraph method.
 *      2020-May-30 (PBMV): Moved to atum.interfaces package.
 *      2020-May-30 (PBMV): Fixed import errors due to package restructuring.
 ******************************************************************************/

package atum.interfaces;

import atum.dataobjects.ObjectGraph;

/**
 * Interface which is to be implemented by all concrete classes and extended by
 * interfaces that fall under the ScriptGenerator product family of the Atum
 * project family.
 */
public interface IScriptGenerator {

    /**
     * Method which sets the passed objGraph to the internal ObjectGraph of the
     * concrete implementation class.
     * 
     * @param objGraph is the instance of link ObjectGraph type which will be used
     *                 to initialize the internal link ObjectGraph field.
     */
    public void SetObjectGraph(ObjectGraph objGraph);

    /**
     * Function which generates Table creation and/or Store Procedure Scripts(if
     * applicable) using the selected DB's objectGraph.
     * 
     * @throws Exception
     */
    public void generateScripts() throws Exception;
}
