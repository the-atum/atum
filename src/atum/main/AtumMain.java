
/*******************************************************************************
 * Author: Pawan Vyas (PBMV)
 * Email: pawan.vyas.gitlab@gmail.com
 *
 * Project: AtumDemo/Main
 * File: AtumMain.java
 *
 * Description:
 *      AtumMain is a dummy main class which executes the project.
 *
 * Purpose:
 *      The amin class to execute the project.
 *
 * Revision History:
 *      2019-Apr-30 (PBMV): File created.
 *      2019-Jul-27 (PBMV): Added header comment block.
 *      2019-Aug-13 (PBMV): Added missing JDOC comments for
 *                          methods: GetCastedComponentDbTypeMap() and
 *                          GetCastedComponentMap().
 *      2019-Aug-13 (PBMV): Also changed usage of provided arguments from
 *                          args[0] etc. to be using proper variable names.
 *      2019-Sep-26 (PBMV): Renamed to AtumMain and subsequent re-naming
 *                          related changes for other interfaces and factories.
 *      2019-Sep-27 (PBMV): Refactored map retrieving utilities to a different
 *                          class as it is not the concern of Main class to
 *                          perform those tasks.
 *      2019-Sep-28 (PBMV): Added calls to create and execute script generator.
 *      2019-Sep-28 (PBMV): Updated user input to be provided from prompts
 *                          instead of from using cmd-line arguments.
 *      2019-Dec-15 (IVNR): Updated main to create object graph and read the
 *                          object graph.
 *      2020-May-24 (IVNR): Added testAtumModulesWithUserInput() method and
 *                          testAtumModules() method for testing Atum.
 *      2020-May-24 (PBMV): Updated logic for test-method calls.
 *      2020-May-24 (PBMV): Standard code auto-formatting using RedHat's Java
 *                          Code-Formatter extension added with the Java
 *                          Language pack.
 *      2020-May-25 (PBMV): Restructured how Atum Test Modules are called.
 *      2020-May-30 (PBMV): Moved to atum.main package.
 *      2020-May-30 (PBMV): Fixed import errors due to package restructuring.
 *      2020-Dec-28 (PBMV): Hardcoded test-run params.
 * ***************************************************************************** */

package atum.main;

import java.util.HashMap;
import java.util.function.Consumer;

import atum.test.TestAtum;

/**
 * Main class for running Atum Test code.
 */
public class AtumMain {

    private static HashMap<Integer, Consumer<String []>> testMethodMap;

    static {
        testMethodMap = new HashMap<>();
        ComposeTestMethods();
    }

    private static void ComposeTestMethods()
    {
        Consumer<String[]> hardcodedMethod = argsArray -> {
            TestAtum testAtum = new TestAtum();
            try {
                testAtum.runTestAtumModules("mysql", "poc", "csharp", "localhost:3306", "root", "root", "pocdb");
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
        testMethodMap.put(1, hardcodedMethod);

        Consumer<String[]> argsMethod = argsArray -> {
            TestAtum testAtum = new TestAtum();
            try {
                testAtum.runTestAtumModules(argsArray[0], argsArray[1], argsArray[2], argsArray[3], argsArray[4], argsArray[5], argsArray[6]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
        testMethodMap.put(7, argsMethod);

        Consumer<String[]> inputMethod = argsArray -> {
            TestAtum testAtum = new TestAtum();
            try {
                testAtum.runTestAtumModulesWithUserInput();
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
        testMethodMap.put(0, inputMethod);
        testMethodMap.put(null, inputMethod);
    }

    public static void main(String args[]) throws Exception {
        if (testMethodMap.containsKey(args.length))
            testMethodMap.get(args.length).accept(args);
        else
            testMethodMap.get(0).accept(args);
    }
}
