
/** *****************************************************************************
 * Author:        Indresh Rawat (IVNR)
 * Email:         indreshrawat.gitlab@gmail.com
 *
 * Project:       Atum/ObjectGraphReader
 * File:          ObjectGraphReaderXML.java
 *
 * Description:
 *       ObjectGraphReaderXML class will create a map after reading the XML file
 *       as per the required structure. This map will be used by the
 *       Code Generator classes.
 *
 * Purpose:
 *      It will be used to generate code of specified database.
 *
 * Revision History:
 *      2019-Aug-24 (IVNR): File created.
 * 		2019-Aug-31 (IVNR): Added getUpdatedObjectGraphMap and setColumnMetaData
 * 							method.
 *      2019-Sep-04 (IVNR): Updated ObjectGraphReader class.
 * 		2019-Nov-07 (IVNR): Changed the name of ColumnMetaData class to
 * 							AtumColumnMetaData class and changed the name of
 * 							method getObjectGraph to getObjectGraphFromFile.
 *      2019-Dec-15 (IVNR): Updated getObjectGraphFromFile() method.
 *      2020-May-25 (PBMV): Standard code auto-formatting using RedHat's Java
 *                          Code-Formatter extension added with the Java
 *                          Language pack.
 *      2020-May-25 (PBMV): Removed getObjectGraphFromRawMap method.
 *      2020-May-25 (PBMV): Implemented IObjectGraphReader and renamed to
 *                          ObjectGraphReaderXML.
 *      2020-May-30 (PBMV): Moved to atum.objgraphreaders package.
 *      2020-May-30 (PBMV): Fixed import errors due to package restructuring.
 *      2020-Dec-28 (PBMV): Changed maps from HashMap to LinkedHashMap to
 *                          preserve insertion order.
 ****************************************************************************** */

package atum.objgraphreaders;

import java.io.File;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import atum.interfaces.IObjectGraphReader;
import atum.dataobjects.AtumColumnMetaData;
import atum.dataobjects.DbRawMetaData;
import atum.dataobjects.ObjectGraph;

/**
 * ObjectGraphReaderXML class will create a map after reading the XML file as
 * per the required structure. This map will be used by the Code Generator
 * classes.
 */
@SuppressWarnings("unchecked")
public class ObjectGraphReaderXML implements IObjectGraphReader {

    // Instantiation a object of DbRawMetadata class
    private DbRawMetaData dbRawMetaData = new DbRawMetaData();

    // Creating a field which will be useful to fill ColumnMetadata.
    private boolean fillMetaData = false;

    /**
     * Method will populate object graph map by parsing the object graph XML file
     * given by the object graph creator. It will be used by code generator as it
     * contains schema metaData.
     *
     * @param objectGraphFilePath is the path of object graph file.
     *
     * @return ObjectGraph object.
     * @throws Exception
     */
    @Override
    public ObjectGraph getObjectGraphFromFile(String objectGraphFilePath) throws Exception {

        // Creating the file which has the object graph.
        File objectGraphFile = new File(objectGraphFilePath);

        // Creating a object graph map which stores the nodes
        // from XML file in the map.
        LinkedHashMap<String, Object> objectGraphMap = new LinkedHashMap<>();

        // Creating a map which will be used by the code generators classes.
        dbRawMetaData.updatedObjectGraphMapFromFile = new LinkedHashMap<>();

        // Initializing it with the DocumentBuilderFactory instance.
        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

        // Initializing it with DocumentBuilder where it will be used to create a
        // document.
        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

        // Creating a document which stores the content of XML file which is parsed into
        // document in DOM Tree format.
        Document document = documentBuilder.parse(objectGraphFile);

        // It is used to remove comments, unwanted text from DOM.
        document.normalize();

        // Checking if the nodes of document has child nodes.
        if (document.hasChildNodes()) {
            // Populating the object graph map which will be used by code generator.
            objectGraphMap = populateObjectGraphMap(document.getChildNodes(), objectGraphMap);

            // Updating the object graph map to add AtumColumnMetaData class to it.
            dbRawMetaData.updatedObjectGraphMapFromFile = (LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, AtumColumnMetaData>>>>) getUpdatedObjectGraphMap(
                    objectGraphMap);
        }

        // Instantiating new ObjectGraph object.
        ObjectGraph objectGraph = new ObjectGraph();

        // Initializing the dbMap with the map of type LinkedHashMap<String, LinkedHashMap<String,
        // LinkedHashMap<String, AtumColumnMetaData>>>.
        objectGraph.dbMap = dbRawMetaData.updatedObjectGraphMapFromFile.get("schemaMetaData");
        // Returning the object graph map.
        return objectGraph;

    }

    /**
     * This method is used to recursively populate the passed currentMap by
     * iterating through DOM objects in the nodeList until each Node's childNode
     * called "valueNode" is reached, which indicates that we have traversed to the
     * depth of the current node and therefore this Node's name and its valueNode's
     * value is to be added in the currentMap.
     *
     * @param nodeList   is the list of Nodes passed which are to be iterated
     * @param currentMap is the map passed in which each Node's name and its value
     *                   is to be added.
     *
     * @return a recursively populated LinkedHashMap<String, Object> to the caller.
     */
    private LinkedHashMap<String, Object> populateObjectGraphMap(NodeList nodeList, LinkedHashMap<String, Object> currentMap) {

        // Getting the length of the passed nodeList.
        int nodeListLength = nodeList.getLength();

        // Iterating through the DOM objects.
        for (int count = 0; count < nodeListLength; count++) {

            // currentNode represents the node currently being iterated in the DOM Tree.
            Node currentNode = nodeList.item(count);

            // Checking if the currentCode is an Element-Node, if not we will skip
            // over the currentNode and move to the next node. as the structure
            // when parsing the XML document is:
            // [0]: text
            // [1]: node
            // [2]: text
            // [3]: node
            // The text elements in the NodeList are not Node Elements, and therefore
            // accessing certain functions from Text-Nodes will result in
            // NullPointerException being raised.
            if (Node.ELEMENT_NODE == currentNode.getNodeType()) {

                // Getting a list of all child Nodes of the currentNode.
                NodeList currentNodeChildrenList = currentNode.getChildNodes();

                // Defining a local variable to store the name of the currentNode.
                // This variable will be the key in the currentMap that is being populated.
                String currentNodeName = currentNode.getNodeName();

                // Checking if the node has child nodes.
                if (currentNode.hasChildNodes()) {

                    // Retrieving the first child of current Node and storing it into firstChild
                    // variable.
                    Node firstChild = currentNode.getFirstChild();

                    // Retrieving the node name of the first child.
                    String firstChildName = firstChild.getNodeName();

                    // Checking If the first child name is "valueNode".
                    if (firstChildName.equalsIgnoreCase("valueNode")) {

                        // Putting the key as currentNodeName and the value as
                        // string which is the node value.
                        currentMap.put(currentNodeName, firstChild.getTextContent());

                        // Continuing the for loop to add node's sibling to the currentMap.
                        continue;
                    }

                    // Putting the key as currentNodeName and the value as
                    // LinkedHashMap<String, Object> type which will be used for iterating
                    // inside DOM objects.
                    currentMap.put(currentNodeName, new LinkedHashMap<String, Object>());

                    // If there are child nodes which are not "valueNode" in the currentNode,
                    // then getting the currentNodeName's value from the currentMap, which is a
                    // LinkedHashMap.
                    LinkedHashMap<String, Object> currentNodeMap = (LinkedHashMap<String, Object>) currentMap.get(currentNodeName);

                    // Now recursively calling this method again by passing it the list of
                    // currentNode's children
                    // and the currentNodeMap defined above as the currentMap for the next
                    // stack-frame.
                    LinkedHashMap<String, Object> populatedMap = populateObjectGraphMap(currentNodeChildrenList,
                            currentNodeMap);

                    // Once the recursion is completed, putting the currentNodeName as key and the
                    // populatedMap as the value in the currentMap.
                    currentMap.put(currentNodeName, populatedMap);

                }
            }
        }

        // Returning the currentMap to the caller of this function.
        return currentMap;
    }

    /**
     * This method is used to update the objectGraphMap which was retrieved by the
     * XML file. Here, the map is being iterated by using recursion and add
     * AtumColumnMetData class to the map.
     *
     * [schemaMetadata -> [databaseName -> [tableName -> [columnName ->
     * [columnProperty , value]]
     *
     * Updated Map :
     *
     * [schemaMetadata -> [databaseName -> [tableName -> [columnName ,
     * AtumColumnMetaData]
     *
     * @param currentMap is the objectGraphMap which is to be updated.
     *
     * @return Returning the currentMap which is updated.
     */
    private LinkedHashMap<?, ?> getUpdatedObjectGraphMap(LinkedHashMap<?, ?> currentMap) {

        // Creating the iterator for the currentMap.
        Iterator<?> currentMapIterator = currentMap.entrySet().iterator();

        // Iterating through the currentMapIterator.
        while (currentMapIterator.hasNext()) {

            // The current element of the map which is the database map.
            Entry<String, Object> currentEntry = (Entry<String, Object>) currentMapIterator.next();

            // Creating a iterator for the currentEntry.
            Object retrievedCurrentEntryValue = currentEntry.getValue();

            // Checking if retrievedCurrentEntryValue is type of String.
            if (retrievedCurrentEntryValue instanceof String) {

                // Setting the value of fillMetaData to true;
                fillMetaData = true;

                // Returning to the caller of method.
                return null;
            }

            // Checking if retrievedCurrentEntryValue is type of LinkedHashMap<String, Object>
            if (retrievedCurrentEntryValue instanceof LinkedHashMap<?, ?>) {

                // Calling the method recursively to iterated through Object graph map.
                getUpdatedObjectGraphMap((LinkedHashMap<String, Object>) retrievedCurrentEntryValue);

                // Checking if fillMetaData is true.
                if (true == fillMetaData) {

                    // Storing retrievedCurrentEntryValue in a LinkedHashMap.
                    LinkedHashMap<String, String> columnPropertyMap = (LinkedHashMap<String, String>) retrievedCurrentEntryValue;

                    // Initializing a AtumColumnMetaData Object which will set the fields by calling
                    // the method.
                    AtumColumnMetaData columnMetaData = setColumnMetaData(columnPropertyMap);

                    // Replacing the columnEntry which is a LinkedHashMap<String, String> with the
                    // columnMetaData Object.
                    currentEntry.setValue(columnMetaData);

                    // Making the fillMetaData value to false.
                    fillMetaData = false;

                }
            }

        }

        // Returning the currentMap to the caller of method.
        return (LinkedHashMap<?, ?>) currentMap;
    }

    /**
     * This method sets the AtumColumnMetaData class fields based on the key of
     * columnPropertyMap.
     *
     * @param columnPropertyMap is a map containing the column properties.
     *
     * @return Returning the AtumColumMetaData object.
     */
    private AtumColumnMetaData setColumnMetaData(LinkedHashMap<String, String> columnPropertyMap) {
        // Instantiating a AtumColumnMetaData Object.
        AtumColumnMetaData columnMetaData = new AtumColumnMetaData();

        // Setting the fields of AtumColumnMetData object.
        columnMetaData.tableCatalog = columnPropertyMap.get("table_catalog");
        columnMetaData.isNullable = Boolean.parseBoolean(columnPropertyMap.get("is_nullable"));
        columnMetaData.tableName = columnPropertyMap.get("table_name");
        columnMetaData.databaseName = columnPropertyMap.get("database_name");
        columnMetaData.columnName = columnPropertyMap.get("column_name");
        columnMetaData.isPrimaryKey = Boolean.parseBoolean(columnPropertyMap.get("is_primaryKey"));
        columnMetaData.isUniqueKey = Boolean.parseBoolean(columnPropertyMap.get("is_uniqueKey"));
        columnMetaData.isMultipleKey = Boolean.parseBoolean(columnPropertyMap.get("is_multipleKey"));
        columnMetaData.setCharacterLengthBytes(columnPropertyMap.get("character_length_bytes"));
        columnMetaData.setCharacterMaximumLength(columnPropertyMap.get("character_maximum_length"));
        columnMetaData.setNumericPrecision(columnPropertyMap.get("numeric_precision"));
        columnMetaData.privileges = columnPropertyMap.get("privileges");
        columnMetaData.columnComment = columnPropertyMap.get("column_comment");
        columnMetaData.collationName = columnPropertyMap.get("collation_name");
        columnMetaData.setNumericScale(columnPropertyMap.get("numeric_scale"));
        columnMetaData.columnType = columnPropertyMap.get("column_type");
        columnMetaData.generationExpression = columnPropertyMap.get("generation_expression");
        columnMetaData.columnPosition = Short.parseShort(columnPropertyMap.get("column_position"));
        columnMetaData.dataType = columnPropertyMap.get("data_type");
        columnMetaData.characterSetName = columnPropertyMap.get("character_set_name");
        columnMetaData.columnDefault = columnPropertyMap.get("column_default");
        columnMetaData.isAutoIncrement = Boolean.parseBoolean(columnPropertyMap.get("is_auto_increment"));
        columnMetaData.isVirtualGenerated = Boolean.parseBoolean(columnPropertyMap.get("is_virtual_generated"));
        columnMetaData.isVirtualStored = Boolean.parseBoolean(columnPropertyMap.get("is_virtual_stored"));
        columnMetaData.isDefaultGenerated = Boolean.parseBoolean(columnPropertyMap.get("is_default_generated"));

        // Returning the columnMetaData object to the caller of method.
        return columnMetaData;
    }

}
