
/** *****************************************************************************
 * Author:        Pawan Vyas (PBMV)
 * Email:         pawan.vyas.gitlab@gmail.com
 *
 * Project:       AtumDemo/DBReader
 * File:          MySqlDBReader.java
 *
 * Description:
 *      Concrete implementing of IDBReader interface for MySQL database.
 *
 * Purpose:
 *      Allows reading and retrieving schema information from MySQL database.
 *
 * Revision History:
 *      2019-Apr-30 (PBMV): File Created.
 *      2019-May-19 (PBMV): Removed implementation for connect method.
 *      2019-Sep-26 (PBMV): AtumMain and subsequent interface and/or factory
 *                          related changes.
 *      2019-Sep-27 (IVNR): Updated getDBMetaData(), getTables() and
 *                          getColumnsInfo() method.
 *      2019-Nov-07 (IVNR): Updated generateSchemaFile() and
 *                          getObjectGraph() method.
 * 		2019-Nov-08 (IVNR): Updated generateSchemaFile() method.
 *      2019-Dec-15 (IVNR): Updated generateSchemaFile() method.
 *      2020-May-24 (IVNR): Added getTableSet() method, Renamed getTables()
 *                          to getDbTablesMap() and updated 
 *                          getColumnsMetaData() method.
 *      2020-May-25 (PBMV): Standard code auto-formatting using RedHat's Java
 *                          Code-Formatter extension added with the Java
 *                          Language pack.
 *      2020-May-25 (PBMV): Updated to not have ConnectionParameter set via
 *                          constructor, and instead be set using the 
 *                          SetConnectionParameter interface method.
 *      2020-May-25 (PBMV): IAtum removal and subsequent changes.
 *      2020-May-25 (PBMV): Updated client code for calling
 *                          ObjectGraphCreatorXML methods.
 *      2020-May-30 (PBMV): Updated client code for instantiating
 *                          IObjectGraphCreator using the factory. Also updated
 *                          method signature for the interface method.
 *      2020-May-30 (PBMV): Moved to atum.dbreaders package.
 *      2020-May-30 (PBMV): Fixed import errors due to package restructuring.
 *      2020-Dec-28 (PBMV): Changed maps from HashMap to LinkedHashMap to
 *                          preserve insertion order.
 ****************************************************************************** */

package atum.dbreaders;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.TreeSet;
import atum.dataobjects.ConnectionParameter;
import atum.dataobjects.DbRawMetaData;
import atum.dataobjects.FormalParametersClass;
import atum.factories.ObjGraphCreatorReaderFactory;
import atum.interfaces.IDBReader;
import atum.interfaces.IObjectGraphCreator;

/**
 * Concrete implementation of IDBReader interface which reads meta-data from the
 * DataBase reader and create an ObjectGraph file for it.
 */
@SuppressWarnings("unchecked")
public class MySqlDBReader implements IDBReader {

    /** */
    private DbRawMetaData dbRawMetaData;

    /** */
    private String objectGraphFileName;

    /** */
    private ConnectionParameter connParamObj;

    /**
     * Default constructor which initializes the instance fields of this class.
     */
    public MySqlDBReader() {
        connParamObj = null;
        objectGraphFileName = null;
        dbRawMetaData = new DbRawMetaData();
    }

    @Override
    public void SetConnectionParameter(ConnectionParameter connParam) {
        connParamObj = connParam;
    }

    @Override
    public String generateAndGetObjectGraphFileName(LinkedHashMap<String, TreeSet<String>> dbTablesMap,
            String documentFormatType) throws Exception {

        if (null == connParamObj)
            throw new Exception("FATAL: Connection parameters for this DBReader."
                    + "\nEnsure Connection Parameters are set before this method is invoked using"
                    + "the IDBReader.SetConnectionParameter method.");

        // This map will load the metadata of database like tables, columns and it's
        // property defined in user database
        dbRawMetaData.dbReaderMap = getColumnsMetaData(dbTablesMap);

        // Instantiation a object of ObjectGraphCreator class
        IObjectGraphCreator objectGraphCreator = ObjGraphCreatorReaderFactory.GetObjGraphCreator(documentFormatType);

        // Creating the object graph file.
        objectGraphFileName = objectGraphCreator.createObjectGraphFile(dbRawMetaData.dbReaderMap);

        // Returning objectGraphFileName to caller.
        return objectGraphFileName;
    }

    /**
     * The method is used to open connection to the database using the username,
     * password and url of the database
     *
     * @return connection object.
     *
     */
    private Connection connectionOpen() throws Exception {

        // Connection is used to connect to the database
        Connection connection = null;

        // Name of driver which will be used to connect to mysql database
        String driverName = "com.mysql.cj.jdbc.Driver";

        // connectionString stores the current database URL
        String connectionString = "jdbc:mysql://" + connParamObj.dbConnectionURL + "/";

        // Initializes the username of the database server
        String userName = connParamObj.dbUserName;

        // Initializes the password of database server
        String password = connParamObj.dbUserPassword;

        // Creating the object based on the driver
        Class.forName(driverName);

        // Initializes the connection to the database using DriverManger method
        connection = DriverManager.getConnection(connectionString, userName, password);

        // Returning connection object
        return connection;
    }

    /**
     * The method gets the set of the schema names available in user database.
     * excluding the system databases in mysql server. Here the schema names are
     * stored in tree structure for better searching. Since searching in tree is
     * O(log n).
     *
     * @return set of database names.
     */
    private TreeSet<String> getDatabaseNames() throws Exception {

        // Storing the database name in a tree so the searching is O(log n).
        TreeSet<String> databaseNameSet = null;

        // Creating a connection variable.
        Connection connection = null;

        // Calling the connectionOpen method to get the connection of the user database.
        connection = connectionOpen();

        // Database names to be excluded.
        String informationSchema = "information_schema";
        String sys = "sys";
        String performanceSchema = "performance_schema";
        String mySql = "mysql";

        // Query to get the database names from the mysql server excluding system
        // databases.
        String query = "select SCHEMA_NAME as 'Database' from information_schema.SCHEMATA where SCHEMA_NAME not in (?, ?, ?, ?);";

        // Creating a prepared statement to execute sql query in database.
        PreparedStatement statement = connection.prepareStatement(query);

        // Setting the designated parameters with the values.
        statement.setString(1, informationSchema);
        statement.setString(2, sys);
        statement.setString(3, performanceSchema);
        statement.setString(4, mySql);

        // Retrieving the database name from the mysql server.
        ResultSet resultSet = statement.executeQuery();

        // Initializing the databaseNameSet which is a tree so when the searching for
        // names happen it will
        // of O(log n).
        databaseNameSet = new TreeSet<>();

        // Retrieving the database name from mysql server and adding it to the
        // databaseNameSet.
        while (resultSet.next())
            databaseNameSet.add(resultSet.getString(1));

        connection.close();

        // Returning the name of the databases.
        return databaseNameSet;
    }

    /**
     * The below method retrieves the table name from the selected database.
     * 
     * @param databaseName is the name of database.
     * 
     * @return a TreeSet containing of table names.
     */
    private TreeSet<String> getTableSet(String databaseName) throws Exception {

        // Creating a connection variable.
        // Calling the connectionOpen method to get the connection of the user database.
        Connection connection = connectionOpen();

        // Query to get the table names from the specified database.
        String queryTable = "select TABLE_NAME from information_schema.TABLES where TABLE_SCHEMA = ? AND TABLE_TYPE = 'BASE TABLE'";

        // Creating a prepared statement to execute sql query in database.
        PreparedStatement statement = connection.prepareStatement(queryTable);

        // Setting the designated parameters with the values.
        statement.setString(1, databaseName);

        // Retrieving the table name from the mysql server.
        ResultSet resultSetTable = statement.executeQuery();

        // Storing the table name in a tree so the searching is O(log n).
        // Initializing the tableNameSet which is a tree so when so when the searching
        // for names happen it will
        // of O(log n).
        TreeSet<String> tableNameSet = new TreeSet<>();

        // Retrieving the database name from mysql server and adding it to the
        // tableNameSet.
        while (resultSetTable.next())
            tableNameSet.add(resultSetTable.getString(1));

        // Closing the connection.
        connection.close();

        // Returning the TreeSet which contains table name to the caller.
        return tableNameSet;
    }

    /**
     * The method gets the set of table names from specified database. Here the
     * schema names are stored in map and table names are stored in tree structure
     * for better searching.Since searching in tree is O(log n).
     *
     * @return the map of schema names with schema names as key and values will be
     *         table names as tree.
     */
    private LinkedHashMap<String, TreeSet<String>> getDbTablesMap() throws Exception {

        // Storing the schema name in hashmap and making schema name it in a key
        LinkedHashMap<String, TreeSet<String>> dbNamesMap = new LinkedHashMap<>();

        // Retrieving the databases.
        // The user is selecting all the user database.
        TreeSet<String> databaseNameSet = getDatabaseNames();

        // Iterating through the selected database.
        for (String databaseName : databaseNameSet) {

            // Retrieving the table names in TreeSet from the method.
            TreeSet<String> tableNameSet = getTableSet(databaseName);

            // Adding the key which is schema name and key would be it's particular table.
            dbNamesMap.put(databaseName, tableNameSet);
        }

        // Returning the map.
        return dbNamesMap;
    }

    /**
     * The method will retrieve all columns and it's properties based on the table
     * given by user. Here, database metadata will be read and map will be populated
     * based on the tables and columns. Example : [dbName, ->[tableName , ->
     * [columnName, ->[columnProperty, propertyValue]] ] ]
     *
     * @param dbNamesMap is map of database where schema name is key and value is
     *                   tableNames in a tree.
     *
     * @return map of table with it's columns.
     */
    private LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, String>>>> getColumnsMetaData(
            LinkedHashMap<String, TreeSet<String>> dbNamesMap) throws Exception {

        // Creating a connection variable.
        Connection connection = null;

        // Creating a formalParameters instance to be used to pass argument list through
        // method.
        FormalParametersClass formalParameters = new FormalParametersClass();

        // Checking if the dbReaderNamesMap is null and is empty.
        if (null == dbNamesMap || dbNamesMap.isEmpty())

            // Retrieving all the database and their tables respectively.
            dbNamesMap = getDbTablesMap();

        // Calling the connectionOpen method to get the connection of the user database.
        connection = connectionOpen();

        // Query to get the column names and it's properties from the tables.
        String queryColumn = "select * from information_schema.COLUMNS where TABLE_NAME = ? and TABLE_SCHEMA = ? order by ordinal_position;";

        // Creating a iterator to iterate through dbReaderNamesMap.
        Iterator<Entry<String, TreeSet<String>>> dbNamesMapIterator = dbNamesMap.entrySet().iterator();

        // Checking if it has next key the iterate through the map.
        while (dbNamesMapIterator.hasNext()) {

            // currentKey has next element for iteration.
            Entry<String, TreeSet<String>> currentKey = dbNamesMapIterator.next();

            // Defining a TreeSet variable.
            TreeSet<String> tableNameList = null;

            // Retrieving the table name from the map.
            TreeSet<String> currentKeyValue = currentKey.getValue();

            // Checking if the currentKeyValue is empty or null.
            if (null == currentKeyValue || currentKeyValue.isEmpty())

                // Retrieving the TreeSet which contains table name from the method.
                tableNameList = getTableSet(currentKey.getKey());
            else

                // Retrieving the value which is table name from the TreeSet.
                tableNameList = currentKeyValue;

            // Creating a prepared statement to execute sql query in database.
            PreparedStatement statement = connection.prepareStatement(queryColumn);

            // Creating a map which will store table and it's column associated with the db.
            LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, String>>> tableMap = new LinkedHashMap<>();

            // Creating a map which will store column and it's properties associated with
            // the table.
            LinkedHashMap<String, LinkedHashMap<String, String>> columnMap;

            // Creating a map which will store the names which will be used by object graph
            // creator and object graph reader.
            formalParameters.commonNodesNameMap = getCommonNodesName();

            // Creating a map which will store the values which will be used by
            // ColumnMetadata class.
            formalParameters.commonValueMap = getCommonValueMap();

            // Iterating through the table name from database.
            for (String tableName : tableNameList) {

                // Setting the designated parameters with the values
                statement.setString(1, tableName);
                statement.setString(2, currentKey.getKey());

                // The variable will store the columns table.
                // Retrieving the columns and it's property from the table
                formalParameters.resultSetColumn = statement.executeQuery();

                // Initializing the empty map.
                columnMap = new LinkedHashMap<>();

                // Iterating through the column from columns tables.
                while (formalParameters.resultSetColumn.next()) {
                    // Creating a map which will store column property name and it's properties
                    // associated with the table.
                    // Initializing the empty map.
                    formalParameters.columnPropertyMap = new LinkedHashMap<>();

                    // Getting the metadata of columns table.
                    formalParameters.metaData = formalParameters.resultSetColumn.getMetaData();

                    // Getting the number of column present in the column table.
                    int columnCount = formalParameters.metaData.getColumnCount();

                    // Iterating through the columns table.
                    for (int count = 1; count <= columnCount; count++) {

                        formalParameters.count = count;

                        // Calling the method to add properties of particular column in a way which will
                        // be common for ObjectGraphCreator
                        // and ObjectGraphReader.
                        formalParameters.columnPropertyMap = addToColumnPropertyMap(formalParameters);
                    }

                    // The columnMap will have column name as key and it's value would be the
                    // columnPropertyMap which has the
                    // properties of particular column.
                    columnMap.put(formalParameters.resultSetColumn.getString("COLUMN_NAME"),
                            formalParameters.columnPropertyMap);
                }
                // This tableMap will have the table name as the key and the value would the
                // columnMap
                // which has the metadata of column.
                tableMap.put(tableName, columnMap);
            }
            // This dbReaderMap will have the schema name as the key and the value would the
            // tableMap
            // which has the metadata of table and it's column.
            dbRawMetaData.dbReaderMap.put(currentKey.getKey(), tableMap);
        }

        // Closing the connection.
        connection.close();

        // Returning the database map.
        return dbRawMetaData.dbReaderMap;
    }

    /**
     * This method adds the columnPropertyMap keys and values where the key is going
     * to be the common node to create Object Graph XML file and value would be
     * specified value for Object Graph XML File. This is useful for reading
     * multiple SQL software and passing same type of XML file to the Code Generator
     * etc.
     *
     * @param formalParameters is class which stores the argument list. The
     *                         arguments used here are : resultSetColumn stores the
     *                         table of the column.
     *
     *                         metaData stores the metadata of column table.
     *
     *                         count used for iterating through the fields.
     *
     *                         commonNodesNameMap a map which contains the common
     *                         nodes which will be used to create XML File.
     *
     *                         columnPropertyMap a map for adding keys and values
     *                         where key is the common nodes and values will be the
     *                         specified value for XML File.
     *
     *                         commonValueMap a map which contains the specified
     *                         values which will be used to create XML File.
     *
     * @return a map which contains the column metadata.
     */
    private LinkedHashMap<String, String> addToColumnPropertyMap(FormalParametersClass formalParameters) throws Exception {

        // Name of the column.
        String columnName = formalParameters.metaData.getColumnName(formalParameters.count);

        // Value of the column.
        Object columnValue = formalParameters.resultSetColumn.getString(formalParameters.count);

        // Checking if the column name exist in the commonNodesNameMap.
        if (formalParameters.commonNodesNameMap.containsKey(columnName)) {
            // Storing the value to check it's type.
            Object checkCommonNodesNameMapValue = formalParameters.commonNodesNameMap.get(columnName);

            // Checking if the value is type of String.
            if (checkCommonNodesNameMapValue instanceof String)
                // Checking if the commonValueMap contains the particular value as key
                if (formalParameters.commonValueMap.containsKey(columnValue)) {
                    // Adding the key as the commonNodesNameMap value which will be used as common
                    // XML node and value would
                    // common Value for the ColumnMetaData class.
                    formalParameters.columnPropertyMap.put(checkCommonNodesNameMapValue.toString(),
                            formalParameters.commonValueMap.get(columnValue));

                    // Returning the columnPropertyMap to the caller.
                    return formalParameters.columnPropertyMap;
                }

            // Checking if the value is type of ArrayList because of COLUMN_KEY and EXTRA.
            if (checkCommonNodesNameMapValue instanceof ArrayList<?>) {
                // Storing the value in a variable.
                String valueInString = columnValue.toString();

                // Converting valueInString into an array using split method where "," is the
                // delimiter.
                String[] valueInArray = valueInString.split("//,");

                // Converting the array into a arrayList.
                ArrayList<String> valueList = new ArrayList<>(Arrays.asList(valueInArray));

                // Iterating through the list.
                for (String item : (ArrayList<String>) checkCommonNodesNameMapValue)
                    // Adding the keys to columnPropertyMap with default value as false.
                    formalParameters.columnPropertyMap.put(item, "false");
                // Iterating through the value list.
                for (String item : valueList)
                    // Checking if the key is present in commonValueMap.
                    if (formalParameters.commonValueMap.containsKey(item))
                        // Replacing the default value from columnPropertyMap if key matches.
                        formalParameters.columnPropertyMap.replace(formalParameters.commonValueMap.get(item), "true");
                // Returning the columnPropertyMap to the caller.
                return formalParameters.columnPropertyMap;
            }
            // Adding a key and value based on
            formalParameters.columnPropertyMap.put(checkCommonNodesNameMapValue.toString(), (String) columnValue);
        }

        // Returning the columnPropertyMap to the caller.
        return formalParameters.columnPropertyMap;
    }

    /**
     * This method creates a LinkedHashMap which will store the column names of column
     * table as key and value would be used to make a common XML node to create and
     * read XML File.
     *
     * @return a LinkedHashMap where key is the column name and value would be the common
     *         node.
     */
    private LinkedHashMap<String, Object> getCommonNodesName() {
        // Creating a LinkedHashMap which will store the column names of column table
        // as key and value would be used to make a common XML node to create and
        // read XML File.
        LinkedHashMap<String, Object> columnNamesMap = new LinkedHashMap<>();

        // Creating a list which will store the multiple column key names.
        ArrayList<String> columnKeyNameList = getListOfColumnKeyNodeNames();

        // Creating a list which will store the extra.
        ArrayList<String> extraList = getListOfExtraNodeNames();

        // Adding the key and value.
        columnNamesMap.put("TABLE_CATALOG", "table_catalog");
        columnNamesMap.put("IS_NULLABLE", "is_nullable");
        columnNamesMap.put("TABLE_NAME", "table_name");
        columnNamesMap.put("TABLE_SCHEMA", "database_name");
        columnNamesMap.put("COLUMN_NAME", "column_name");
        columnNamesMap.put("COLUMN_KEY", columnKeyNameList);
        columnNamesMap.put("CHARACTER_OCTET_LENGTH", "character_length_bytes");
        columnNamesMap.put("CHARACTER_MAXIMUM_LENGTH", "character_maximum_length");
        columnNamesMap.put("NUMERIC_PRECISION", "numeric_precision");
        columnNamesMap.put("PRIVILEGES", "privileges");
        columnNamesMap.put("COLUMN_COMMENT", "column_comment");
        columnNamesMap.put("DATETIME_PRECISION", "dateTime_precision");
        columnNamesMap.put("COLLATION_NAME", "collation_name");
        columnNamesMap.put("NUMERIC_SCALE", "numeric_scale");
        columnNamesMap.put("COLUMN_TYPE", "column_type");
        columnNamesMap.put("GENERATION_EXPRESSION", "generation_expression");
        columnNamesMap.put("ORDINAL_POSITION", "column_position");
        columnNamesMap.put("DATA_TYPE", "data_type");
        columnNamesMap.put("CHARACTER_SET_NAME", "character_set_name");
        columnNamesMap.put("COLUMN_DEFAULT", "column_default");
        columnNamesMap.put("EXTRA", extraList);

        // Returning the columnNamesMap to the caller of method.
        return columnNamesMap;
    }

    /**
     * This method creates a list which will store the multiple column key names.
     * whether it is a primary key, unique key etc.
     *
     * @return the list which will be used for checking is it a primary key or
     *         unique key etc.
     */
    private ArrayList<String> getListOfColumnKeyNodeNames() {
        // Creating a list which will store the multiple column key names.
        ArrayList<String> columnKeyNameList = new ArrayList<>();
        columnKeyNameList.add("is_primaryKey");
        columnKeyNameList.add("is_uniqueKey");
        columnKeyNameList.add("is_multipleKey");

        // Returning the list to the caller of method.
        return columnKeyNameList;
    }

    /**
     * This method creates a list which will store the extra where extra can auto
     * increment, virtual generated columns etc.
     *
     * @return the list which will be used for checking is it auto increment or
     *         virtual generated etc.
     */
    private ArrayList<String> getListOfExtraNodeNames() {
        // Creating a list which will store the extra.
        ArrayList<String> extraList = new ArrayList<>();
        extraList.add("is_auto_increment");
        extraList.add("is_virtual_generated");
        extraList.add("is_virtual_stored");
        extraList.add("is_default_generated");
        extraList.add("current_timeStamp");

        // Returning the extra list to the caller of method.
        return extraList;
    }

    /**
     * This method a map which will store the value as key which is the column data
     * and value would be which is required by columnMetaData class.
     *
     * @return a LinkedHashMap where key is the column value and value would be used by
     *         the ColumnMetaData class.
     */
    private LinkedHashMap<String, String> getCommonValueMap() {
        // Creating a map which will store the value as key and value would be which is
        // required by columnMetaData class.
        LinkedHashMap<String, String> commonValueMap = new LinkedHashMap<>();

        // Adding the key and value.
        commonValueMap.put("YES", "true");
        commonValueMap.put("NO", "false");
        commonValueMap.put("PRI", "is_primaryKey");
        commonValueMap.put("UNI", "is_uniqueKey");
        commonValueMap.put("MUL", "is_multiple");
        commonValueMap.put("auto_increment", "is_auto_increment");
        commonValueMap.put("VIRTUAL", "is_virtual_generated");
        commonValueMap.put("PERSISTENT", "is_virtual_stored");
        commonValueMap.put("DEFAULT_GENERATED", "is_default_generated");
        commonValueMap.put("CURRENT_TIMESTAMP", "current_timeStamp");

        // Returning the map to the caller of the method.
        return commonValueMap;
    }
}
