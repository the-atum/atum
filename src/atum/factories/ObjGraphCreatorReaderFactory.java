
/*******************************************************************************
 * Author:        Pawan Vyas (PBMV)
 * Email:         pawan.vyas.gitlab@gmail.com
 * 
 * Project:       Atum/Factories
 * File:          ObjGraphCreatorReaderFactory.java
 * 
 * Description:
 *      Abstract Factory class which produces objects of IObjectGraphCreator and
 *      IObjectGraphReader types.
 * 
 * Purpose:
 *      Used for getting concrete implementations of IObjectGraphCreator and
 *      IObjectGraphReader interfaces based on passed document-format type, eg:
 *      XML, JSON, and so on.
 * 
 * Revision History:
 *      2020-May-30 (PBMV): File created.
 *      2020-May-30 (PBMV): Updated methods to static.
 *      2020-May-30 (PBMV): Moved to atum.factories package.
 *      2020-May-30 (PBMV): Fixed import errors due to package restructuring.
 *******************************************************************************/

package atum.factories;

import java.util.HashMap;
import atum.interfaces.IObjectGraphCreator;
import atum.interfaces.IObjectGraphReader;
import atum.utils.AtumMapRetriever;

/**
 * Abstract Factory class which produces objects of IObjectGraphCreator and
 * IObjectGraphReader types.
 */
public class ObjGraphCreatorReaderFactory {

    /**
     * Constant for holding the default node-key name from the
     * AtumClassesPropertyFile for loading the map of ObjGraphCreator.
     */
    private static final String OBJCREATOR_COMPONENT_KEY = "objgraphcreators";

    /**
     * Constant for holding the default node-key name from the
     * AtumClassesPropertyFile for loading the map of ObjGraphReaders.
     */
    private static final String OBJREADER_COMPONENT_KEY = "objgraphreaders";

    /**
     * Map for holding the keys and class names for currently available
     * ObjGraphCreators.
     */
    private static HashMap<String, String> ObjGraphCreatorMap;

    /**
     * Map for holding the keys and class names for currently available
     * ObjGraphReaders.
     */
    private static HashMap<String, String> ObjGraphReaderMap;

    /**
     * Static constructor to initialize the static fields of this class.
     */
    static {

        // Using AtumMapRetriever utility class to get the map corresponding to
        // ObjGraphCreator from the AtumClassesPropertyFile.
        ObjGraphCreatorMap = AtumMapRetriever.GetCastedComponentMap(OBJCREATOR_COMPONENT_KEY);

        // Using AtumMapRetriever utility class to get the map corresponding to
        // ObjGraphReader from the AtumClassesPropertyFile.
        ObjGraphReaderMap = AtumMapRetriever.GetCastedComponentMap(OBJREADER_COMPONENT_KEY);
    }

    /**
     * Method which returns a new instance of a concrete implementation of
     * IObjectGraphCreator, based on the passed documentFormatType passed.
     * 
     * @param documentFormatType is the type of the document format which will be
     *                           used to determine which IObjectGraphCreator
     *                           concrete implementation is instantiated. Eg: XML,
     *                           JSON, etc. This parameter is case-insensitive.
     * @return A new instance of a concrete implementation of IObjectGraphCreator
     *         type.
     * @throws Exception If passed documentFormatType was not found, or if class
     *                   loading threw an exception.
     */
    public static IObjectGraphCreator GetObjGraphCreator(String documentFormatType) throws Exception {

        // Converting the passed documentFormatType to lower case all the keys in ObjGraphCreatorMap are
        // lower-case.
        documentFormatType = documentFormatType.toLowerCase();

        // Checking if documentFormatType exists in the ObjGraphCreatorMap.
        boolean documentFormatTypeExists = ObjGraphCreatorMap.containsKey(documentFormatType);

        // If documentFormatType doesn't exist in the map, then throwing an appropriate
        // exception.
        if (!documentFormatTypeExists)
            throw new Exception("FATAL: Concrete implementation of " + documentFormatType
                    + " for an IObjectGraphCreator doesn't exist yet.");

        // If documentFormatType exists, then getting its corresponding classname.
        String className = ObjGraphCreatorMap.get(documentFormatType);

        // Loading, creating and returning an instance of the concrete
        // documentFormatType class.
        return (IObjectGraphCreator) Class.forName(className).getConstructor().newInstance();
    }

    /**
     * Method which returns a new instance of a concrete implementation of
     * IObjectGraphReader, based on the passed documentFormatType passed.
     * 
     * @param documentFormatType is the type of the document format which will be
     *                           used to determine which IObjectGraphReader concrete
     *                           implementation is instantiated. Eg: XML, JSON, etc.
     *                           This parameter is case-insensitive.
     * @return A new instance of a concrete implementation of IObjectGraphReader
     *         type.
     * @throws Exception If passed documentFormatType was not found, or if class
     *                   loading threw an exception.
     */
    public static IObjectGraphReader GetObjGraphReader(String documentFormatType) throws Exception {

        // Converting the passed documentFormatType to lower case all the keys in ObjGraphReaderMap are
        // lower-case.
        documentFormatType = documentFormatType.toLowerCase();

        // Checking if documentFormatType exists in the ObjGraphReaderMap.
        boolean documentFormatTypeExists = ObjGraphReaderMap.containsKey(documentFormatType);

        // If documentFormatType doesn't exist in the map, then throwing an appropriate
        // exception.
        if (!documentFormatTypeExists)
            throw new Exception("FATAL: Concrete implementation of " + documentFormatType
                    + " for an IObjectGraphReader doesn't exist yet.");

        // If documentFormatType exists, then getting its corresponding classname.
        String className = ObjGraphReaderMap.get(documentFormatType);

        // Loading, creating and returning an instance of the concrete
        // documentFormatType class.
        return (IObjectGraphReader) Class.forName(className).getConstructor().newInstance();
    }
}
