
/*******************************************************************************
 * Author:        Pawan Vyas (PBMV)
 * Email:         pawan.vyas.gitlab@gmail.com
 * 
 * Project:       AtumDemo/Factories
 * File:          DBReaderFactory.java
 * 
 * Description:
 *      A Factory class which creates instances of concrete IDBReader
 *      implementations.
 * 
 * Purpose:
 *      To get concrete IDBReader instances as and when requested based on the
 *      supplied source DataBase type.
 * 
 * Revision History:
 *      2019-Apr-30 (PBMV): File Created.
 *      2019-Sep-26 (PBMV): AtumMain and subsequent interface and/or factory
 *                          related changes.
 *      2020-May-25 (PBMV): Standard code auto-formatting using RedHat's Java
 *                          Code-Formatter extension added with the Java
 *                          Language pack.
 *      2020-May-25 (PBMV): Updated this Factory's call to be static and made
 *                          subsequent changes. Also removed injecting
 *                          ConnectionParameter via constructor.
 *      2020-May-30 (PBMV): Moved to atum.factories package.
 *      2020-May-30 (PBMV): Fixed import errors due to package restructuring.
*******************************************************************************/

package atum.factories;

import java.util.HashMap;
import atum.interfaces.IDBReader;
import atum.utils.AtumMapRetriever;

/**
 * Factory class which creates DataBase-Reader objects based on passed DataBase
 * Type.
 */
public class DBReaderFactory {

    /**
     * Constant to hold the default key name for "dbreaders" key in the
     * PropertyFile.
     */
    private static final String COMPONENT_KEY = "dbreaders";

    /**
     * Represents map for available concrete implementations of IDBReader interface.
     * Each key is the available DataBase type, and its value is the name of the
     * concrete implementation of its class.
     */
    private static HashMap<String, String> DBReaderMap;

    /**
     * Static Constructor block to initialize the static fields of this class.
     */
    static {

        // Getting the CastedComponent map from the AtumMapRetriever.
        DBReaderMap = AtumMapRetriever.GetCastedComponentMap(COMPONENT_KEY);
    }

    private DBReaderFactory() {

    }

    /**
     * This factory method creates instances of IDBReader types and returns them.
     * Which specific object is to be created is decided based on the passed
     * sourceDbType.
     * 
     * @param sourceDbType is the type of the source database from which its
     *                     meta-data is to be read.
     * @return an instance of IDBReader object.
     * @throws Exception If sourceDbType is not provided or doesn't exist.
     */
    public static IDBReader GetDBReader(String sourceDbType) throws Exception {

        // Convert the passed sourceDbType to lower case all the keys in DBReaderMap are
        // lower-case.
        sourceDbType = sourceDbType.toLowerCase();

        // Check if requested sourceDbType exists in the DBReaderMap or not.
        boolean sourceDbTypeExists = DBReaderMap.containsKey(sourceDbType);

        // If sourceDbType doesn't exist, then we throw an exception with an appropriate
        // error message.
        if (!sourceDbTypeExists)
            throw new Exception(
                    "FATAL: " + sourceDbType + " is not a valid <DB_TO_READ>, or it's implementation doesn't exist.");

        // If concreteType exists, then getting it's full
        // class name from the DBReaderMap.
        String className = DBReaderMap.get(sourceDbType);

        // Loading the class and returning a new instance using parameterized
        // constructor by injected it with connParam parameter object.
        return (IDBReader) Class.forName(className).getConstructor().newInstance();
    }
}
