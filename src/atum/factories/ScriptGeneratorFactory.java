
/** *****************************************************************************
 * Author:        Pawan Vyas (PBMV)
 * Email:         pawan.vyas.gitlab@gmail.com
 *
 * Project:       AtumDemo/Factories
 * File:          ScriptGeneratorFactory.java
 *
 * Description:
 *      Factory class which creates Script-Generator objects based on passed DataBase
 *      Type.
 *
 * Purpose:
 *      This class will be used to create objects of IScriptGenerator type.
 *
 * Revision History:
 *      2019-Sep-28 (PBMV): File Created.
 *      2020-May-25 (PBMV): Standard code auto-formatting using RedHat's Java
 *                          Code-Formatter extension added with the Java
 *                          Language pack.
 *      2020-May-25 (PBMV): Updated GetScriptGenerator method to no longer inject
 *                          ObjectGraph via constructor.
 *      2020-May-30 (PBMV): Moved to atum.factories package.
 *      2020-May-30 (PBMV): Fixed import errors due to package restructuring.
 ****************************************************************************** */

package atum.factories;

import java.util.HashMap;

import atum.interfaces.IScriptGenerator;
import atum.utils.AtumMapRetriever;

/**
 * Factory class which creates Script-Generator objects based on passed DataBase
 * Type.
 */
public class ScriptGeneratorFactory {

    /**
     * Constant to hold the default key name for "scriptgens" key in the
     * PropertyFile.
     */
    private static final String COMPONENT_KEY = "scriptgens";

    /**
     * Represents map for available concrete implementations of IScriptGenerator
     * interface. Each key is the available target database type, and its value is
     * the name of the concrete implementation of its class.
     */
    private static HashMap<String, String> ScriptGeneratorMap;

    /** Static constructor to initialize the static fields of this class */
    static {
        ScriptGeneratorMap = AtumMapRetriever.GetCastedComponentMap(COMPONENT_KEY);
    }

    /** */
    private ScriptGeneratorFactory() {

    }

    /**
     * This factory method creates instances of IScriptGenerator types and returns
     * them. Which specific object is to be created is decided based on the passed
     * targetDbType.
     * 
     * @param targetDbType is the type of the target DataBase for which its
     *                     appropriate IScriptGenerator is to be instantiated.
     * @return an instance of IScriptGenerator object.
     * @throws Exception If targetDbType is not provided, or doesn't exist.
     */
    public static IScriptGenerator GetScriptGenerator(String targetDbType) throws Exception {

        // Converting the passed targetDbType to lower case all the keys in ScriptGeneratorMap are
        // lower-case.
        targetDbType = targetDbType.toLowerCase();

        // Check if requested targetDbType exists in the ScriptGeneratorMap or not.
        boolean targetDbTypeExists = ScriptGeneratorMap.containsKey(targetDbType);

        // If targetDbType doesn't exist in ScriptGeneratorMap,
        // then we throw an exception with an appropriate error message.
        if (!targetDbTypeExists)
            throw new Exception(
                    "FATAL: " + targetDbType + " is not a valid <DB_TO_GEN>, or it's implementation doesn't exist.");

        // If targetDbType exists in the ScriptGeneratorMap,
        // then getting its full class name.
        String className = ScriptGeneratorMap.get(targetDbType);

        // Loading the targetDbType class and returning its type-casted instance.
        return (IScriptGenerator) Class.forName(className).getConstructor().newInstance();
    }
}
