
/*******************************************************************************
 * Author:        Pawan Vyas (PBMV)
 * Email:         pawan.vyas.gitlab@gmail.com
 * 
 * Project:       AtumDemo/Factories
 * File:          CodeGeneratorFactory.java
 * 
 * Description:
 *      Factory class which creates Code-Generator objects based on passed DataBase
 *      Type and its corresponding Code Language Type.
 * 
 * Purpose:
 *      To allow creating specific ICodeGenerator instances.
 *  
 * Revision History:
 *      2019-May-19 (PBMV): File created.
 *      2019-Sep-26 (PBMV): AtumMain and subsequent interface and/or factory
 *                          related changes.
 *      2020-May-24 (IVNR): Created a static block for typed map.
 *      2020-May-24 (PBMV): Standard code auto-formatting using RedHat's Java
 *                          Code-Formatter extension added with the Java
 *                          Language pack.
 *      2020-May-24 (PBMV): Updated DbCodeGeneratorFactory to now directly
 *                          create ICodeGenerator instances rather than creating
 *                          other factories. This class no longer implements an
 *                          Interface as that requires creating its instances
 *                          which was deemed unnecessary. Made it static.
 *      2020-May-25 (PBMV): Renamed to CodeGeneratorFactory.
 *      2020-May-25 (PBMV): Updated GetCodeGenerator method to no longer inject
 *                          ObjectGraph via constructor.
 *      2020-May-30 (PBMV): Moved to atum.factories package.
 *      2020-May-30 (PBMV): Fixed import errors due to package restructuring.
 *******************************************************************************/

package atum.factories;

import java.util.HashMap;
import java.util.Map;
import atum.interfaces.ICodeGenerator;
import atum.utils.AtumMapRetriever;

/**
 * Factory class which creates Code-Generator objects based on passed DataBase
 * Type and its corresponding Code Language Type.
 */
@SuppressWarnings("unchecked")
public class CodeGeneratorFactory {

    /**
     * Constant to hold the default key name for "dbcodegens" key in the
     * PropertyFile.
     */
    private static final String DB_CODE_GEN_KEY = "dbcodegens";

    /**
     * Represents the internal map for DbCodeGeneratorFactory, where each key in the
     * outer map is a supported Target DataBase name, and its value is an internal
     * map which further contains the supported Code Languages name against their
     * concrete class names for CodeGenerator classes that are implementations of
     * ICodeGenerator interface.
     */
    private static HashMap<String, HashMap<String, String>> DbCodeGeneratorMap;

    /**
     * Static constructor block for initializing the static members of this class.
     */
    static {

        // Instantiating DbCodeGeneratorMap as a new empty HashMap.
        DbCodeGeneratorMap = new HashMap<>();

        // Creating a local variable to hold uncasted Component Map.
        HashMap<String, Object> uncastedComponentMap;

        // Retrieving the uncastedMap of atum Property file from AtumMapRetriever
        // against the DB_CODE_GEN_KEY.
        uncastedComponentMap = AtumMapRetriever.GetUncastedComponentMap(DB_CODE_GEN_KEY);

        // Iterating through the uncastedMap.
        for (Map.Entry<String, Object> entry : uncastedComponentMap.entrySet()) {

            // Instantiating the innerMap which will be used as value for dbCodeGenMap.
            HashMap<String, String> innerMap = new HashMap<String, String>();

            // Creating entryValueMap variable to hold entry.getValue();
            HashMap<String, Object> entryValueMap = null;

            // Checking if the entry.getValue() is type of HashMap<String, Object>.
            if (entry.getValue() instanceof HashMap<?, ?>)

                // Retrieving the HashMap Of type <String, Object> from entry's value.
                entryValueMap = (HashMap<String, Object>) entry.getValue();

            // Iterating through the entryValueMap which is a HashMap of type <String,
            // Object>.
            for (Map.Entry<String, Object> innerEntry : entryValueMap.entrySet()) {

                // Retrieving the innerEntry map key.
                String innerEntryKey = innerEntry.getKey();

                // Retrieving the innerEntry map value.
                Object innerEntryValue = innerEntry.getValue();

                // Checking if the key and value of the innerEntry map is type of String.
                if (innerEntryKey instanceof String && innerEntryValue instanceof String) {

                    // Adding the Key and value to the innerMap from innerEntry.
                    innerMap.put(innerEntryKey, (String) innerEntryValue);
                }
            }

            // In dbCodeGenMap we are adding a key which is type string from the
            // uncastedComponentMap and
            // value is new HashMap of type <String, String>.
            DbCodeGeneratorMap.put(entry.getKey(), innerMap);
        }
    }

    /** */
    private CodeGeneratorFactory() {

    }

    /**
     * This factory method creates instances of ICodeGenerator types and returns
     * them. Which specific object is to be created is decided based on the passed
     * targetDbType and codeLangType parameters.
     * 
     * @param targetDbType is the type of the target DataBase for which its
     *                     appropriate code-language's ICodeGenerator is to be
     *                     instantiated.
     * @param codeLangType is the type of the language corresponding to which the
     *                     ICodeGenerator will be instantiated.
     * @return an instance of ICodeGenerator object.
     * @throws Exception If DbType, or CodeLang is not provided or either do no
     *                   exist. Or instantiation of an ICodeGenerator object fails
     *                   due to Class.forName().newInstance() method call.
     */
    public static ICodeGenerator GetCodeGenerator(String targetDbType, String codeLangType) throws Exception {

        // Convert the passed targetDbType to lower case all the keys in CodeGenMap for DBMaps are
        // lower-case.
        targetDbType = targetDbType.toLowerCase();
        
        // Similarly convert the passed codeLangType to lower case all the keys in DBMaps for CodeLangMap are
        // lower-case.
        codeLangType = codeLangType.toLowerCase();

        // Checking if requested dbTypeExists exists in the
        // DbCodeGeneratorFactoriesMap.
        boolean dbTypeExists = DbCodeGeneratorMap.containsKey(targetDbType);

        // If targetDbType doesn't exists in the DbCodeGeneratorMap,
        // then we throw an exception with an appropriate error message.
        if (!dbTypeExists)
            throw new RuntimeException("FATAL: " + targetDbType
                    + " is not a supported DataBase for which language code can be generated yet.");

        // If targetDbType exists in the DbCodeGeneratorMap,
        // then getting whether it's internal CodeGenMap contains the codeLangType key
        // ot not.
        boolean codeLangTypeExists = DbCodeGeneratorMap.get(targetDbType).containsKey(codeLangType);

        // If codeLangType doesn't exists in the internal CodeGenMap of the
        // targetDbType,
        // then we throw an exception with an appropriate error message.
        if (!codeLangTypeExists)
            throw new RuntimeException(
                    "FATAL: " + codeLangType + " is not a supported language for which code can be generated yet.");

        // If targetDbType and codeLangType exist, then getting the actual concrete
        // class name for the
        // appropriate ICodeGenerator implementation from the DbCodeGeneratorMap.
        String className = DbCodeGeneratorMap.get(targetDbType).get(codeLangType);

        // Loading the selected ICodeGenerator class and returning its type-casted
        // instance.
        return (ICodeGenerator) Class.forName(className).getConstructor().newInstance();
    }
}
