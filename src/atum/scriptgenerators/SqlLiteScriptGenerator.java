
/** *****************************************************************************
 * Author:        Pawan Vyas (PBMV)
 * Email:         pawan.vyas.gitlab@gmail.com
 *
 * Project:       Atum/ScriptGenerator
 * File:          SqlLiteScriptGenerator.java
 *
 * Description:
 *      Concrete implementation of IScriptGenerator interface for generating
 *      table creation code SqlLite database.
 *
 * Purpose:
 *      Provides implementation for generating table code, note that SqlLite
 *      database does not have functionality of StoredProcedures, therefore
 *      the code which would ideally be in SPs, is instead written in the
 *      DataAccessLayer classes instead.
 *
 * Revision History:
 *      2019-Sep-28 (PBMV): File Created.
 *      2019-Sep-27 (IVNR): Added generateCreateScript() method.
 *      2020-May-25 (PBMV): Standard code auto-formatting using RedHat's Java
 *                          Code-Formatter extension added with the Java
 *                          Language pack.
 *      2020-May-25 (PBMV): IAtum removal and subsequent changes.
 *      2020-May-25 (PBMV): Added implementation for SetObjectGraph method and
 *                          related changes to move logic out from ctor.
 *      2020-May-30 (PBMV): Moved to atum.scriptgenerators package.
 *      2020-May-30 (PBMV): Fixed import errors due to package restructuring.
 ****************************************************************************** */

package atum.scriptgenerators;

import java.util.ArrayList;
import atum.interfaces.IScriptGenerator;
import atum.dataobjects.AtumColumnMetaData;
import atum.dataobjects.CodeFileInfo;
import atum.dataobjects.ObjectGraph;
import atum.utils.CodeFileWriter;

/**
 * Concrete implementation of IScriptGenerator interface for generating table
 * creation code SqlLite database.
 */
public class SqlLiteScriptGenerator implements IScriptGenerator {

    private ObjectGraph internalObjGraph;

    private ArrayList<String> dbNames;
    private ArrayList<String> tables;
    private ArrayList<String> columns;

    /**
     * Default constructor.
     */
    public SqlLiteScriptGenerator() {
        internalObjGraph = null;
        dbNames = null;
        tables = null;
        columns = null;
    }

    @Override
    public void SetObjectGraph(ObjectGraph objGraph) {
        internalObjGraph = objGraph;
        internalObjGraph.driverName = "";
        dbNames = internalObjGraph.getDatabases();
        tables = new ArrayList<>();
        columns = new ArrayList<>();
    }

    @Override
    public void generateScripts() throws Exception {

        if (null == internalObjGraph)
            throw new Exception("FATAL: ObjectGraph for this ScriptGenerator not initialized."
                    + "\nEnsure that ObjectGraph is set before this method is invoked using"
                    + "the IScriptGenerator.SetObjectGraph method.");

        generateCreateScript();
    }

    /**
     * Function to generate scripts for the passed dbReader.
     *
     * @return a string of generated scripts.
     */
    private void generateCreateScript() {

        for (String dbName : dbNames) {

            // Retrieving the list of tables from the dbReader.
            tables = internalObjGraph.getTables(dbName);

            // Defining the codeFileInfo object.
            CodeFileInfo codeFileInfo = new CodeFileInfo();
            codeFileInfo.langDirName = "SqlLite/";
            codeFileInfo.dbDirName = dbName + "/";
            codeFileInfo.subTypeDirName = "Scripts/";
            codeFileInfo.fileType = "_CREATE";
            codeFileInfo.fileExtension = ".sqlite";

            // Creating a variable to store the primary key name.
            String primaryKeyName = "";

            // Iterating through the table to generate scripts for every table.
            for (String table : tables) {

                // Defining stringBuilder object which will hold the generated script code
                // including new lines and tab indentations as and when code is generated.
                StringBuilder sbCode = new StringBuilder();

                sbCode.append("\n/**\n * AutoGenerated script for table: ").append(table).append(" in ").append(dbName)
                        .append(" database.\n */\n");
                sbCode.append("DROP TABLE IF EXISTS ").append(table).append(";\n");
                sbCode.append("CREATE TABLE ").append(table).append(" (\n");

                // Retrieving the columns from dbReader.
                columns = internalObjGraph.getColumns(dbName, table);

                // Iterating through the columns of the table.
                for (String column : columns) {

                    // Retrieving the columnMetaData of the particular column.
                    AtumColumnMetaData colData = internalObjGraph.dbMap.get(dbName).get(table).get(column);

                    sbCode.append("\t").append(colData.columnName).append(" ").append(colData.columnType);

                    // Checking if the column is not nullable.
                    if (!colData.isNullable)
                        // Appending NOT NULL to string builder.
                        sbCode.append(" NOT NULL");

                    // Checking if the column is auto increment.
                    if (colData.isAutoIncrement)
                        // Appending NOT NULL to string builder.
                        sbCode.append(" AUTO INCREMENT");

                    // Checking if the column is a primary key.
                    if (colData.isPrimaryKey)
                        // Assigning the column name to the variable.
                        primaryKeyName = colData.columnName;

                    sbCode.append(",\n");
                }

                // Checking if the name is present in the primaryKeyName variable.
                if ((null != primaryKeyName) && !primaryKeyName.isEmpty())
                    // Appending primary key to the script.
                    sbCode.append("\tPRIMARY KEY (").append(primaryKeyName).append(")\n");

                sbCode.append(")\n");

                // For each existing table in the list of tables, updating the codeFileInfo
                // parameter object's tableName to the current table name.
                // And updating its content field to the generated code in string.
                codeFileInfo.tableName = table;
                codeFileInfo.content = sbCode.toString();

                // Call CodeFileWriter's generateFile by passing it the fileInfo parameter
                // object.
                CodeFileWriter.generateFile(codeFileInfo);
            }
        }
    }

}
