
/** *****************************************************************************
 * Author:        Indresh Rawat (IVNR)
 * Email:         indreshrawat.gitlab@gmail.com
 *
 * Project:       AtumDemo/ObjectGraph
 * File:          AtumColumnMetaData.java
 *
 * Description:
 *       AtumColumnMetaData class will be used by the CodeGenerator to
 *       create code based on it.
 *
 * Purpose:
 *      It will be used to create code for the columns of the table.
 *
 * Revision History:
 *      2019-Sep-24 (IVNR): File created.
 * 		2019-Sep-26 (PBMV): Renamed to AtumColumnMetaData and added CPP
 * 							datatype field to store DB equivalent data-type.
 * 		2019-Nov-07 (IVNR): Added Setter to the fields of class.
 *      2020-May-24 (PBMV): Added class definition jdoc-comment for this class.
 *      2020-May-24 (PBMV): Standard code auto-formatting using RedHat's Java
 *                          Code-Formatter extension added with the Java
 *                          Language pack.
 *      2020-May-30 (PBMV): Moved to atum.dataobjects package.
 ****************************************************************************** */

package atum.dataobjects;

 /**
  * Class definition for each Column's meta data that is read by the DBReaders.
  */
public class AtumColumnMetaData {

    // Catalog of the table.
    public String tableCatalog;

    // Is the column nullable.
    public boolean isNullable;

    // Name of the table.
    public String tableName;

    // Name of the database.
    public String databaseName;

    // Name of the column.
    public String columnName;

    // Is the column Primary.
    public boolean isPrimaryKey;

    // Is the column Unique.
    public boolean isUniqueKey;

    // Is the column has multiple key can be foreign key or composite key.
    public boolean isMultipleKey;

    // Length of the column field in bytes.
    public int characterLengthBytes;

    // Maximum Length of the column field.
    public int characterMaximumLength;

    // The numeric precision if column is of type numeric
    public int numericPrecision;

    // Privileges of the user.
    public String privileges;

    // Comment in the column.
    public String columnComment;

    // Collation name,
    public String collationName;

    // The numeric scale if column is of type numeric.
    public int numericScale;

    // Type of the column.
    public String columnType;

    // The generated expression of the column.
    public String generationExpression;

    // The position of the column.
    public int columnPosition;

    // The data type of the column.
    public String dataType;

    // The character set of the column.
    public String characterSetName;

    // the Default value of the column.
    public String columnDefault;

    // Is the column auto incremented.
    public boolean isAutoIncrement;

    // Is the column virtual generated.
    public boolean isVirtualGenerated;

    // Is the column virtual stored.
    public boolean isVirtualStored;

    // Is the column default generated.
    public boolean isDefaultGenerated;

    public void setCharacterLengthBytes(String characterLengthBytes) {
        // Checking if it is empty.
        // Setting the field value as zero.
        // Setting the value.
        this.characterLengthBytes = (null == characterLengthBytes || characterLengthBytes.isEmpty()) ? 0 : Integer.parseInt(characterLengthBytes);
    }

    public void setCharacterMaximumLength(String characterMaximumLength) {
        // Checking if it is empty.
        // Setting the field value as zero.
        // Setting the value.
        this.characterMaximumLength = (null == characterMaximumLength || characterMaximumLength.isEmpty()) ? 0 : Integer.parseInt(characterMaximumLength);
    }

    public void setNumericPrecision(String numericPrecision) {
        // Checking if it is empty.
        // Setting the field value as zero.
        // Setting the value.
        this.numericPrecision = (null == numericPrecision || numericPrecision.isEmpty()) ? 0 : Integer.parseInt(numericPrecision);
    }

    public void setNumericScale(String numericScale) {
        // Checking if it is empty.
        // Setting the field value as zero.
        // Setting the value.
        this.numericScale = (null == numericScale || numericScale.isEmpty()) ? 0 : Integer.parseInt(numericScale);
    }

}
