
/** *****************************************************************************
 * Author: Pawan Vyas (PBMV)
 * Email: pawan.vyas.gitlab@gmail.com
 *
 * Project: Atum/Utils
 * File: CodeFileInfo.java
 *
 * Description:
 *      This is a pure data object class which stores information regarding a
 *      file that is to be written based on generated code.
 *
 * Purpose:
 *      This pure data object class simply acts as a parameter object to
 *      CodeFileWriter's utility function so that as and when needed more
 *      file information fields can be added to this class and hence
 *      modifications towards CodeFileWriter's utility method can be avoided.
 *
 * Revision History:
 *      2019-Sep-26 (PBMV): File Created.
 *      2020-May-24 (PBMV): Standard code auto-formatting using RedHat's Java
 *                          Code-Formatter extension added with the Java
 *                          Language pack.
 *      2020-May-25 (PBMV): Updated output directory naming.
 *      2020-May-30 (PBMV): Moved to atum.dataobjects package.
 *      2020-Dec-29 (PBMV): Changed to 24 hour(0-23) date format and added
 *                          optional field for further segregating output with
 *                          table-name directory as well. Also initialized
 *                          fields with empty-string, to avoid nulls.
 ****************************************************************************** */

package atum.dataobjects;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * This is a pure data object class which stores information regarding a file
 * that is to be written based on generated code. This pure data object class
 * simply acts as a parameter object to CodeFileWriter's utility function so
 * that as and when needed more file information fields can be added to this
 * class and hence modifications towards CodeFileWriter's utility method can be
 * avoided.
 */
public class CodeFileInfo {

    /**
     * Constant for representing an empty string.
     */
    private static final String EMPTY =  "";

    /**
     * is the location of the output directory where the file will be created.
     */
    public static final String outputDirName;

    static {
        LocalDateTime ldt = LocalDateTime.now();
        String runDate = DateTimeFormatter.ofPattern("yyyy-MM-dd_H-mm-ss", Locale.ENGLISH).format(ldt);
        outputDirName = "AtumOutputDir_T" + Thread.currentThread().getId() + "_D" + runDate + "/";
    }

    /**
     * is the name of the top level directory where this file will be placed. It
     * represents the name of the Database to club together all the files and
     * directories generated for that database under one directory.
     */
    public String dbDirName = EMPTY;

    /**
     * is the name of the language/DB-type for which this file is being created so
     * that code for each language is categorized under the same directory. This
     * directory is created inside dbDirName, and any further directories are placed
     * under this. Eg: Java, CSharp, or MySQL, SqlLite etc.
     */
    public String langDirName = EMPTY;

    /**
     * is the name of the dir which represents type of generated code. Eg:
     * DataObject, DataAccessLayer, DBCreateScripts, DBStoredProcedures etc. It
     * represents the name of the generate code type so files belonging to that
     * particular type can be clubbed together under one directory within the
     * langDirName directory.
     */
    public String subTypeDirName = EMPTY;

    /**
     * is the name of the directory which will be placed inside the
     * subTypeDirName. It represents the name of the table for this this file is
     * being generated.
     */
    public String tableDirName = EMPTY;

    /**
     * is the name of the file which will be placed inside the tableDirName. It
     * represents the name of the table for this this file is being generated.
     */
    public String tableName = EMPTY;

    /**
     * is the code-type to be appended to the file name which will complete the
     * actual name of the file. Eg: DO, DAL, _CREATE, _SP, etc.
     */
    public String fileType = EMPTY;

    /**
     * is the extension which is to be suffixed at the end of the file name.
     * Example: .java, .cpp, .cs, .sql, etc.
     */
    public String fileExtension = EMPTY;

    /**
     * is the actual content which is to be written in the file.
     */
    public String content = EMPTY;
}
