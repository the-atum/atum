
/** *****************************************************************************
 * Author:        Indresh Rawat (IVNR)
 * Email:         indreshrawat.gitlab@gmail.com
 *
 * Project:       Atum/DbDetails
 * File:          DbDetails.java
 *
 * Description:
 *      This class defines the fields which are encapsulated and needed for 
 *      CodeGenerators to work on for their internal method calls.
 *
 * Purpose:
 *      Instances of this class are used for passing around as ParamObjects
 *      to internal methods of CodeGenerators to avoid parameter-bloat in method
 *      calls and signatures.
 *
 * Revision History:
 *      2019-Dec-15 (IVNR): File created.
 *      2020-May-25 (PBMV): Standard code auto-formatting using RedHat's Java
 *                          Code-Formatter extension added with the Java
 *                          Language pack.
 *      2020-May-24 (PBMV): Converted member comments to proper jdoc-comments.
 *      2020-May-24 (PBMV): Updated proper description and purpose of this class.
 *      2020-May-28 (IVNR): Added primaryKeyMap field.
 *      2020-May-30 (PBMV): Moved to atum.dataobjects package.
 ****************************************************************************** */

package atum.dataobjects;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * This class defines the fields which are encapsulated and needed for
 * CodeGenerators to pass around for their internal method calls.
 */
public class DbDetails {

    /** Is the name of the table for being used in the CodeGenerator. */
    public String table;

    /** Is the name of the file in which the generated code will be saved. */
    public String fileTableName;

    /** Database name. */
    public String dbName;

    /** Is a set Map for prepared/callable statement. */
    public HashMap<String, String> setCallMap;

    /** Is a get Map for prepared/ callable statement. */
    public HashMap<String, String> getCallMap;

    /** List of columns. */
    public ArrayList<String> columns;

    /**  Defining a HashMap[String, String] whose
     key is column name and value is data type. */
    public HashMap<String, String> primaryKeyMap;
}
