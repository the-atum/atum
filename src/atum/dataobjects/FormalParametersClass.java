
/*******************************************************************************
 * Author:        Indresh Rawat (IVNR)
 * Email:         indreshrawat.gitlab@gmail.com
 * 
 * Project:       Atum/FormalParametersClass
 * File:          FormalParametersClass.java
 * 
 * Description:
 *       FormalParametersClass can be used as the method's argument list
 *       if the particular method as a huge amount of argument list.
 * 
 * Purpose:
 *      It is used as a argument list of the method which can reduce to write 
 *      a large amount of argument list when calling a method.
 * 
 * Revision History:
 *      2019-Sep-04 (IVNR): File created.
 *      2020-May-25 (PBMV): Standard code auto-formatting using RedHat's Java
 *                          Code-Formatter extension added with the Java
 *                          Language pack.
 *      2020-May-30 (PBMV): Moved to atum.dataobjects package.
 *      2020-Dec-28 (PBMV): Changed maps from HashMap to LinkedHashMap to
 *                          preserve insertion order.
 ****************************************************************************** */

package atum.dataobjects;

import java.util.LinkedHashMap;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

/**
 * It is used as a argument list of the method which can reduce to write a large
 * amount of argument list when calling a method.
 */
public class FormalParametersClass {
     
     // resultSetColumn stores the table of the column.
     public ResultSet resultSetColumn;

     // metaData stores the metadata of column table.
     public ResultSetMetaData metaData;

     // count used for iterating through the fields.
     public int count;

     // Creating a map which will store column property name and it's properties
     // associated with the table.
     // commonNodesNameMap a map which contains the common nodes which will be used
     // to create XML File.
     public LinkedHashMap<String, Object> commonNodesNameMap;

     // columnPropertyMap a map for adding keys and values where key is the common
     // nodes and values will be the
     // specified value for XML File.
     public LinkedHashMap<String, String> columnPropertyMap;

     // commonValueMap a map which contains the specified values which will be used
     // to create XML File.
     public LinkedHashMap<String, String> commonValueMap;

}
