
/** *****************************************************************************
 * Author:        Indresh Rawat (IVNR)
 * Email:         indreshrawat.gitlab@gmail.com
 *
 * Project:       Atum/DbRawMetaData
 * File:          DbRawMetaData.java
 *
 * Description:
 *       DbRawMetaData class stores different type of HashMap specified for
 *       the ObjectGraphReader and MySqlDbReader.
 *
 * Purpose:
 *       It is used for the readability of the code because of the HashMap
 *       type specified in the code.
 *
 * Revision History:
 *      2019-Sep-04 (IVNR): File created.
 *      2020-May-24 (PBMV): Standard code auto-formatting using RedHat's Java
 *                          Code-Formatter extension added with the Java
 *                          Language pack.
 *      2020-May-24 (PBMV): Updated class jdoc-comment of this class.
 *      2020-May-30 (PBMV): Moved to atum.dataobjects package.
 *      2020-Dec-28 (PBMV): Changed maps from HashMap to LinkedHashMap to
 *                          preserve insertion order.
 ****************************************************************************** */

package atum.dataobjects;

import java.util.LinkedHashMap;

/**
 * DbRawMetaData class stores different type of HashMap specified for the
 * ObjectGraphReader and MySqlDbReader. It is used for the readability of the
 * code because of the HashMap type specified in the code.
 */
public class DbRawMetaData {

    /**
     * Represents a field which is type of [dbName, [tableName, [columnName,
     * [columnProperties, value]]]]
     */
    public LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, String>>>> dbReaderMap;

    /**
     * Represents a field which is type of [rootNode, [dbName, [tableName,
     * [columnName, AtumColumnMetaData]]]]
     */
    public LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, AtumColumnMetaData>>>> updatedObjectGraphMapFromFile;

    /**
     * Represents a field which is type of [dbName, [tableName, [columnName,
     * AtumColumnMetaData]]]
     */
    public LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, AtumColumnMetaData>>> updatedObjectGraphMapFromRawMap;

    /**
     * Default constructor which initializes the fields of this pure dataObject
     * class.
     */
    public DbRawMetaData() {
        dbReaderMap = new LinkedHashMap<>();
        updatedObjectGraphMapFromFile = new LinkedHashMap<>();
        updatedObjectGraphMapFromRawMap = new LinkedHashMap<>();
    }

}
