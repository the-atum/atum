/*******************************************************************************
 * Author:        Pawan Vyas (PBMV)
 * Email:         pawan.vyas.gitlab@gmail.com
 * 
 * Project:       Atum/DBReaders
 * File:          ConnectionParameter.java
 * 
 * Description:
 *      Pure data object class used to encapsulate the 3 DB connection parameters
 *      passed out as a ParamObject.
 * 
 * Purpose:
 *      Instances of this class are used as ParamObjects for passing around to
 *      other methods or to constructors.
 * 
 * Revision History:
 *      2019-Nov-16 (PBMV): File created.
 *      2020-May-24 (PBMV): Added missing Header-Comment block.
 *      2020-May-24 (PBMV): Standard code auto-formatting using RedHat's Java
 *                          Code-Formatter extension added with the Java
 *                          Language pack.
 *      2020-May-30 (PBMV): Moved to atum.dataobjects package.
 ****************************************************************************** */

package atum.dataobjects;

/**
 * Pure data object class to pass/inject connection parameters.
 */
public class ConnectionParameter {

    /**
     * Represents the user name of the database which will be used to establish a
     * connection to it.
     */
    public String dbUserName;

    /**
     * Represents the credentials of the user.
     */
    public String dbUserPassword;

    /**
     * Represents the location where the database server is running.
     * 
     * Eg: localhost:3306, etc.
     */
    public String dbConnectionURL;
}