
/** *****************************************************************************
 * Author:        Indresh Rawat (IVNR)
 * Email:         indreshrawat.gitlab@gmail.com
 *
 * Project:       AtumDemo/ObjectGraph
 * File:          ObjectGraph.java
 *
 * Description:
 *      ObjectGraph is used by the dbReader, code generator and script generator.
 *      Here the information of  database will be stored.
 *
 * Purpose:
 *      It is used by the dbReader, code generator and script generator.
 *
 * Revision History:
 *      2019-Sep-24 (IVNR): File created.
 *      2020-May-25 (PBMV): Standard code auto-formatting using RedHat's Java
 *                          Code-Formatter extension added with the Java
 *                          Language pack.
 *      2020-May-30 (PBMV): Moved to atum.dataobjects package.
 *      2020-Dec-28 (PBMV): Changed maps from HashMap to LinkedHashMap to
 *                          preserve insertion order.
 ****************************************************************************** */

package atum.dataobjects;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * ObjectGraph is used by the dbReader, code generator and script generator.
 * Here the information of database will be stored.
 */
public class ObjectGraph {

    // Pure data Object which holds the connection parameters.
    public ConnectionParameter connParamObj;

    // The driver which will be used to connect the database.
    public String driverName;

    // The dbMap which contain the information of the database.
    public LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, AtumColumnMetaData>>> dbMap;

    /**
     * Default constructor which initializes the dbMap of this object graph.
     */
    public ObjectGraph() {
        connParamObj = new ConnectionParameter();
        dbMap = new LinkedHashMap<>();
        driverName = "";
    }

    /**
     * Function which gets the list of all the databases in the current ObjectGraph.
     *
     * @return an ArrayList of String type where each entry is a DB name in the
     *         currentObject graph.
     */
    public ArrayList<String> getDatabases() {

        // Creating a list to store all the db names.
        ArrayList<String> dbNames = new ArrayList<>();

        // Iterating through the object graph map.
        this.dbMap.entrySet().forEach((currentEntry) -> {
            // Adding each dbName key into the dbNames list.
            dbNames.add(currentEntry.getKey());
        });

        // Returning to the caller of the function.
        return dbNames;
    }

    /**
     * Function which gets the list of all the Tables in the passed dbName.
     *
     * @param dbName is the name of the database whose list of tables is to be
     *               retrieved.
     *
     * @return an ArrayList of String type where each entry is a table name in the
     *         passed dbName.
     */
    public ArrayList<String> getTables(String dbName) {

        // Creating an array list of mock up tables.
        ArrayList<String> tables = new ArrayList<>();

        // Creating a tableMap which retrieves the table present in the database from
        // the dbMap.
        LinkedHashMap<String, LinkedHashMap<String, AtumColumnMetaData>> tableMap = this.dbMap.get(dbName);

        // Iterating through the tableMap.
        tableMap.entrySet().forEach((currentEntry) -> {

            // Adding the keys which is the table name to the list.
            tables.add(currentEntry.getKey());
        });

        // Returning the table list to the caller.
        return tables;
    }

    /**
     * Function which gets the list of all the columns in the passed database's
     * given table.
     *
     * @param dbName    is the name of the db whose table is to be fetched.
     * @param tableName is the name of the table whose columns are to be fetched.
     *
     * @return an ArrayList of String type where each entry is a column name in the
     *         passed table within the dbName.
     */
    public ArrayList<String> getColumns(String dbName, String tableName) {

        // Retrieving the columns from the tableMap of the given db.
        LinkedHashMap<String, AtumColumnMetaData> columnMap = this.dbMap.get(dbName).get(tableName);

        // Creating an array list of mock up columns.
        ArrayList<String> colList = new ArrayList<>();

        // Iterating through the columnMap.
        columnMap.entrySet().forEach((currentEntry) -> {

            // Adding the keys which is the column name to the list.
            colList.add(currentEntry.getKey());
        });

        // Returning the column list to the caller.
        return colList;
    }

}
