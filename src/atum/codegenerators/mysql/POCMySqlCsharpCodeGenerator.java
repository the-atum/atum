/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atum.codegenerators.mysql;

import atum.dataobjects.AtumColumnMetaData;
import atum.dataobjects.CodeFileInfo;
import atum.dataobjects.DbDetails;
import atum.dataobjects.ObjectGraph;
import atum.interfaces.ICodeGenerator;
import atum.utils.AtumUtils;
import atum.utils.CodeFileWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 *
 * @author Indresh Rawat
 */
public class POCMySqlCsharpCodeGenerator implements ICodeGenerator {

    /**
     * Field which holds the retrieved ObjectGraph from the injected IDBReader.
     */
    private ObjectGraph internalObjGraph;

    private ArrayList<String> dbNames;
    private ArrayList<String> tables;
    private ArrayList<String> columns;
    private final static String N1 = "\n";
    private final static String N2 = N1 + "\n";
    private final static String T0 = "";
    private final static String T1 = T0 + "\t";
    private final static String T2 = T1 + T1;
    private final static String T3 = T2 + T1;
    private final static String T4 = T3 + T1;
    private final static String T5 = T4 + T1;
    private final static String T6 = T5 + T1;

    // Creating a map to hold data mapper in Csharp.
    private HashMap<String, String> csharpDataMapper;
    private HashMap<String, String> csharpConverterMap;
    private HashMap<String, String> mySqlDbTypeMap;
    private HashMap<String, String> commentMap;

    /**
     * Default constructor.
     */
    public POCMySqlCsharpCodeGenerator() {
        internalObjGraph = null;
        dbNames = null;
        tables = null;
        columns = null;
        csharpDataMapper = null;
    }

    @Override
    public void SetObjectGraph(ObjectGraph objGraph) {
        internalObjGraph = objGraph;
        internalObjGraph.driverName = "";
        dbNames = internalObjGraph.getDatabases();
        tables = new ArrayList<>();
        columns = new ArrayList<>();
        csharpDataMapper = initCsharpMap();
        csharpConverterMap = initCsharpConvertToMap();
        mySqlDbTypeMap = initMySqlDbTypeMap();
        commentMap = initCommentMap();
    }

    @Override
    public void generateCode() throws Exception {
        if (null == internalObjGraph)
            throw new Exception("FATAL: ObjectGraph for this CodeGenerator not initialized."
                    + "\nEnsure that ObjectGraph is set before this method is invoked using"
                    + "the ICodeGenerator.SetObjectGraph method.");
        generateDO();
        generateDAL();
    }

    private String snippetWarning(String tab) {
        StringBuilder sbCode = new StringBuilder();
        sbCode.append(tab).append("/// DO NOT MODIFY CONTENTS OF THIS CLASS.").append(N1);
        sbCode.append(tab).append("/// EXTEND THIS CLASS VIA INHERITANCE TO ADD ADDITIONAL OR OVERRIDDEN METHODS.").append(N1);
        sbCode.append(tab).append("/// AUTO-GENERATED ON: ").append(new Date().toString()).append(".").append(N1);
        return sbCode.toString();
    }

    private String snippetDOComment(String tab, String dbName, String tableName)
    {
        StringBuilder sbCode = new StringBuilder();
        
        sbCode.append(tab).append("/// <summary>").append(N1)
            .append(tab).append("/// Auto-Generated DataObject class for table: ").append(tableName).append(" in database: ").append(dbName).append(".").append(N1)
            .append(snippetWarning(tab))
            .append(tab).append("/// </summary>").append(N1);

        return sbCode.toString();
    }

    private String snippetFieldComment(String tab, String fieldName, String fieldDesc) {
        StringBuilder sbCode = new StringBuilder();
        sbCode.append(tab).append("/// <summary>").append(N1);
        sbCode.append(tab).append("/// ").append(fieldName).append(" ").append(fieldDesc).append(N1);
        sbCode.append(tab).append("/// </summary>").append(N1);
        return sbCode.toString();
    }

    private String snippetToStringComment(String tab)
    {
        StringBuilder sbCode = new StringBuilder();
        
        sbCode.append(tab).append("/// <summary>").append(N1)
            .append(tab).append("/// Overridden System.Object.ToString method to provide a custom human-readable string for this DO.").append(N1)
            .append(tab).append("/// </summary>").append(N1)
            .append(tab).append("/// <returns>").append(N1)
            .append(tab).append("/// A human-readable string representation for this DO containing all the fields with their values.").append(N1)
            .append(tab).append("/// </returns>").append(N1);
        return sbCode.toString();
    }

    private String snippetDALComment(String tab, String dbName, String tableName)
    {
        StringBuilder sbCode = new StringBuilder();
        
        sbCode.append(tab).append("/// <summary>").append(N1)
            .append(tab).append("/// Auto-Generated implementation of IDataAccessLayer interface for table: ").append(tableName).append(" in database: ").append(dbName).append(".").append(N1)
            .append(snippetWarning(tab))
            .append(tab).append("/// </summary>").append(N1);

        return sbCode.toString();
    }

    private String snippetConnComment(String tab) {
        StringBuilder sbCode = new StringBuilder();
        sbCode.append(tab).append("/// <summary>").append(N1);
        sbCode.append(tab).append("/// Represents the connection string to be used for connecting to the database.").append(N1);
        sbCode.append(tab).append("/// </summary>").append(N1);
        return sbCode.toString();
    }

    private String snippetCmdObjComment(String tab) {
        StringBuilder sbCode = new StringBuilder();
        sbCode.append(tab).append("/// <summary>").append(N1);
        sbCode.append(tab).append("/// Method which creates a new MySqlCommand object using the ConnectionString and passed procedureName.").append(N1);
        sbCode.append(tab).append("/// </summary>").append(N1);
        sbCode.append(tab).append("/// <param name=\"procedureName\">").append(N1);
        sbCode.append(tab).append("/// Is the name of the stored procedure passed to be used to create the MySqlCommand object.").append(N1);
        sbCode.append(tab).append("/// </param>").append(N1);
        sbCode.append(tab).append("/// <returns>").append(N1);
        sbCode.append(tab).append("/// A new MySqlCommand object created using the ConnectionString and passed procedureName.").append(N1);
        sbCode.append(tab).append("/// </returns>").append(N1);
        return sbCode.toString();
    }
    
    private String snippetMethodComment(String tab, String action, String obj, String result)
    {
        StringBuilder sbCode = new StringBuilder();
        sbCode.append(tab).append("/// <summary>").append(N1);
        sbCode.append(tab).append("/// ").append(commentMap.get(action)).append(N1);
        sbCode.append(tab).append("/// </summary>").append(N1);
        sbCode.append(tab).append("/// <param name=\"obj\">").append(N1);
        sbCode.append(tab).append("/// ").append(commentMap.get(obj)).append(N1);
        sbCode.append(tab).append("/// </param>").append(N1);
        sbCode.append(tab).append("/// <returns>").append(N1);
        sbCode.append(tab).append("/// ").append(commentMap.get(result)).append(N1);
        sbCode.append(tab).append("/// </returns>").append(N1);
        return sbCode.toString();
    }

    private String snippetMethodComment(String tab, String action, String result)
    {
        StringBuilder sbCode = new StringBuilder();
        sbCode.append(tab).append("/// <summary>").append(N1);
        sbCode.append(tab).append("/// ").append(commentMap.get(action)).append(N1);
        sbCode.append(tab).append("/// </summary>").append(N1);
        sbCode.append(tab).append("/// <returns>").append(N1);
        sbCode.append(tab).append("/// ").append(commentMap.get(result)).append(N1);
        sbCode.append(tab).append("/// </returns>").append(N1);
        return sbCode.toString();
    }

    /**
     * Function to generate the DO for the passed dbReader in Csharp.
     *
     * @return a string of the generated DataObject Code.
     */
    private void generateDO() {

        // Getting DB name from dBReader.
        for (String dbName : dbNames) {

            // Getting tables list from dBReader.
            tables = internalObjGraph.getTables(dbName);

            // Defining and setting up CodeFileInfo object.
            CodeFileInfo codeFileInfo = new CodeFileInfo();
            codeFileInfo.langDirName = "CSharp/";
            codeFileInfo.dbDirName = dbName + "/";
            codeFileInfo.subTypeDirName = "DataObjects/";
            codeFileInfo.fileType = "DO";
            codeFileInfo.fileExtension = ".cs";

            // Iterating over each table and generating a DO class for that table.
            for (String table : tables) {

                // Updating the name of the table to follow CSharp specific class naming
                // standards.
                String fileTableName = AtumUtils.properCase(table);

                // Defining stringBuilder object which will hold the generated DO code
                // including new lines and tab indentations as and when code is generated.
                StringBuilder sbCode = new StringBuilder();

                sbCode.append("using System;").append(N2)
                    .append("namespace MySqlDAL.DO").append(braceOpen(T0))
                    .append(snippetDOComment(T1, dbName, fileTableName))
                    .append(T1).append("public class ").append(fileTableName).append(codeFileInfo.fileType)
                    .append(braceOpen(T1));

                // Getting columns list from dBReader.
                columns = internalObjGraph.getColumns(dbName, table);

                String toStringSnippet = "";
                // Iterating of columns and generating code fields for each column.
                for (String column : columns) {

                    // Getting the AtumColumnMetaData object for this current column.
                    AtumColumnMetaData colData = internalObjGraph.dbMap.get(dbName).get(table).get(column);

                    // Updating the column Name for each field of the DO class to follow CSharp
                    // specific language standard.
                    String colName = AtumUtils.properCase(colData.columnName);
                    if (csharpDataMapper.containsKey(colData.dataType)) {
                        sbCode.append(snippetFieldComment(T2, colData.columnName, colData.columnComment));
                        sbCode.append(T2).append("public ").append(csharpDataMapper.get(colData.dataType)).append(" ")
                                .append(colName).append(" { set; get; }").append(N2);
                        toStringSnippet += colName + ": {" + colName + "} ";
                    }
                }

                String returnString = "$\"" + toStringSnippet + "\";";
                sbCode.append(snippetToStringComment(T2))
                    .append(T2).append("public override string ToString()").append(braceOpen(T2)).append(T3)
                    .append("string returnString = ").append(returnString).append(N1).append(T3)
                    .append("return \"{ \" + returnString + \"}\";")
                    .append(triBraceClose(T0, T1, T2));

                // For each existing table in the list of tables, updating the codeFileInfo
                // parameter object's tableName to the current table name.
                // And updating its content field to the generated code in string.
                codeFileInfo.tableName = fileTableName;
                codeFileInfo.content = sbCode.toString();

                // Call CodeFileWriter's generateFile by passing it the fileInfo parameter
                // object.
                CodeFileWriter.generateFile(codeFileInfo);
            }
        }
    }

    /**
     * Function to generate the DAL for the passed objGraph.
     *
     * @return a string of the generated DAL Code.
     */
    private void generateDAL() {
        // Getting DB name from dBReader.
        for (String dbName : dbNames) {

            // Getting tables list from dBReader.
            tables = internalObjGraph.getTables(dbName);

            // Defining and setting up CodeFileInfo object.
            CodeFileInfo codeFileInfo = new CodeFileInfo();
            codeFileInfo.langDirName = "Csharp/";
            codeFileInfo.dbDirName = dbName + "/";
            codeFileInfo.subTypeDirName = "DAL/";
            codeFileInfo.fileType = "DAL";
            codeFileInfo.fileExtension = ".cs";

            // Creating DbDetails object which is a paramObj;
            DbDetails dbDetails = new DbDetails();

            // Iterating over each table and generating a DAL class for that table.
            for (String table : tables) {

                // Updating the name of the table to follow Csharp specific class name
                // Standards.
                String fileTableName = "A" + AtumUtils.properCase(table);
                String classTableName = fileTableName + codeFileInfo.fileType;

                // Defining stringBuilder object which will hold the generated DAL code
                // including new lines and tab indentations as and when code is generated.
                StringBuilder sbCode = new StringBuilder();
                
                sbCode.append("using System;").append(N1);
                sbCode.append("using System.Collections.Generic;").append(N1);
                sbCode.append("using System.Data;").append(N1);
                sbCode.append("using System.Configuration;").append(N1);
                sbCode.append("using System.ComponentModel;").append(N1);
                sbCode.append("using MySql.Data.MySqlClient;").append(N1);
                sbCode.append("using DALInterface;").append(N1);
                sbCode.append("using MySqlDAL.DO;").append(N2);

                sbCode.append("namespace MySqlDAL.DAL.Generated").append(braceOpen(T0));
                sbCode.append(snippetDALComment(T1, dbName, table));
                sbCode.append(T1).append("public abstract class ").append(classTableName).append(" : IDataAccessLayer")
                        .append(braceOpen(T1));

                dbDetails.dbName = dbName;
                dbDetails.fileTableName = classTableName;
                dbDetails.table = table;

                // Calling the generateConnectionStringField method to append the field for
                // connection string.
                sbCode.append(N1).append(generateConnectionStringField()).append(N1);

                // Calling the generateConnectionStringField method to append the field for
                // connection string.
                sbCode.append(N1).append(generateGetCommandObject(dbDetails));

                // Calling the generatedDALCreate method to append the create method.
                sbCode.append(N1).append(generateDALCreate(dbDetails));

                // Calling the generateDALRead method to append the read method.
                sbCode.append(N1).append(generateDALReadById(dbDetails));

                // Calling the generatedDALReadAll method to append the readAll method.
                sbCode.append(N1).append(generateDALReadAll(dbDetails));

                // Calling the generateDALUpdate method to append the update method.
                sbCode.append(N1).append(generateDALUpdate(dbDetails));

                // Calling the generateDALDelete method to append the update method.
                sbCode.append(N1).append(generateDALDelete(dbDetails));

                sbCode.append(dualBraceClose(T0, T1));

                // For each existing table in the list of tables, updating the codeFileInfo
                // parameter object's tableName to the current table name.
                // And updating its content field to the generated code in string.
                codeFileInfo.tableName = fileTableName;
                codeFileInfo.content = sbCode.toString();

                // Call CodeFileWriter's generateFile by passing it the fileInfo parameter
                // object.
                CodeFileWriter.generateFile(codeFileInfo);
            }
        }
    }

    private String generateConnectionStringField() {

        // Server=myServerAddress;Port=1234;Database=myDataBase;Uid=myUsername;Pwd=myPassword;
        // Defining StringBuilder object which will hold the generated Command.
        StringBuilder sbCode = new StringBuilder();
        sbCode.append(snippetConnComment(T2))
            .append(T2).append("protected string ConnectionString = ConfigurationManager.AppSettings[\"MySqlConnection\"];");

        // Returning the string from the sbCode to the caller.
        return sbCode.toString();
    }

    /**
     * Function to generate code for MySqlCommand object.
     * 
     * @param dbDetails Is the passed DbDetails param-obj which contains relevant
     *                  data needed.
     *
     * @return the String of MySqlCommand object
     */
    private String generateGetCommandObject(DbDetails dbDetails) {

        // Defining StringBuilder object which will hold the generated Command.
        StringBuilder sbCode = new StringBuilder();

        sbCode.append(snippetCmdObjComment(T2))
            .append(T2).append("protected virtual MySqlCommand GetCommandObj(string procedureName)").append(braceOpen(T2))
            .append(T3).append("return new MySqlCommand(")
            .append("procedureName, new MySqlConnection(ConnectionString));")
            .append(braceClose(T2));

        // Returning the string to the caller of the function.
        return sbCode.toString();
    }

    private String braceInsert(String tab, String brace) {
        StringBuilder sbCode = new StringBuilder();
        sbCode.append(N1).append(tab).append(brace).append(N1);
        return sbCode.toString();
    }

    private String braceOpen(String tab) {
        return braceInsert(tab, "{");
    }
    
    private String triBraceClose(String tab1, String tab2, String tab3) {
        StringBuilder sbCode = new StringBuilder();
        sbCode.append(dualBraceClose(tab2, tab3)).append(tab1).append("}").append(N1);
        return sbCode.toString();
    }

    private String dualBraceClose(String tab1, String tab2) {
        StringBuilder sbCode = new StringBuilder();
        sbCode.append(braceClose(tab2)).append(tab1).append("}").append(N1);
        return sbCode.toString();
    }

    private String braceClose(String tab) {
        return braceInsert(tab, "}");
    }

    private String braceCloseSemi(String tab) {
        StringBuilder sbCode = new StringBuilder();
        sbCode.append(N1).append(tab).append("};");
        return sbCode.toString();
    }

    private String snippetCmdObjInit(String tab) {
        StringBuilder sbCode = new StringBuilder();

        sbCode.append(tab).append("mysqlCmd = GetCommandObj(spName);").append(N1);
        sbCode.append(tab).append("mysqlCmd.CommandType = CommandType.StoredProcedure;").append(N1);

        return sbCode.toString();
    }

    private String snippetSpNameCmdObj(String tab, String spName) {
        StringBuilder sbCode = new StringBuilder();
        sbCode.append(tab).append("string spName = \"").append(spName).append("\";").append(N1);
        sbCode.append(tab).append("MySqlCommand mysqlCmd = null;").append(N1);
        return sbCode.toString();
    }

    private String snippetResultObj(String tab1, String tab2, String methodMsg, String spName) {
        StringBuilder sbCode = new StringBuilder();
        sbCode.append(tab1).append("ResultObject result = new ResultObject()").append(braceOpen(tab1));
        sbCode.append(tab2).append("Method = \"").append(methodMsg).append("\",").append(N1);
        sbCode.append(tab2).append("StoredProcedure = spName,").append(N1);
        sbCode.append(tab2).append("Parameter = obj").append(braceCloseSemi(tab1));
        return sbCode.toString();
    }

    private String snippetResultObjNoParam(String tab1, String tab2, String methodMsg, String spName) {
        StringBuilder sbCode = new StringBuilder();
        sbCode.append(tab1).append("ResultObject result = new ResultObject()").append(braceOpen(tab1));
        sbCode.append(tab2).append("Method = \"").append(methodMsg).append("\",").append(N1);
        sbCode.append(tab2).append("StoredProcedure = spName").append(braceCloseSemi(tab1));
        return sbCode.toString();
    }

    private String snippetTryOpen(String tab) {
        StringBuilder sbCode = new StringBuilder();
        sbCode.append(N1).append(tab).append("try").append(braceOpen(tab));
        return sbCode.toString();
    }

    private String snippetTypeCastObj(String tab, String tableDOName, String tableDOObjName) {
        StringBuilder sbCode = new StringBuilder();
        sbCode.append(tab).append(tableDOName).append(" ").append(tableDOObjName).append(" = obj as ")
                .append(tableDOName).append(";").append(N1);
        return sbCode.toString();
    }

    private String snippetExecNonQuery(String tab, String boolName) {
        StringBuilder sbCode = new StringBuilder();
        sbCode.append(tab).append("mysqlCmd.Connection.Open();").append(N1);
        sbCode.append(tab).append("bool ").append(boolName).append(" = 0 < mysqlCmd.ExecuteNonQuery();").append(N1);
        return sbCode.toString();
    }

    private String snippetCatchFinallyReturn(String tab1, String tab2, String tab3) {
        StringBuilder sbCode = new StringBuilder();

        // In catch block.
        sbCode.append(tab1).append("catch (Exception exObj)").append(braceOpen(tab1)).append(tab2)
                .append("result.Failure = $\"{exObj.Message} ({exObj.HResult})\";").append(N1)
                .append(tab2).append("Console.Error.WriteLine(result.Failure);").append(N1)
                .append(tab2).append("Console.Error.WriteLine(exObj.StackTrace);")
                .append(braceClose(tab1)) // end catch.
                // In finally block.
                .append(tab1).append("finally").append(braceOpen(tab1)).append(tab2)
                .append("mysqlCmd?.Connection?.Close();").append(braceClose(tab1)) // end finally.
                .append(tab1).append("return result;");

        return sbCode.toString();
    }

    /**
     * Function to generate create code for DAL.
     *
     * @return the String of create code
     */
    private String generateDALCreate(DbDetails dbDetails) {
        // Defining StringBuilder object which will hold the generated create code.
        StringBuilder sbCode = new StringBuilder();

        // The extension of the stored procedure.
        String spName = dbDetails.table + "_SP_CREATE";
        String tableDOName = AtumUtils.properCase(dbDetails.table) + "DO";
        String tableDOObjName = dbDetails.table + "Obj";

        sbCode.append(snippetMethodComment(T2,"Cr","CrP","CrR"))
            .append(T2).append("public virtual ResultObject Create(object obj)")
            .append(braceOpen(T2))
            .append(snippetSpNameCmdObj(T3, spName))
            .append(T3).append("ResultObject result = new ResultObject()").append(braceOpen(T3))
            .append(T4).append("Method = \"Create:").append(tableDOObjName).append("\",").append(N1)
            .append(T4).append("StoredProcedure = spName,").append(N1)
            .append(T4).append("Parameter = obj").append(braceCloseSemi(T3))
            .append(snippetTryOpen(T3))
            .append(snippetTypeCastObj(T4, tableDOName, tableDOObjName))
            .append(snippetCmdObjInit(T4));

        // Getting columns list from dBReader.
        columns = internalObjGraph.getColumns(dbDetails.dbName, dbDetails.table);

        // String to compose the after-execution snippet of the out_param variable.
        String outFetchString = "";

        // Iterating through the columns of the table.
        for (String column : columns) {

            // Retrieving the columnMetaData of the particular column.
            AtumColumnMetaData colData = internalObjGraph.dbMap.get(dbDetails.dbName).get(dbDetails.table).get(column);
            String colFieldName = AtumUtils.properCase(colData.columnName);
            String colSPParamName = "param_" + colData.columnName;
            if (!colData.isPrimaryKey) {
                sbCode.append(T4).append("mysqlCmd.Parameters.AddWithValue(\"").append(colSPParamName).append("\", ")
                        .append(tableDOObjName).append(".").append(colFieldName).append(");").append(N1);
            } else {
                sbCode.append(T4).append("mysqlCmd.Parameters.Add(\"out_").append(colSPParamName).append("\", ")
                        .append(mySqlDbTypeMap.get(colData.dataType)).append(").Direction = ParameterDirection.Output;")
                        .append(N1);

                // Composing the out_param snippet for the primary key.
                StringBuilder outFetchBuilder = new StringBuilder();
                outFetchBuilder.append(tableDOObjName).append(".").append(colFieldName).append(" = ")
                        .append(csharpConverterMap.get(colData.dataType)).append("(mysqlCmd.Parameters[\"out_")
                        .append(colSPParamName).append("\"].Value);").append(N1);

                outFetchString += outFetchBuilder.toString();
            }
        }
        String boolName = "rowsInserted";
        sbCode.append(snippetExecNonQuery(T4, boolName))
            .append(T4).append("if (").append(boolName).append(")")
            .append(braceOpen(T4)).append(T5).append(outFetchString).append(T5).append("result.Data = ")
            .append(tableDOObjName).append(";")
            .append(dualBraceClose(T3, T4)) // end id, end try.
            .append(snippetCatchFinallyReturn(T3, T4, T5)) // end catch, end finally, return.
            .append(braceClose(T2)); // End of generated function.

        // Returning the string to the caller of the function.
        return sbCode.toString();
    }

    /**
     * Function to generate Update code for DAL.
     *
     * @return the String of Update code.
     */
    private String generateDALUpdate(DbDetails dbDetails) {
        // Defining StringBuilder object which will hold the generated create code.
        StringBuilder sbCode = new StringBuilder();

        // The extension of the stored procedure.
        String spName = dbDetails.table + "_SP_UPDATE";
        String tableDOName = AtumUtils.properCase(dbDetails.table) + "DO";
        String tableDOObjName = dbDetails.table + "Obj";

        sbCode.append(snippetMethodComment(T2,"Up","UpP","UpR"))
            .append(T2).append("public virtual ResultObject Update(object obj)").append(braceOpen(T2));

        String methodMsg = "Update:" + tableDOObjName;
        sbCode.append(snippetSpNameCmdObj(T3, spName))
            .append(snippetResultObj(T3, T4, methodMsg, spName))
            .append(snippetTryOpen(T3))
            .append(snippetTypeCastObj(T4, tableDOName, tableDOObjName))
            .append(snippetCmdObjInit(T4));

        // Getting columns list from dBReader.
        columns = internalObjGraph.getColumns(dbDetails.dbName, dbDetails.table);

        // Iterating through the columns of the table.
        for (String column : columns) {

            // Retrieving the columnMetaData of the particular column.
            AtumColumnMetaData colData = internalObjGraph.dbMap.get(dbDetails.dbName).get(dbDetails.table).get(column);
            String colFieldName = AtumUtils.properCase(colData.columnName);
            String colSPParamName = "param_" + colData.columnName;
            sbCode.append(T4).append("mysqlCmd.Parameters.AddWithValue(\"").append(colSPParamName).append("\", ")
                    .append(tableDOObjName).append(".").append(colFieldName).append(");").append(N1);
        }

        String boolName = "rowsUpdated";
        sbCode.append(snippetExecNonQuery(T4, boolName))
            .append(T4).append("result.Data = ").append(boolName)
            .append(";").append(N1).append(T4).append("if (!").append(boolName).append(")").append(braceOpen(T4))
            .append(T5).append("result.Failure = $\"No record found for ").append(tableDOObjName).append(": {")
            .append(tableDOObjName).append("}\";").append(dualBraceClose(T3, T4)) // end if, end try.
            .append(snippetCatchFinallyReturn(T3, T4, T5)) // end catch, end finally, return.
            .append(braceClose(T2)); // End of generated function.

        // Returning the string to the caller of the function.
        return sbCode.toString();
    }

    /**
     * Function to generate Delete code for DAL.
     *
     * @return the String of Delete code.
     */
    private String generateDALDelete(DbDetails dbDetails) {

        // Defining StringBuilder object which will hold the generated create code.
        StringBuilder sbCode = new StringBuilder();

        // The extension of the stored procedure.
        String spName = dbDetails.table + "_SP_DELETE";

        AtumColumnMetaData primaryColData = null;
        String primaryFieldName = "";
        String primaryParamTypeCastSnippet = "";
        String primaryParamAddSnippet = "";

        // Getting columns list from dBReader.
        columns = internalObjGraph.getColumns(dbDetails.dbName, dbDetails.table);

        // Iterating through the columns of the table.
        for (String column : columns) {

            // Retrieving the columnMetaData of the particular column.
            AtumColumnMetaData colData = internalObjGraph.dbMap.get(dbDetails.dbName).get(dbDetails.table).get(column);

            if (colData.isPrimaryKey) {
                primaryColData = colData;
                primaryFieldName = primaryColData.columnName;

                StringBuilder typeCastBuilder = new StringBuilder();
                String dType = primaryColData.dataType;
                typeCastBuilder.append(csharpDataMapper.get(dType)).append(" ").append(primaryFieldName).append(" = ")
                        .append(csharpConverterMap.get(dType)).append("(obj);");

                primaryParamTypeCastSnippet = typeCastBuilder.toString();

                StringBuilder paramBuilder = new StringBuilder();
                String colSPParamName = "param_" + primaryColData.columnName;
                paramBuilder.append("mysqlCmd.Parameters.AddWithValue(\"").append(colSPParamName).append("\", ")
                        .append(primaryFieldName).append(");");
                primaryParamAddSnippet = paramBuilder.toString();

                break;
            }
        }

        sbCode.append(snippetMethodComment(T2,"Dl","DlP","DlR"))
            .append(T2).append("public virtual ResultObject Delete(object obj)").append(braceOpen(T2));

        String methodMsg = "Delete";
        String deleteFailSnippet = "result.Failure = $\"Delete Failed\";";
        if (null != primaryColData) {
            methodMsg = "Delete:" + primaryFieldName;
            deleteFailSnippet = "result.Failure = $\"No record found for " + primaryFieldName + ": {" + primaryFieldName + "}\";";
        }

        sbCode.append(snippetSpNameCmdObj(T3, spName))
            .append(snippetResultObj(T3, T4, methodMsg, spName))
            .append(snippetTryOpen(T3))
            .append(T4).append(primaryParamTypeCastSnippet).append(N1)
            .append(snippetCmdObjInit(T4))
            .append(T4).append(primaryParamAddSnippet).append(N1);

        String boolName = "rowsDeleted";
        sbCode.append(snippetExecNonQuery(T4, boolName))
            .append(T4).append("result.Data = rowsDeleted;").append(N1)
            .append(T4).append("if (!").append(boolName).append(")").append(braceOpen(T4))
            .append(T5).append(deleteFailSnippet)
            .append(dualBraceClose(T3, T4))// end if, end try.
            .append(snippetCatchFinallyReturn(T3, T4, T5)) // end catch, end finally, return.
            .append(braceClose(T2)); // End of generated function.

        // Returning the string to the caller of the function.
        return sbCode.toString();
    }

    /**
     * Function to generate ReadById code for DAL.
     *
     * @return the String of ReadById code.
     */
    private String generateDALReadById(DbDetails dbDetails) {

        // Defining StringBuilder object which will hold the generated create code.
        StringBuilder sbCode = new StringBuilder();

        // The extension of the stored procedure.
        String spName = dbDetails.table + "_SP_READBYID";
        String tableDOName = AtumUtils.properCase(dbDetails.table) + "DO";
        String tableDOObjName = dbDetails.table + "Obj";

        // Getting columns list from dBReader.
        columns = internalObjGraph.getColumns(dbDetails.dbName, dbDetails.table);

        // Finding primary key for the table.
        AtumColumnMetaData primaryColData = null;
        for (String column : columns) {

            // Retrieving the columnMetaData of the particular column.
            primaryColData = internalObjGraph.dbMap.get(dbDetails.dbName).get(dbDetails.table).get(column);

            if (primaryColData.isPrimaryKey) {
                break;
            }
        }

        // Only run if at least one of the columns is a primary key.
        if (primaryColData.isPrimaryKey) {

            String primaryFieldName = AtumUtils.properCase(primaryColData.columnName);

            
            StringBuilder typeCastBuilder = new StringBuilder();
            String dType = primaryColData.dataType;
            typeCastBuilder.append(csharpDataMapper.get(dType)).append(" ").append(primaryFieldName).append(" = ")
                    .append(csharpConverterMap.get(dType)).append("(obj);");

            String primaryParamTypeCastSnippet = typeCastBuilder.toString();

            sbCode.append(snippetMethodComment(T2,"Ri", "RiP", "RiR"))
                .append(T2).append("public virtual ResultObject Read(object obj)")
                .append(braceOpen(T2))
                .append(T3).append(tableDOName).append(" ").append(tableDOObjName).append(" = new ").append(tableDOName).append("();").append(N1);
                
            String methodMsg = "Read:" + primaryFieldName;
            sbCode.append(snippetSpNameCmdObj(T3, spName))
                .append(snippetResultObj(T3, T4, methodMsg, spName))
                .append(snippetTryOpen(T3))
                .append(T4).append(primaryParamTypeCastSnippet).append(N1)
                .append(snippetCmdObjInit(T4));

            String primaryColSPParamName = "param_" + primaryColData.columnName;
            sbCode.append(T4).append("mysqlCmd.Parameters.AddWithValue(\"").append(primaryColSPParamName).append("\", ").append(primaryFieldName).append(");").append(N1)
                .append(T4).append("mysqlCmd.Connection.Open();").append(N1)
                .append(T4).append("using (MySqlDataReader mysqlDR = mysqlCmd.ExecuteReader())")
                .append(braceOpen(T4))
                .append(T5).append("result.Failure = $\"No record found for ").append(primaryFieldName).append(": {").append(primaryFieldName).append("}\";").append(N1)
                .append(T5).append("while(mysqlDR.Read())")
                .append(braceOpen(T5));

            for (String column : columns) {
                AtumColumnMetaData colData = internalObjGraph.dbMap.get(dbDetails.dbName).get(dbDetails.table)
                        .get(column);
                String colFieldName = AtumUtils.properCase(colData.columnName);
                sbCode.append(T6).append(tableDOObjName).append(".").append(colFieldName).append(" = ").append(csharpConverterMap.get(colData.dataType)).append("(").append("mysqlDR[\"").append(colData.columnName).append("\"]);").append(N1);
            }
            sbCode.append(T6).append("result.Data = ").append(tableDOObjName).append(";").append(N1)
                .append(T6).append("result.Failure = null;")
                .append(triBraceClose(T3, T4, T5))
                .append(snippetCatchFinallyReturn(T3, T4, T5))
                .append(braceClose(T2));
        }

        // Returning the string to the caller of the function.
        return sbCode.toString();
    }

    private String generateDALReadAll(DbDetails dbDetails) {

        // Defining StringBuilder object which will hold the generated create code.
        StringBuilder sbCode = new StringBuilder();

        // The extension of the stored procedure.
        String spName = dbDetails.table + "_SP_READALL";
        String tableDOName = AtumUtils.properCase(dbDetails.table) + "DO";
        String tableDOObjName = dbDetails.table + "Obj";

        String listTypeName = "List<" + tableDOName + ">";
        String listObjName = dbDetails.table + "List";

        sbCode.append(snippetMethodComment(T2,"Ra", "RaR"))
            .append(T2).append("public virtual ResultObject ReadAll()").append(braceOpen(T2));

        sbCode.append(T3).append(listTypeName).append(" ").append(listObjName).append(" = new ").append(listTypeName)
                .append("();").append(N1);

        String methodMsg = "Read";
        sbCode.append(snippetSpNameCmdObj(T3, spName))
            .append(snippetResultObjNoParam(T3, T4, methodMsg, spName))
            .append(snippetTryOpen(T3))
            .append(snippetCmdObjInit(T4));

        sbCode.append(T4).append("mysqlCmd.Connection.Open();").append(N1);
        sbCode.append(T4).append("using (MySqlDataReader mysqlDR = mysqlCmd.ExecuteReader())")
            .append(braceOpen(T4))
            .append(T5).append("while(mysqlDR.Read())")
            .append(braceOpen(T5))
            .append(T6).append(tableDOName).append(" ").append(tableDOObjName).append(" = new ").append(tableDOName).append("();").append(N1);

        for (String column : columns) {
            AtumColumnMetaData colData = internalObjGraph.dbMap.get(dbDetails.dbName).get(dbDetails.table).get(column);
            String colFieldName = AtumUtils.properCase(colData.columnName);
            sbCode.append(T6).append(tableDOObjName).append(".").append(colFieldName).append(" = ").append(csharpConverterMap.get(colData.dataType)).append("(").append("mysqlDR[\"").append(colData.columnName).append("\"]);").append(N1);
        }

        sbCode.append(T6).append(listObjName).append(".Add(").append(tableDOObjName).append(");")
            .append(braceClose(T5))
            .append(T5).append("result.Data = ").append(listObjName).append(";").append(N1)
            .append(T5).append("result.Failure = (0 < result.Data.Count) ? null : \"No records found.\";")
            .append(dualBraceClose(T3, T4))
            .append(snippetCatchFinallyReturn(T3, T4, T5))
            .append(braceClose(T2));

        // Returning the string to the caller of the function.
        return sbCode.toString();
    }

    /**
     * The below method is used to initialize the csharp Data mapper where key is
     * datatype in mysql and value is the datatype in Csharp.
     * 
     * @return a HashMap of <String, String>.
     */
    private static HashMap<String, String> initCsharpMap() {

        // Instantiating csharpMap map where key is datatype in mysql and
        // value is the datatype in Csharp.
        HashMap<String, String> csharpMap = new HashMap<>();

        csharpMap.put("bit", "bool");
        csharpMap.put("tinyint", "bool");
        csharpMap.put("smallint", "int");
        csharpMap.put("mediumint", "int");
        csharpMap.put("int", "int");
        csharpMap.put("bigint", "long");
        csharpMap.put("numeric", "double");
        csharpMap.put("decimal", "double");
        csharpMap.put("double", "double");
        csharpMap.put("char", "string");
        csharpMap.put("varchar", "string");
        csharpMap.put("binary", "byte[]");
        csharpMap.put("varbinary", "byte[]");
        csharpMap.put("tinyblob", "byte[]");
        csharpMap.put("blob", "byte[]");
        csharpMap.put("mediumblob", "byte[]");
        csharpMap.put("longblob", "byte[]");
        csharpMap.put("tinytext", "string");
        csharpMap.put("text", "string");
        csharpMap.put("mediumtext", "string");
        csharpMap.put("longtext", "string");
        csharpMap.put("enum", "string");
        csharpMap.put("set", "string");
        csharpMap.put("date", "DateTime");
        csharpMap.put("datetime", "DateTime");
        csharpMap.put("timestamp", "DateTime");
        csharpMap.put("time", "DateTime");
        csharpMap.put("year", "int");
        csharpMap.put("geometry", "byte[]");

        // Returning csharpMap to the caller.
        return csharpMap;
    }

    private static HashMap<String, String> initCsharpConvertToMap() {
        HashMap<String, String> cSharpConvertToMap = new HashMap<>();

        cSharpConvertToMap.put("bit", "Convert.ToBoolean");
        cSharpConvertToMap.put("tinyint", "Convert.ToBoolean");
        cSharpConvertToMap.put("smallint", "Convert.ToInt32");
        cSharpConvertToMap.put("mediumint", "Convert.ToInt32");
        cSharpConvertToMap.put("int", "Convert.ToInt32");
        cSharpConvertToMap.put("bigint", "Convert.ToInt64");
        cSharpConvertToMap.put("numeric", "Convert.ToDouble");
        cSharpConvertToMap.put("decimal", "Convert.ToDouble");
        cSharpConvertToMap.put("double", "Convert.ToDouble");
        cSharpConvertToMap.put("char", "Convert.ToString");
        cSharpConvertToMap.put("varchar", "Convert.ToString");
        cSharpConvertToMap.put("binary", "(byte[])");
        cSharpConvertToMap.put("varbinary", "(byte[])");
        cSharpConvertToMap.put("tinyblob", "(byte[])");
        cSharpConvertToMap.put("blob", "(byte[])");
        cSharpConvertToMap.put("mediumblob", "(byte[])");
        cSharpConvertToMap.put("longblob", "(byte[])");
        cSharpConvertToMap.put("tinytext", "Convert.ToString");
        cSharpConvertToMap.put("text", "Convert.ToString");
        cSharpConvertToMap.put("mediumtext", "Convert.ToString");
        cSharpConvertToMap.put("longtext", "Convert.ToString");
        cSharpConvertToMap.put("enum", "Convert.ToString");
        cSharpConvertToMap.put("set", "Convert.ToString");
        cSharpConvertToMap.put("date", "Convert.ToDateTime");
        cSharpConvertToMap.put("datetime", "Convert.ToDateTime");
        cSharpConvertToMap.put("timestamp", "Convert.ToDateTime");
        cSharpConvertToMap.put("time", "Convert.ToDateTime");
        cSharpConvertToMap.put("year", "Convert.ToInt32");
        cSharpConvertToMap.put("geometry", "(byte[])");

        return cSharpConvertToMap;
    }

    private static HashMap<String, String> initMySqlDbTypeMap() {
        HashMap<String, String> cSharpConvertToMap = new HashMap<>();

        cSharpConvertToMap.put("bit", "MySqlDbType.Bit");
        cSharpConvertToMap.put("tinyint", "MySqlDbType.Bit");
        cSharpConvertToMap.put("smallint", "MySqlDbType.Int32");
        cSharpConvertToMap.put("mediumint", "MySqlDbType.Int32");
        cSharpConvertToMap.put("int", "MySqlDbType.Int32");
        cSharpConvertToMap.put("bigint", "MySqlDbType.Int64");
        cSharpConvertToMap.put("numeric", "MySqlDbType.Double");
        cSharpConvertToMap.put("decimal", "MySqlDbType.Double");
        cSharpConvertToMap.put("double", "MySqlDbType.Double");
        cSharpConvertToMap.put("varchar", "MySqlDbType.VarChar");
        cSharpConvertToMap.put("date", "MySqlDbType.Date");
        cSharpConvertToMap.put("datetime", "MySqlDbType.Datetime");
        cSharpConvertToMap.put("timestamp", "MySqlDbType.Timestamp");
        cSharpConvertToMap.put("time", "MySqlDbType.Time");
        cSharpConvertToMap.put("year", "MySqlDbType.Year");

        return cSharpConvertToMap;
    }

    private static HashMap<String, String>  initCommentMap() {
        HashMap<String, String> commentsMap = new HashMap<>();
        
        // CREATE
        commentsMap.put("Cr", "Method which inserts the passed obj in the database.");  // summary
        commentsMap.put("CrP", "Is the passed obj which will be inserted in the database."); // param
        commentsMap.put("CrR", "A ResultObject wrapper detailing result of successful insertion or failure."); // returns
        
        // UPDATE
        commentsMap.put("Up",  "Method which updates the passed obj in the database.");
        commentsMap.put("UpP", "Is the passed obj whose existing record will be updated in the database.");
        commentsMap.put("UpR", "A ResultObject wrapper detailing result of successful update or failure.");
        
        // READ BY ID
        commentsMap.put("Ri", "Method which reads a single record from the database matching the passed obj.");
        commentsMap.put("RiP", "Is the passed obj that is to be fetched from the database if it exists.");
        commentsMap.put("RiR", "A ResultObject wrapper detailing result of successful fetch of the passed obj or failure.");
        
        // READ ALL
        commentsMap.put("Ra", "Method which reads all records from the database.");
        commentsMap.put("RaR", "A ResultObject wrapper detailing result of successful fetch of all records or failure.");
        
        // DELETE
        commentsMap.put("Dl", "Method which removes a record from the database based on the passed obj.");
        commentsMap.put("DlP", "Is the passed obj whose record will be soft-deleted from the database.");
        commentsMap.put("DlR", "A ResultObject wrapper detailing result of successful deletion or failure.");

        return commentsMap;
    }
}
