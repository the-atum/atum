
/** *****************************************************************************
 * Author:        Pawan Vyas (PBMV)
 * Email:         pawan.vyas.gitlab@gmail.com
 *
 * Project:       AtumDemo/MySql/CodeGenerator
 * File:          MySqlCsharpCodeGenerator.java
 *
 * Description:
 *      Concrete implementation of ICodeGenerator interface for generating
 *      code for Csharp language for MySql database.
 *
 * Purpose:
 *      Allows generating DO and DAL code written in Csharp for the given DB.
 *
 * Revision History:
 *      2019-May-19 (PBMV): File Created.
 *      2019-Sep-26 (PBMV): AtumMain and subsequent interface and/or factory
 *                          related changes.
 *      2019-Sep-26 (PBMV): ICodeGenerator interface related changes for demo.
 *      2020-May-25 (PBMV): IAtum removal and subsequent changes.
 *      2020-May-25 (PBMV): Added implementation for SetObjectGraph method and
 *                          related changes to move logic out from ctor.
 *      2020-May-25 (IVNR): Added initCsharpMap() method to make the Data Mapper
 *                          internalized.
 *      2020-May-30 (PBMV): Moved to atum.codegenerators.mysql package.
 *      2020-May-30 (PBMV): Fixed import errors due to package restructuring.
 *      2020-Jun-09 (PBMV): Fixed code generation errors.
 *      2020-Jun-09 (PBMV): Changed dataMapper field from static to instance.
 *      2020-Dec-28 (PBMV): Added implementation for ReadById and ReadAll code
 *                          generators and csharpConverterMap and initializer,
 *                          also updated csharpDataMapper to use double for
 *                          relevant datatypes instead of float.
 ****************************************************************************** */

package atum.codegenerators.mysql;

import java.util.ArrayList;
import java.util.HashMap;
import atum.interfaces.ICodeGenerator;
import atum.dataobjects.AtumColumnMetaData;
import atum.dataobjects.CodeFileInfo;
import atum.dataobjects.DbDetails;
import atum.dataobjects.ObjectGraph;
import atum.utils.AtumUtils;
import atum.utils.CodeFileWriter;

public class MySqlCsharpCodeGenerator implements ICodeGenerator {

    /**
     * Field which holds the retrieved ObjectGraph from the injected IDBReader.
     */
    private ObjectGraph internalObjGraph;

    private ArrayList<String> dbNames;
    private ArrayList<String> tables;
    private ArrayList<String> columns;
    private final static String N1 = "\n";
    private final static String N2 = "\n\n";
    private final static String T1 = "\t";
    private final static String T2 = "\t\t";
    private final static String T3 = "\t\t\t";
    private final static String T4 = "\t\t\t\t";

    // Creating a map to hold data mapper in Csharp.
    private HashMap<String, String> csharpDataMapper;
    private HashMap<String, String> csharpConverterMap;

    /**
     * Default constructor.
     */
    public MySqlCsharpCodeGenerator() {
        internalObjGraph = null;
        dbNames = null;
        tables = null;
        columns = null;
        csharpDataMapper = null;
    }

    @Override
    public void SetObjectGraph(ObjectGraph objGraph) {
        internalObjGraph = objGraph;
        internalObjGraph.driverName = "";
        dbNames = internalObjGraph.getDatabases();
        tables = new ArrayList<>();
        columns = new ArrayList<>();
        csharpDataMapper = initCsharpMap();
        csharpConverterMap = initCsharpConvertToMap();
    }

    @Override
    public void generateCode() throws Exception {
        if (null == internalObjGraph)
            throw new Exception("FATAL: ObjectGraph for this CodeGenerator not initialized."
                    + "\nEnsure that ObjectGraph is set before this method is invoked using"
                    + "the ICodeGenerator.SetObjectGraph method.");
        generateDO();
        generateDAL();
    }

    /**
     * Function to generate the DO for the passed dbReader in Csharp.
     *
     * @return a string of the generated DataObject Code.
     */
    private void generateDO() {

        // Getting DB name from dBReader.
        for (String dbName : dbNames) {

            // Getting tables list from dBReader.
            tables = internalObjGraph.getTables(dbName);

            // Defining and setting up CodeFileInfo object.
            CodeFileInfo codeFileInfo = new CodeFileInfo();
            codeFileInfo.langDirName = "CSharp/";
            codeFileInfo.dbDirName = dbName + "/";
            codeFileInfo.subTypeDirName = "DataObjects/";
            codeFileInfo.fileType = "DO";
            codeFileInfo.fileExtension = ".cs";

            // Iterating over each table and generating a DO class for that table.
            for (String table : tables) {

                // Updating the name of the table to follow CSharp specific class naming
                // standards.
                String fileTableName = AtumUtils.properCase(table);

                // Defining stringBuilder object which will hold the generated DO code
                // including new lines and tab indentations as and when code is generated.
                StringBuilder sbCode = new StringBuilder();

                sbCode.append("using System;").append(N1);
                sbCode.append(N1).append("/**\n * AutoGenerated DO code for table: ").append(fileTableName)
                        .append(" in ").append(dbName).append(" database.\n */").append(N1);
                sbCode.append("namespace ").append(dbName).append(" {").append(N1);
                sbCode.append(T1).append("public class ").append(fileTableName).append(codeFileInfo.fileType)
                        .append(" {").append(N1);

                // Getting columns list from dBReader.
                columns = internalObjGraph.getColumns(dbName, table);

                // Iterating of columns and generating code fields for each column.
                for (String column : columns) {

                    // Getting the AtumColumnMetaData object for this current column.
                    AtumColumnMetaData colData = internalObjGraph.dbMap.get(dbName).get(table).get(column);

                    // Updating the column Name for each field of the DO class to follow CSharp
                    // specific language standard.
                    String colName = AtumUtils.properCase(colData.columnName);
                    if (csharpDataMapper.containsKey(colData.dataType))
                        sbCode.append(T2).append("public ").append(csharpDataMapper.get(colData.dataType)).append(" ")
                                .append(colName).append(" { set; get; }").append(N1);
                }

                sbCode.append(T1).append("}").append(N1);
                sbCode.append("}").append(N1);

                // For each existing table in the list of tables, updating the codeFileInfo
                // parameter object's tableName to the current table name.
                // And updating its content field to the generated code in string.
                codeFileInfo.tableName = fileTableName;
                codeFileInfo.content = sbCode.toString();

                // Call CodeFileWriter's generateFile by passing it the fileInfo parameter
                // object.
                CodeFileWriter.generateFile(codeFileInfo);
            }
        }
    }

    /**
     * Function to generate the DAL for the passed objGraph.
     *
     * @return a string of the generated DAL Code.
     */
    private void generateDAL() {
        // Getting DB name from dBReader.
        for (String dbName : dbNames) {

            // Getting tables list from dBReader.
            tables = internalObjGraph.getTables(dbName);

            // Defining and setting up CodeFileInfo object.
            CodeFileInfo codeFileInfo = new CodeFileInfo();
            codeFileInfo.langDirName = "Csharp/";
            codeFileInfo.dbDirName = dbName + "/";
            codeFileInfo.subTypeDirName = "DAL/";
            codeFileInfo.fileType = "DAL";
            codeFileInfo.fileExtension = ".cs";

            // Creating DbDetails object which is a paramObj;
            DbDetails dbDetails = new DbDetails();

            // Iterating over each table and generating a DAL class for that table.
            for (String table : tables) {

                // Updating the name of the table to follow Csharp specific class name
                // Standards.
                String fileTableName = AtumUtils.properCase(table);
                // Defining stringBuilder object which will hold the generated DAL code
                // including new lines and tab indentations as and when code is generated.
                StringBuilder sbCode = new StringBuilder();

                sbCode.append(N1).append("/**\n * AutoGenerated DAL code for table: ").append(fileTableName)
                        .append(" in ").append(dbName).append(" database.\n */").append(N1);

                sbCode.append("using System;").append(N1);
                sbCode.append("using System.Collections.Generic;").append(N1);
                sbCode.append("using System.Data;").append(N1);
                sbCode.append("using System.Configuration;").append(N1);
                sbCode.append("using System.ComponentModel;").append(N1);
                sbCode.append("using MySql.Data.MySqlClient;").append(N2);

                sbCode.append("namespace ").append(dbName).append(" {").append(N1);
                sbCode.append(T1).append("public abstract class ").append(fileTableName).append(codeFileInfo.fileType)
                        .append(" {").append(N1);

                dbDetails.dbName = dbName;
                dbDetails.fileTableName = fileTableName;
                dbDetails.table = table;

                // Calling the generateConnectionStringField method to append the field for
                // connection string.
                sbCode.append(N1).append(generateConnectionStringField(dbDetails)).append(N1);

                // Calling the generateConnectionStringField method to append the field for
                // connection string.
                sbCode.append(N1).append(generateGetCommandObject(dbDetails)).append(N1);

                // Calling the generatedDALCreate method to append the create method.
                sbCode.append(N1).append(generateDALCreate(dbDetails)).append(N1);

                // Calling the generateDALUpdate method to append the update method.
                sbCode.append(N1).append(generateDALUpdate(dbDetails)).append(N1);

                // Calling the generateDALDelete method to append the update method.
                sbCode.append(N1).append(generateDALDelete(dbDetails)).append(N1);

                // Calling the generateDALRead method to append the read method.
                sbCode.append(N1).append(generateDALReadById(dbDetails)).append(N1);

                // Calling the generatedDALReadAll method to append the readAll method.
                sbCode.append(N1).append(generateDALReadAll(dbDetails)).append(N1);

                sbCode.append(N1).append(T1).append("}").append(N1);
                sbCode.append("}").append(N2);

                // For each existing table in the list of tables, updating the codeFileInfo
                // parameter object's tableName to the current table name.
                // And updating its content field to the generated code in string.
                codeFileInfo.tableName = fileTableName;
                codeFileInfo.content = sbCode.toString();

                // Call CodeFileWriter's generateFile by passing it the fileInfo parameter
                // object.
                CodeFileWriter.generateFile(codeFileInfo);
            }
        }
    }

    private String generateConnectionStringField(DbDetails dbDetails) {

        // Server=myServerAddress;Port=1234;Database=myDataBase;Uid=myUsername;Pwd=myPassword;
        String serverAddress = "";
        String portNumber = "";
        String[] connURLItems = internalObjGraph.connParamObj.dbConnectionURL.split(":");
        if (connURLItems.length >= 2) {
            serverAddress = connURLItems[0];
            portNumber = connURLItems[1];
        }
        String userName = internalObjGraph.connParamObj.dbUserName;
        String password = internalObjGraph.connParamObj.dbUserPassword;
        String dbName = dbDetails.dbName;

        // Defining StringBuilder object which will hold the generated Command.
        StringBuilder sbCode = new StringBuilder();
        sbCode.append(T2).append("protected const string ConnectionString = ").append("\"Server=").append(serverAddress)
                .append(";Port=").append(portNumber).append(";Database=").append(dbName).append(";Uid=")
                .append(userName).append(";Pwd=").append(password).append("\";");

        // Returning the string from the sbCode to the caller.
        return sbCode.toString();
    }

    /**
     * Function to generate code for MySqlCommand object.
     * 
     * @param dbDetails Is the passed DbDetails param-obj which contains relevant
     *                  data needed.
     *
     * @return the String of MySqlCommand object
     */
    private String generateGetCommandObject(DbDetails dbDetails) {

        // Defining StringBuilder object which will hold the generated Command.
        StringBuilder sbCode = new StringBuilder();

        sbCode.append(T2).append("protected MySqlCommand GetCommandObj(string procedureName) {").append(N1);
        sbCode.append(T3).append("return new MySqlCommand(")
                .append("procedureName, new MySqlConnection(ConnectionString));").append(N1);
        sbCode.append(T2).append("}");

        // Returning the string to the caller of the function.
        return sbCode.toString();
    }

    /**
     * Function to generate create code for DAL.
     *
     * @return the String of create code
     */
    private String generateDALCreate(DbDetails dbDetails) {
        // Defining StringBuilder object which will hold the generated create code.
        StringBuilder sbCode = new StringBuilder();

        // The extension of the stored procedure.
        String spName = dbDetails.table + "_SP_CREATE";
        String tableDOName = AtumUtils.properCase(dbDetails.table) + "DO";
        String tableDOObjName = dbDetails.table + "Obj";

        sbCode.append(T2).append("public bool ").append("Create(").append(tableDOName)
                .append(" ").append(tableDOObjName).append(") {").append(N1);

        sbCode.append(T3).append("MySqlCommand mysqlCmd = GetCommandObj(\"").append(spName).append("\");").append(N1);
        sbCode.append(T3).append("mysqlCmd.CommandType = CommandType.StoredProcedure;").append(N1);

        // Getting columns list from dBReader.
        columns = internalObjGraph.getColumns(dbDetails.dbName, dbDetails.table);

        // Iterating through the columns of the table.
        for (String column : columns) {

            // Retrieving the columnMetaData of the particular column.
            AtumColumnMetaData colData = internalObjGraph.dbMap.get(dbDetails.dbName).get(dbDetails.table).get(column);
            if (!colData.isPrimaryKey) {
                String colFieldName = AtumUtils.properCase(colData.columnName);
                String colSPParamName = "param_" + colData.columnName;
                sbCode.append(T3).append("mysqlCmd.Parameters.AddWithValue(\"").append(colSPParamName).append("\", ")
                        .append(tableDOObjName).append(".").append(colFieldName).append(");").append(N1);
            }
        }

        sbCode.append(T3).append("mysqlCmd.Connection.Open();").append(N1);
        sbCode.append(T3).append("int rowsAffected = mysqlCmd.ExecuteNonQuery();").append(N1);
        sbCode.append(T3).append("mysqlCmd.Connection.Close();").append(N1);
        sbCode.append(T3).append("return 0 != rowsAffected ? true : false;").append(N1);

        // End of generated function.
        sbCode.append(T2).append("}");

        // Returning the string to the caller of the function.
        return sbCode.toString();
    }

    /**
     * Function to generate Update code for DAL.
     *
     * @return the String of Update code.
     */
    private String generateDALUpdate(DbDetails dbDetails) {
        // Defining StringBuilder object which will hold the generated create code.
        StringBuilder sbCode = new StringBuilder();

        // The extension of the stored procedure.
        String spName = dbDetails.table + "_SP_UPDATE";
        String tableDOName = AtumUtils.properCase(dbDetails.table) + "DO";
        String tableDOObjName = dbDetails.table + "Obj";

        sbCode.append(T2).append("public bool ").append("Update(").append(tableDOName)
                .append(" ").append(tableDOObjName).append(") {").append(N1);

        sbCode.append(T3).append("MySqlCommand mysqlCmd = GetCommandObj(\"").append(spName).append("\");").append(N1);
        sbCode.append(T3).append("mysqlCmd.CommandType = CommandType.StoredProcedure;").append(N1);

        // Getting columns list from dBReader.
        columns = internalObjGraph.getColumns(dbDetails.dbName, dbDetails.table);

        // Iterating through the columns of the table.
        for (String column : columns) {

            // Retrieving the columnMetaData of the particular column.
            AtumColumnMetaData colData = internalObjGraph.dbMap.get(dbDetails.dbName).get(dbDetails.table).get(column);
            String colFieldName = AtumUtils.properCase(colData.columnName);
            String colSPParamName = "param_" + colData.columnName;
            sbCode.append(T3).append("mysqlCmd.Parameters.AddWithValue(\"").append(colSPParamName).append("\", ")
                    .append(tableDOObjName).append(".").append(colFieldName).append(");").append(N1);
        }

        sbCode.append(T3).append("mysqlCmd.Connection.Open();").append(N1);
        sbCode.append(T3).append("int rowsAffected = mysqlCmd.ExecuteNonQuery();").append(N1);
        sbCode.append(T3).append("mysqlCmd.Connection.Close();").append(N1);
        sbCode.append(T3).append("return 0 != rowsAffected ? true : false;").append(N1);

        // End of generated function.
        sbCode.append(T2).append("}");

        // Returning the string to the caller of the function.
        return sbCode.toString();
    }

    /**
     * Function to generate Delete code for DAL.
     *
     * @return the String of Delete code.
     */
    private String generateDALDelete(DbDetails dbDetails) {
        // Defining StringBuilder object which will hold the generated create code.
        StringBuilder sbCode = new StringBuilder();

        // The extension of the stored procedure.
        String spName = dbDetails.table + "_SP_DELETE";
        String tableDOName = AtumUtils.properCase(dbDetails.table) + "DO";
        String tableDOObjName = dbDetails.table + "Obj";

        sbCode.append(T2).append("public bool ").append("Delete(").append(tableDOName)
                .append(" ").append(tableDOObjName).append(") {").append(N1);

        sbCode.append(T3).append("MySqlCommand mysqlCmd = GetCommandObj(\"").append(spName).append("\");").append(N1);
        sbCode.append(T3).append("mysqlCmd.CommandType = CommandType.StoredProcedure;").append(N1);

        // Getting columns list from dBReader.
        columns = internalObjGraph.getColumns(dbDetails.dbName, dbDetails.table);

        // Iterating through the columns of the table.
        for (String column : columns) {

            // Retrieving the columnMetaData of the particular column.
            AtumColumnMetaData colData = internalObjGraph.dbMap.get(dbDetails.dbName).get(dbDetails.table).get(column);

            if (colData.isPrimaryKey) {
                String colFieldName = AtumUtils.properCase(colData.columnName);
                String colSPParamName = "param_" + colData.columnName;
                sbCode.append(T3).append("mysqlCmd.Parameters.AddWithValue(\"").append(colSPParamName).append("\", ")
                        .append(tableDOObjName).append(".").append(colFieldName).append(");").append(N1);
                break;
            }
        }

        sbCode.append(T3).append("mysqlCmd.Connection.Open();").append(N1);
        sbCode.append(T3).append("int rowsAffected = mysqlCmd.ExecuteNonQuery();").append(N1);
        sbCode.append(T3).append("mysqlCmd.Connection.Close();").append(N1);
        sbCode.append(T3).append("return 0 != rowsAffected ? true : false;").append(N1);

        // End of generated function.
        sbCode.append(T2).append("}");

        // Returning the string to the caller of the function.
        return sbCode.toString();
    }

    /**
     * Function to generate ReadById code for DAL.
     *
     * @return the String of ReadById code.
     */
    private String generateDALReadById(DbDetails dbDetails) {

        // Defining StringBuilder object which will hold the generated create code.
        StringBuilder sbCode = new StringBuilder();

        // The extension of the stored procedure.
        String spName = dbDetails.table + "_SP_READBYID";
        String tableDOName = AtumUtils.properCase(dbDetails.table) + "DO";
        String tableDOObjName = dbDetails.table + "Obj";

        // Getting columns list from dBReader.
        columns = internalObjGraph.getColumns(dbDetails.dbName, dbDetails.table);

        // Finding primary key for the table.
        AtumColumnMetaData primaryColData = null;
        for (String column : columns) {

            // Retrieving the columnMetaData of the particular column.
            primaryColData = internalObjGraph.dbMap.get(dbDetails.dbName).get(dbDetails.table).get(column);

            if (primaryColData.isPrimaryKey) {
                break;
            }
        }

        // Only run if at least one of the columns is a primary key.
        if (primaryColData.isPrimaryKey) {

            String primaryFieldName = AtumUtils.properCase(primaryColData.columnName);

            sbCode.append(T2).append("public ").append(tableDOName).append(" ").append("ReadById(")
                    .append(csharpDataMapper.get(primaryColData.dataType)).append(" ").append(primaryFieldName)
                    .append(") {").append(N1);

            sbCode.append(T3).append(tableDOName).append(" ").append(tableDOObjName).append("= new ")
                    .append(tableDOName).append("();").append(N1);

            sbCode.append(T3).append("MySqlCommand mysqlCmd = GetCommandObj(\"").append(spName).append("\");")
                    .append(N1);
            sbCode.append(T3).append("mysqlCmd.CommandType = CommandType.StoredProcedure;").append(N1);

            String primaryColSPParamName = "param_" + primaryColData.columnName;
            sbCode.append(T3).append("mysqlCmd.Parameters.AddWithValue(\"").append(primaryColSPParamName).append("\", ")
                    .append(primaryFieldName).append(");").append(N1);

            sbCode.append(T3).append("mysqlCmd.Connection.Open();").append(N1);
            sbCode.append(T3).append("MySqlDataReader mysqlDR = mysqlCmd.ExecuteReader();").append(N1);
            sbCode.append(T3).append("while(mysqlDR.Read()) {").append(N1);

            for (String column : columns) {
                AtumColumnMetaData colData = internalObjGraph.dbMap.get(dbDetails.dbName).get(dbDetails.table)
                        .get(column);
                String colFieldName = AtumUtils.properCase(colData.columnName);
                sbCode.append(T4).append(tableDOObjName).append(".").append(colFieldName).append(" = ")
                        .append(csharpConverterMap.get(colData.dataType)).append("(").append("mysqlDR[\"")
                        .append(colData.columnName).append("\"]);").append(N1);
            }

            sbCode.append(T3).append("}").append(N1);
            sbCode.append(T3).append("mysqlDR.Close();").append(N1);
            sbCode.append(T3).append("mysqlCmd.Connection.Close();").append(N1);
            sbCode.append(T3).append("return ").append(tableDOObjName).append(";").append(N1);

            // End of generated function.
            sbCode.append(T2).append("}");
        }

        // Returning the string to the caller of the function.
        return sbCode.toString();
    }

    private String generateDALReadAll(DbDetails dbDetails) {

        // Defining StringBuilder object which will hold the generated create code.
        StringBuilder sbCode = new StringBuilder();

        // The extension of the stored procedure.
        String spName = dbDetails.table + "_SP_READALL";
        String tableDOName = AtumUtils.properCase(dbDetails.table) + "DO";
        String tableDOObjName = dbDetails.table + "Obj";

        String listTypeName = "List<" + tableDOName + ">";
        String listObjName = dbDetails.table + "List";

        sbCode.append(T2).append("public ").append(listTypeName).append(" ReadAll() {").append(N1);

        sbCode.append(T3).append(listTypeName).append(" ").append(listObjName).append(" = new ").append(listTypeName)
                .append("();").append(N1);

        sbCode.append(T3).append("MySqlCommand mysqlCmd = GetCommandObj(\"").append(spName).append("\");").append(N1);
        sbCode.append(T3).append("mysqlCmd.CommandType = CommandType.StoredProcedure;").append(N1);

        sbCode.append(T3).append("mysqlCmd.Connection.Open();").append(N1);
        sbCode.append(T3).append("MySqlDataReader mysqlDR = mysqlCmd.ExecuteReader();").append(N1);
        sbCode.append(T3).append("while(mysqlDR.Read()) {").append(N1);
        sbCode.append(T4).append(tableDOName).append(" ").append(tableDOObjName).append(" = new ").append(tableDOName)
                .append("();").append(N1);

        for (String column : columns) {
            AtumColumnMetaData colData = internalObjGraph.dbMap.get(dbDetails.dbName).get(dbDetails.table).get(column);
            String colFieldName = AtumUtils.properCase(colData.columnName);
            sbCode.append(T4).append(tableDOObjName).append(".").append(colFieldName).append(" = ")
                    .append(csharpConverterMap.get(colData.dataType)).append("(").append("mysqlDR[\"")
                    .append(colData.columnName).append("\"]);").append(N1);
        }

        sbCode.append(T4).append(listObjName).append(".Add(").append(tableDOObjName).append(");").append(N1);

        sbCode.append(T3).append("}").append(N1);
        sbCode.append(T3).append("mysqlDR.Close();").append(N1);
        sbCode.append(T3).append("mysqlCmd.Connection.Close();").append(N1);

        sbCode.append(T3).append("return ").append(listObjName).append(";").append(N1);

        // End of generated function.
        sbCode.append(T2).append("}");

        // Returning the string to the caller of the function.
        return sbCode.toString();
    }

    /**
     * The below method is used to initialize the csharp Data mapper where key is
     * datatype in mysql and value is the datatype in Csharp.
     * 
     * @return a HashMap of <String, String>.
     */
    private static HashMap<String, String> initCsharpMap() {

        // Instantiating csharpMap map where key is datatype in mysql and
        // value is the datatype in Csharp.
        HashMap<String, String> csharpMap = new HashMap<>();

        csharpMap.put("tinyint", "int");
        csharpMap.put("smallint", "int");
        csharpMap.put("mediumint", "int");
        csharpMap.put("int", "int");
        csharpMap.put("bigint", "long");
        csharpMap.put("bit", "bool");
        csharpMap.put("numeric", "double");
        csharpMap.put("decimal", "double");
        csharpMap.put("double", "double");
        csharpMap.put("char", "string");
        csharpMap.put("varchar", "string");
        csharpMap.put("binary", "byte[]");
        csharpMap.put("varbinary", "byte[]");
        csharpMap.put("tinyblob", "byte[]");
        csharpMap.put("blob", "byte[]");
        csharpMap.put("mediumblob", "byte[]");
        csharpMap.put("longblob", "byte[]");
        csharpMap.put("tinytext", "string");
        csharpMap.put("text", "string");
        csharpMap.put("mediumtext", "string");
        csharpMap.put("longtext", "string");
        csharpMap.put("enum", "string");
        csharpMap.put("set", "string");
        csharpMap.put("date", "DateTime");
        csharpMap.put("datetime", "DateTime");
        csharpMap.put("timestamp", "DateTime");
        csharpMap.put("time", "DateTime");
        csharpMap.put("year", "int");
        csharpMap.put("geometry", "byte[]");

        // Returning csharpMap to the caller.
        return csharpMap;
    }

    private static HashMap<String, String> initCsharpConvertToMap() {
        HashMap<String, String> cSharpConvertToMap = new HashMap<>();

        cSharpConvertToMap.put("tinyint", "Convert.ToInt32");
        cSharpConvertToMap.put("smallint", "Convert.ToInt32");
        cSharpConvertToMap.put("mediumint", "Convert.ToInt32");
        cSharpConvertToMap.put("int", "Convert.ToInt32");
        cSharpConvertToMap.put("bigint", "Convert.ToInt64");
        cSharpConvertToMap.put("bit", "Convert.ToBoolean");
        cSharpConvertToMap.put("numeric", "Convert.ToDouble");
        cSharpConvertToMap.put("decimal", "Convert.ToDouble");
        cSharpConvertToMap.put("double", "Convert.ToDouble");
        cSharpConvertToMap.put("char", "Convert.ToString");
        cSharpConvertToMap.put("varchar", "Convert.ToString");
        cSharpConvertToMap.put("binary", "(byte[])");
        cSharpConvertToMap.put("varbinary", "(byte[])");
        cSharpConvertToMap.put("tinyblob", "(byte[])");
        cSharpConvertToMap.put("blob", "(byte[])");
        cSharpConvertToMap.put("mediumblob", "(byte[])");
        cSharpConvertToMap.put("longblob", "(byte[])");
        cSharpConvertToMap.put("tinytext", "Convert.ToString");
        cSharpConvertToMap.put("text", "Convert.ToString");
        cSharpConvertToMap.put("mediumtext", "Convert.ToString");
        cSharpConvertToMap.put("longtext", "Convert.ToString");
        cSharpConvertToMap.put("enum", "Convert.ToString");
        cSharpConvertToMap.put("set", "Convert.ToString");
        cSharpConvertToMap.put("date", "Convert.ToDateTime");
        cSharpConvertToMap.put("datetime", "Convert.ToDateTime");
        cSharpConvertToMap.put("timestamp", "Convert.ToDateTime");
        cSharpConvertToMap.put("time", "Convert.ToDateTime");
        cSharpConvertToMap.put("year", "Convert.ToInt32");
        cSharpConvertToMap.put("geometry", "(byte[])");

        return cSharpConvertToMap;
    }
}
