/** *****************************************************************************
 * Author:        Pawan Vyas (PBMV)
 * Email:         pawan.vyas.gitlab@gmail.com
 *
 * Project:       AtumDemo/MySql/CodeGenerator
 * File:          JavaCodeGenerator.java
 *
 * Description:
 *      Concrete implementation of ICodeGenerator interface for generating
 *      code for Java language for MySql database.
 *
 * Purpose:
 *      Allows generating DO and DAL code written in Java for the given DB.
 *
 * Revision History:
 *      2019-Apr-30 (PBMV): File Created.
 *      2019-May-19 (PBMV): Removed dummy DAL and BAL methods and updated
 *                          comment for generated DO class.
 *      2019-Sep-26 (PBMV): AtumMain and subsequent interface and/or factory
 *                          related changes.
 *      2019-Sep-26 (PBMV): ICodeGenerator interface related changes for demo.
 *      2019-Nov-14 (IVNR): Added generateDAL() method.
 *      2019-Dec-12 (IVNR): Updated generateDAL() method.
 *      2020-May-25 (PBMV): IAtum removal and subsequent changes.
 *      2020-May-25 (PBMV): Added implementation for SetObjectGraph method and
 *                          related changes to move logic out from ctor.
 *      2020-May-25 (IVNR): Added initJavaMap() method to make the Data Mapper
 *                          internalized.
 *      2020-May-28 (IVNR): Added getPrimaryKeyMap() method, updated 
 *                          generateDAL() method, generateDALCreate() method,
 *                          generateDALReadById() method and 
 *                          generateDALDelete() method.
 *      2020-May-30 (PBMV): Moved to atum.codegenerators.mysql package.
 *      2020-May-30 (PBMV): Fixed import errors due to package restructuring.
 *      2020-Jun-09 (PBMV): Changed dataMapper field from static to instance.
 *      2020-Jun-09 (IVNR): Updated generateDAL()method, generateDALCreate() 
 *                          method, generateDALReadAll() method, 
 *                          generateDALReadById() method, generateDALUpdate() 
 *                          method, and generateDALDelete() method.
 ****************************************************************************** */

package atum.codegenerators.mysql;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import atum.interfaces.ICodeGenerator;
import atum.dataobjects.AtumColumnMetaData;
import atum.dataobjects.CodeFileInfo;
import atum.dataobjects.DbDetails;
import atum.dataobjects.ObjectGraph;
import atum.utils.AtumUtils;
import atum.utils.CodeFileWriter;

public class MySqlJavaCodeGenerator implements ICodeGenerator {

    private ObjectGraph internalObjGraph;

    private ArrayList<String> dbNames;
    private ArrayList<String> tables;
    private ArrayList<String> columns;
    private final static String N1 = "\n";
    private final static String N2 = "\n\n";
    private final static String T1 = "\t";
    private final static String T2 = "\t\t";
    private final static String T3 = "\t\t\t";

    // Creating a map to hold data mapper in java.
    private HashMap<String, String> javaDataMapper;
    
    /**
     * Default constructor.
     */
    public MySqlJavaCodeGenerator() {
        internalObjGraph = null;
        dbNames = null;
        tables = null;
        columns = null;
    }
    
    @Override
    public void SetObjectGraph(ObjectGraph objGraph) {
        internalObjGraph = objGraph;
        internalObjGraph.driverName = "com.mysql.cj.jdbc.Driver";
        dbNames = internalObjGraph.getDatabases();
        tables = new ArrayList<>();
        columns = new ArrayList<>();
        javaDataMapper = initJavaMap();
    }

    @Override
    public void generateCode() throws Exception {
        if (null == internalObjGraph)
            throw new Exception("FATAL: ObjectGraph for this CodeGenerator not initialized."
                    + "\nEnsure that ObjectGraph is set before this method is invoked using"
                    + "the ICodeGenerator.SetObjectGraph method.");

        generateDO();
        generateDAL();
    }

    /**
     * Function to generate the DO for the passed dbReader.
     *
     * @return a string of the generated DataObject Code.
     */
    private void generateDO() {

        for (String dbName : dbNames) {

            // Getting tables list from dBReader.
            tables = internalObjGraph.getTables(dbName);

            // Defining and setting up CodeFileInfo object.
            CodeFileInfo codeFileInfo = new CodeFileInfo();
            codeFileInfo.langDirName = "Java/";
            codeFileInfo.dbDirName = dbName + "/";
            codeFileInfo.subTypeDirName = "DataObjects/";
            codeFileInfo.fileType = "DO";
            codeFileInfo.fileExtension = ".java";

            // Iterating over each table and generating a DO class for that table.
            for (String table : tables) {

                // Updating the name of the table to follow Java specific class name Standards.
                String fileTableName = table.substring(0, 1).toUpperCase() + table.substring(1).toLowerCase();

                // Defining stringBuilder object which will hold the generated DO code
                // including new lines and tab indentations as and when code is generated.
                StringBuilder sbCode = new StringBuilder();

                sbCode.append("\n/**\n * AutoGenerated DO code for table: ").append(fileTableName).append(" in ")
                        .append(dbName).append(" database.\n */\n");
                sbCode.append("public class ").append(fileTableName).append(codeFileInfo.fileType).append(" {")
                        .append(N1);

                // Getting columns list from dBReader.
                columns = internalObjGraph.getColumns(dbName, table);

                // Iterating of columns and generating code fields for each column.
                for (String column : columns) {

                    // Getting the AtumColumnMetaData object for this current column.
                    AtumColumnMetaData colData = internalObjGraph.dbMap.get(dbName).get(table).get(column);

                    // Checking if the column type exist in the map.
                    if (javaDataMapper.containsKey(colData.dataType))
                        sbCode.append(T1).append("public ").append(javaDataMapper.get(colData.dataType)).append(" ")
                                .append(colData.columnName).append(";").append(N1);
                }

                sbCode.append("}").append(N1);

                // For each existing table in the list of tables, updating the codeFileInfo
                // parameter object's tableName to the current table name.
                // And updating its content field to the generated code in string.
                codeFileInfo.tableName = fileTableName;
                codeFileInfo.content = sbCode.toString();

                // Call CodeFileWriter's generateFile by passing it the fileInfo parameter
                // object.
                CodeFileWriter.generateFile(codeFileInfo);
            }
        }
    }

    /**
     * Function to generate the DAL for the passed dbReader.
     *
     * @return a string of the generated DAL Code.
     */
    private void generateDAL() {

        // Getting DB name from dBReader.
        for (String dbName : dbNames) {

            // Getting tables list from dBReader.
            tables = internalObjGraph.getTables(dbName);

            // Defining and setting up CodeFileInfo object.
            CodeFileInfo codeFileInfo = new CodeFileInfo();
            codeFileInfo.langDirName = "Java/";
            codeFileInfo.dbDirName = dbName + "/";
            codeFileInfo.subTypeDirName = "DAL/";
            codeFileInfo.fileType = "DAL";
            codeFileInfo.fileExtension = ".java";

            // Creating DbDetails object which is a paramObj;
            DbDetails dbDetails = new DbDetails();

            dbDetails.setCallMap = retrieveSetCallMap();
            dbDetails.getCallMap = retrieveGetCallMap();

            // Iterating over each table and generating a DAL class for that table.
            for (String table : tables) {

                // Updating the name of the table to follow Java specific class name Standards.
                String fileTableName = AtumUtils.properCase(table);
                // Defining stringBuilder object which will hold the generated DAL code
                // including new lines and tab indentations as and when code is generated.
                StringBuilder sbCode = new StringBuilder();

                sbCode.append("\n/**\n * AutoGenerated DAL code for table: ").append(fileTableName).append(" in ")
                        .append(dbName).append(" database.\n */\n");
                sbCode.append("import java.sql.Connection;").append(N1);
                sbCode.append("import java.sql.CallableStatement; ").append(N1);
                sbCode.append("import java.sql.ResultSet;").append(N1);
                sbCode.append("import java.sql.DriverManager;").append(N1);
                sbCode.append("import java.util.LinkedHashMap;").append(N1);
                sbCode.append(N2);

                sbCode.append("public class ").append(fileTableName).append(codeFileInfo.fileType).append(" {")
                        .append(N1);

                dbDetails.dbName = dbName;
                dbDetails.fileTableName = fileTableName;
                dbDetails.table = table; 

                // Retrieving column list from object graph.
                dbDetails.columns = internalObjGraph.getColumns(dbName, table);
    
                // Retrieving the primaryKeyMap from the method.
                dbDetails.primaryKeyMap = getPrimaryKeyMap(dbDetails);

                // Calling the generateGetConnection method to append the connection method.
                sbCode.append(T1).append(generateGetConnection(dbDetails)).append(N1);

                // Calling the generatedDALCreate method to append the create method.
                sbCode.append(T1).append(generateDALCreate(dbDetails)).append(N1);

                // Calling the generatedDALReadAll method to append the readAll method.
                sbCode.append(T1).append(generateDALReadAll(dbDetails)).append(N1);

                // Calling the generateDALReadById method to append the read by id method.
                sbCode.append(T1).append(generateDALReadById(dbDetails)).append(N1);

                // Calling the generateDALUpdate method to append the update method.
                sbCode.append(T1).append(generateDALUpdate(dbDetails)).append(N1);

                // Calling the generateDALDelete method to append the delete method.
                sbCode.append(T1).append(generateDALDelete(dbDetails)).append(N1);

                sbCode.append("}").append(N1);

                // For each existing table in the list of tables, updating the codeFileInfo
                // parameter object's tableName to the current table name.
                // And updating its content field to the generated code in string.
                codeFileInfo.tableName = fileTableName;
                codeFileInfo.content = sbCode.toString();

                // Call CodeFileWriter's generateFile by passing it the fileInfo parameter
                // object.
                CodeFileWriter.generateFile(codeFileInfo);
            }
        }
    }

    /**
     * Function to generate connection code for DAL.
     *
     * @return the String of connection code
     */
    private String generateGetConnection(DbDetails dbDetails) {

        // Defining StringBuilder object which will hold the generated connection code.
        StringBuilder sbCode = new StringBuilder();

        sbCode.append("private Connection getConnectionObj() throws Exception {").append(N1);

        sbCode.append(T2).append("Connection connection = null; ").append(N1);

        sbCode.append(T2).append("String driverName = \"").append(internalObjGraph.driverName).append("\";").append(N1);

        sbCode.append(T2).append("String connectionString = \"jdbc:mysql://")
                .append(internalObjGraph.connParamObj.dbConnectionURL).append("/").append(dbDetails.dbName)
                .append("\";").append(N1);

        sbCode.append(T2).append("String userName = \"").append(internalObjGraph.connParamObj.dbUserName).append("\";")
                .append(N1);

        sbCode.append(T2).append("String password = \"").append(internalObjGraph.connParamObj.dbUserPassword)
                .append("\";").append(N1);

        sbCode.append(T2).append("Class.forName(driverName);").append(N1);

        sbCode.append(T2).append("connection = DriverManager.getConnection(connectionString, userName, password);")
                .append(N1);

        sbCode.append(T2).append("return connection;")
                .append(N1);

        sbCode.append(T1).append("}").append(N1);

        // Returning the string to the caller of the function.
        return sbCode.toString();
    }

    /**
     * Function to generate create code for DAL.
     *
     * @return the String of create code.
     */
    private String generateDALCreate(DbDetails dbDetails) {

        // Defining StringBuilder object which will hold the generated create code.
        StringBuilder sbCode = new StringBuilder();

        // The extension of the stored procedure.
        String spExtension = "_SP_CREATE";
        sbCode.append("public boolean ").append(dbDetails.table).append("Create(").append(dbDetails.fileTableName)
                .append("DO ").append(dbDetails.table).append("DO").append(") throws Exception {").append(N1);

        sbCode.append(T2).append("Connection connection = null;").append(N1);

        sbCode.append(T2).append("connection = getConnectionObj();").append(N1);

        // Getting columns list from dBDetails.
        columns = dbDetails.columns;

        // Retrieving the number of columns.
        int numberOfColumns = columns.size();

        // Size of primaryKeyMap.
        int sizeOfPrimaryKeyMap = dbDetails.primaryKeyMap.size();

        // Subtracting numberOfColumns from primaryKeyMap.
        numberOfColumns = numberOfColumns - sizeOfPrimaryKeyMap;

        // Number of inputs stores the ? which would be asked for call.
        String numberOfInputs = "";
        for (int i = 0; i < numberOfColumns; i++) {
            
            numberOfInputs += "?";
            if (i != numberOfColumns - 1)
                numberOfInputs += ", ";
        }

        sbCode.append(T2).append("CallableStatement callableStatement = connection.prepareCall(\"call ")
                .append(dbDetails.table).append(spExtension).append("(").append(numberOfInputs).append(")\");")
                .append(N1);

        // Initializing count = 1;
        int count = 1;

        // Iterating through the columns of the table.
        for (String column : columns) {

            // Retrieving the columnMetaData of the particular column.
            AtumColumnMetaData colData = internalObjGraph.dbMap.get(dbDetails.dbName).get(dbDetails.table).get(column);

            // Checking if the column is a primary key.
            if (colData.isPrimaryKey)
                continue;

            // Checking if the column type exist in the map.
            if (javaDataMapper.containsKey(colData.dataType)) {
                // Retrieving data type from map.
                String dataType = javaDataMapper.get(colData.dataType);

                // Checking if the columnType exist in the map.
                if (dbDetails.setCallMap.containsKey(dataType))

                    // Append the value to the callableStatement.
                    sbCode.append(T2).append("callableStatement.").append(dbDetails.setCallMap.get(dataType))
                            .append("(").append(count).append(", ").append(dbDetails.table).append("DO.")
                            .append(colData.columnName).append(");").append(N1);

            }
            count++;
        }
        sbCode.append(T2).append("int i = callableStatement.executeUpdate();").append(N1);
        sbCode.append(T2).append("return 0 != i;").append(N1);

        // End of generated function.
        sbCode.append(T1).append("}").append(N1);

        // Returning the string to the caller of the function.
        return sbCode.toString();
    }

    /**
     * Function to generate ReadAll code for DAL.
     *
     * @return the String of readAll code.
     */
    private String generateDALReadAll(DbDetails dbDetails) {

        // Defining StringBuilder object which will hold the generated readAll code.
        StringBuilder sbCode = new StringBuilder();

        // The extension of the DO and stored procedure.
        String CapsDoExtension = dbDetails.fileTableName + "DO";
        String spExtension = dbDetails.table + "_SP_READALL";
        String tableDoExtension = dbDetails.table + "DO";
        String tableMapExtension = tableDoExtension + "Map";

        sbCode.append("public LinkedHashMap<Integer, " + CapsDoExtension + "> ").append(dbDetails.table)
                .append("ReadAll() throws Exception {").append(N1);

        sbCode.append(T2).append("Connection connection = null;").append(N1);

        sbCode.append(T2).append("LinkedHashMap<Integer, " + CapsDoExtension + "> ").append(tableMapExtension)
                .append(" = ").append("new LinkedHashMap<>();").append(N1);

        sbCode.append(T2).append("connection = getConnectionObj();").append(N1);

        // Getting columns list from dbDetails.
        columns = dbDetails.columns;

        sbCode.append(T2).append("CallableStatement callableStatement = connection.prepareCall(\"call ")
                .append(spExtension).append("()\");").append(N1);

        sbCode.append(T2).append("ResultSet rs = callableStatement.executeQuery();").append(N1);

        sbCode.append(T2).append(CapsDoExtension).append(" ").append(tableDoExtension).append(" = ").append("new ")
                .append(CapsDoExtension).append("();").append(N1);

        sbCode.append(T2).append("while (rs.next()) {").append(N1); 

        // Creating empty string to hold primary key column name.
        String primaryIdName = "";

        // Iterating through the columns of the table.
        for (String column : columns) {

            // Retrieving the columnMetaData of the particular column.
            AtumColumnMetaData colData = internalObjGraph.dbMap.get(dbDetails.dbName).get(dbDetails.table).get(column);

            // Retrieving the name of the column.
            String columnName = colData.columnName;

            // Checking if it is primary key.
            if (colData.isPrimaryKey)
                // Retrieving the primary key name.
                primaryIdName = columnName;

            // Checking if the column type exist in the map.
            if (javaDataMapper.containsKey(colData.dataType)) {
                // Retrieving data type from map.
                String dataType = javaDataMapper.get(colData.dataType);

                // Checking if the columnType exist in the map.
                if (dbDetails.getCallMap.containsKey(dataType))
                    // Append the value to the callableStatement.
                    sbCode.append(T3).append(tableDoExtension).append(".").append(columnName).append(" = ")
                            .append("rs.").append(dbDetails.getCallMap.get(dataType)).append("(").append("\"")
                            .append(columnName).append("\"").append(");").append(N1);
            }
        }
        sbCode.append(T3).append(tableMapExtension).append(".put(").append(tableDoExtension).append(".")
                .append(primaryIdName).append(", ").append(tableDoExtension).append(");").append(N1);

        sbCode.append(T2).append("}").append(N1);
        sbCode.append(T2).append("return ").append(tableMapExtension).append(";").append(N1);
        sbCode.append(T1).append("}").append(N1);

        // Returning the string to the caller of the function.
        return sbCode.toString();
    }

    /**
     * Function to generate ReadById code for DAL.
     *
     * @return the String of readById code.
     */
    private String generateDALReadById(DbDetails dbDetails) {
        // Defining StringBuilder object which will hold the generated readById code.
        StringBuilder sbCode = new StringBuilder();

        // The extension of the DO and stored procedure.
        String CapsDoExtension = dbDetails.fileTableName + "DO";
        String spExtension = dbDetails.table + "_SP_READBYID";
        String tableDoExtension = dbDetails.table + "DO";

        // Getting columns list from dbDetails.
        columns = dbDetails.columns;

        // Defining a variable.
        String dataType = "";

        sbCode.append("public " + CapsDoExtension + " ").append(dbDetails.table).append("ReadById(");

        // Retrieving the size of primaryKeyMap.
        int sizeOfPrimaryKeyMap = dbDetails.primaryKeyMap.size();
        
        // Checking if the primaryKeyMap size is greater than and equal to 1.
        // If it has primary key.
        if (1 <= sizeOfPrimaryKeyMap) {
            
            // Iterating through the primaryKeyMap.
            for (Map.Entry<String, String> entry : dbDetails.primaryKeyMap.entrySet()) {
                        
                // Retrieving the data type of a column.
                dataType = entry.getValue();

                // Checking if the column data type exist.
                if (javaDataMapper.containsKey(dataType))
                    sbCode.append(javaDataMapper.get(dataType)).append(" ").append(entry.getKey()).append(", ");
            }
            
            sbCode.deleteCharAt(sbCode.length() - 1);
            sbCode.deleteCharAt(sbCode.length() - 1);
        }

        sbCode.append(") throws Exception {").append(N1);

        sbCode.append(T2).append("Connection connection = null;").append(N1);

        sbCode.append(T2).append("connection = getConnectionObj();").append(N1);

        // Retrieving the number of columns.
        int numberOfColumns = sizeOfPrimaryKeyMap;

        // Number of inputs stores the ? which would be asked for call.
        String numberOfInputs = "";
        for (int i = 0; i < numberOfColumns; i++) {
            
            numberOfInputs += "?";
            if (i != numberOfColumns - 1)
                numberOfInputs += ", ";
        }

        sbCode.append(T2).append("CallableStatement callableStatement = connection.prepareCall(\"call ")
                .append(spExtension).append("(").append(numberOfInputs).append(")\");").append(N1);

        // Checking if the primaryKeyMap size is greater than and equal to 1.
        // If it has primary key.
        if (1 <= sizeOfPrimaryKeyMap) {
            
            // Initializing count = 1;
            int count = 1;
            
            // Iterating through the primaryKeyMap.
            for (Map.Entry<String, String> entry : dbDetails.primaryKeyMap.entrySet()) {

                // Retrieving the data type of a column.
                dataType = entry.getValue();
                
                // Checking if the column data type exist.
                if (javaDataMapper.containsKey(dataType))
                    sbCode.append(T2).append("callableStatement.").append(dbDetails.setCallMap.get(javaDataMapper.get(dataType)))
                    .append("(").append(count).append(", ").append(entry.getKey()).append(");").append(N1);
                
                // Incrementing the count.
                count++;             
            }
        }       

        sbCode.append(T2).append("ResultSet rs = callableStatement.executeQuery();").append(N1);

        sbCode.append(T2).append(CapsDoExtension).append(" ").append(tableDoExtension).append(" = ").append("new ")
                .append(CapsDoExtension).append("();").append(N1);

        sbCode.append(T2).append("while (rs.next()) {").append(N1);

        // Iterating through the columns of the table.
        for (String column : columns) {

            // Retrieving the columnMetaData of the particular column.
            AtumColumnMetaData colData = internalObjGraph.dbMap.get(dbDetails.dbName).get(dbDetails.table).get(column);

            // Retrieving the name of the column.
            String columnName = colData.columnName;

            // Checking if the column type exist in the map.
            if (javaDataMapper.containsKey(colData.dataType)) {
                // Retrieving data type from map.
                dataType = javaDataMapper.get(colData.dataType);

                // Checking if the columnType exist in the map.
                if (dbDetails.getCallMap.containsKey(dataType))
                    // Append the value to the callableStatement.
                    sbCode.append(T3).append(tableDoExtension).append(".").append(columnName).append(" = ")
                            .append("rs.").append(dbDetails.getCallMap.get(dataType)).append("(").append("\"")
                            .append(columnName).append("\"").append(");").append(N1);
            }
        }

        sbCode.append(T2).append("}").append(N1);
        sbCode.append(T2).append("return ").append(tableDoExtension).append(";").append(N1);
        sbCode.append(T1).append("}").append(N1);

        // Returning the string to the caller of the function.
        return sbCode.toString();
    }

    /**
     * Function to generate update code for DAL.
     *
     * @return the String of update code.
     */
    private String generateDALUpdate(DbDetails dbDetails) {
        // Defining StringBuilder object which will hold the generated update code.
        StringBuilder sbCode = new StringBuilder();

        // The extension of the DO and stored procedure.
        String CapsDoExtension = dbDetails.fileTableName + "DO";
        String spExtension = dbDetails.table + "_SP_UPDATE";
        String tableDoExtension = dbDetails.table + "DO";

        sbCode.append("public boolean ").append(dbDetails.table).append("Update(").append(CapsDoExtension).append(" ")
                .append(tableDoExtension).append(") throws Exception {").append(N1);

        sbCode.append(T2).append("Connection connection = null;").append(N1);

        sbCode.append(T2).append("connection = getConnectionObj();").append(N1);

        // Getting columns list from dbDetails.
        columns = dbDetails.columns;

        // Retrieving the number of columns.
        int numberOfColumns = columns.size();

        // Number of inputs stores the ? which would be asked for call.
        String numberOfInputs = "";
        for (int i = 0; i < numberOfColumns; i++) {
            numberOfInputs += "?";
            numberOfInputs += ", ";
        }        

        numberOfInputs = numberOfInputs.substring(0, numberOfInputs.length() - 2);

        sbCode.append(T2).append("CallableStatement callableStatement = connection.prepareCall(\"call ")
                .append(spExtension).append("(").append(numberOfInputs).append(")\");").append(N1);
        
        // Initializing count = 1;
        int count = 1;

        // Iterating through the columns of the table.
        for (String column : columns) {

            // Retrieving the columnMetaData of the particular column.
            AtumColumnMetaData colData = internalObjGraph.dbMap.get(dbDetails.dbName).get(dbDetails.table).get(column);

            // Checking if the column type exist in the map.
            if (javaDataMapper.containsKey(colData.dataType)) {

                // Retrieving data type from map.
                String dataType = javaDataMapper.get(colData.dataType);

                // Checking if the columnType exist in the map.
                if (dbDetails.setCallMap.containsKey(dataType))

                    // Append the value to the callableStatement.
                    sbCode.append(T2).append("callableStatement.").append(dbDetails.setCallMap.get(dataType))
                            .append("(").append(count).append(", ").append(tableDoExtension).append(".")
                            .append(colData.columnName).append(");").append(N1);
            }

            // Incrementing count by 1.
            count++;
        }
        sbCode.append(T2).append("int i = callableStatement.executeUpdate(); ").append(N1);
        sbCode.append(T2).append("return 0 != i;").append(N1);

        // End of generated function.
        sbCode.append(T1).append("}").append(N1);

        // Returning the string to the caller of the function.
        return sbCode.toString();
    }

    /**
     * Function to generate delete code for DAL.
     *
     * @return the String of delete code.
     */
    private String generateDALDelete(DbDetails dbDetails) {
        // Defining StringBuilder object which will hold the generated delete code.
        StringBuilder sbCode = new StringBuilder();

        // The extension of the DO and stored procedure.
        String spExtension = dbDetails.table + "_SP_DELETE";

        String dataType = "";

        sbCode.append("public boolean ").append(dbDetails.table).append("Delete(");

        // Getting columns list from dbDetails.
        columns = dbDetails.columns;

        // Retrieving the size of primaryKeyMap.
        int sizeOfPrimaryKeyMap = dbDetails.primaryKeyMap.size();
        
        // Checking if the primaryKeyMap size is greater than and equal to 1.
        // If it has primary key.
        if (1 <= sizeOfPrimaryKeyMap) {
            
            // Iterating through the primaryKeyMap.
            for (Map.Entry<String, String> entry : dbDetails.primaryKeyMap.entrySet()) {
                        
                // Retrieving the data type of a column.
                dataType = entry.getValue();

                // Checking if the column data type exist.
                if (javaDataMapper.containsKey(dataType))
                    sbCode.append(javaDataMapper.get(dataType)).append(" ").append(entry.getKey()).append(", ");
            }
            
            sbCode.deleteCharAt(sbCode.length() - 1);
            sbCode.deleteCharAt(sbCode.length() - 1);
        }

        sbCode.append(") throws Exception {").append(N1);

        sbCode.append(T2).append("Connection connection = null;").append(N1);

        sbCode.append(T2).append("connection = getConnectionObj();").append(N1);

        // Retrieving the number of columns.
        int numberOfColumns = sizeOfPrimaryKeyMap;

        // Number of inputs stores the ? which would be asked for call.
        String numberOfInputs = "";
        for (int i = 0; i < numberOfColumns; i++) {
            
            numberOfInputs += "?";
            if (i != numberOfColumns - 1)
                numberOfInputs += ", ";
        }

        sbCode.append(T2).append("CallableStatement callableStatement = connection.prepareCall(\"call ")
                .append(spExtension).append("(").append(numberOfInputs).append(")\");").append(N1);

        // Checking if the primaryKeyMap size is greater than and equal to 1.
        // If it has primary key.
        if (1 <= sizeOfPrimaryKeyMap) {
            
            // Initializing count = 1;
            int count = 1;
            
            // Iterating through the primaryKeyMap.
            for (Map.Entry<String, String> entry : dbDetails.primaryKeyMap.entrySet()) {

                // Retrieving the data type of a column.
                dataType = entry.getValue();
                
                // Checking if the column data type exist.
                if (javaDataMapper.containsKey(dataType))
                    sbCode.append(T2).append("callableStatement.").append(dbDetails.setCallMap.get(javaDataMapper.get(dataType)))
                    .append("(").append(count).append(", ").append(entry.getKey()).append(");").append(N1);
                
                // Incrementing the count.
                count++;             
            }
        } 

        sbCode.append(T2).append("int i = callableStatement.executeUpdate();").append(N1);
        sbCode.append(T2).append("return 0 != i;").append(N1);

        // End of generated function.
        sbCode.append(T1).append("}");

        // Returning the string to the caller of the function.
        return sbCode.toString();
    }

    /**
     * The below method will separate primary key columns from the columns of
     * the table and store it in the HashMap of type <String, String>.
     * 
     * @param dbDetails is the paramObject which contains the database details.
     * 
     * @return the HashMap of type <String, String> where key is column name and
     *         value is data type of primary key column.
     */
    private HashMap<String, String> getPrimaryKeyMap(DbDetails dbDetails) {

        // Instantiating a HashMap whose key would be column name and
        // value would be data type type.
        HashMap<String, String> primaryKeyMap = new HashMap<>();

        // Retrieving column list from dbDetails.
        columns = dbDetails.columns;

        // Iterating through the columns of the table.
        for (String column : columns) {

            // Retrieving the columnMetaData of the particular column.
            AtumColumnMetaData colData = internalObjGraph.dbMap.get(dbDetails.dbName).get(dbDetails.table).get(column);

            // Checking if the column is a primary key.
            if (colData.isPrimaryKey)
                
                // Putting key as columnName and value as dataType in primaryKeyMap.
                primaryKeyMap.put(colData.columnName, colData.dataType);
        }

        // Returning the primaryKey to the caller.
        return primaryKeyMap;
    }

    /**
     * The below method is used to initialize the java Data mapper
     * where key is datatype in mysql and value is the datatype in Java.
     * 
     * @return a HashMap of <String, String>.
     */
    private static HashMap<String, String> initJavaMap() {

        // Instantiating javaMap map where key is datatype in mysql and
        // value is the datatype in Java.
        HashMap<String, String> javaMap = new HashMap<>();

        javaMap.put("tinyint", "int");
        javaMap.put("smallint", "int");
        javaMap.put("mediumint", "int");
        javaMap.put("int", "int");
        javaMap.put("bigint", "long");
        javaMap.put("bit", "boolean");
        javaMap.put("numeric", "float");
        javaMap.put("decimal", "float");
        javaMap.put("float", "float");
        javaMap.put("double", "double");
        javaMap.put("char", "String");
        javaMap.put("varchar", "String");
        javaMap.put("binary", "byte[]");
        javaMap.put("varbinary", "byte[]");
        javaMap.put("tinyblob", "java.sql.Blob");
        javaMap.put("blob", "java.sql.Blob");
        javaMap.put("mediumblob", "java.io.InputStream");
        javaMap.put("longblob", "java.io.InputStream");
        javaMap.put("tinytext", "String");
        javaMap.put("text", "String");
        javaMap.put("mediumtext", "String");
        javaMap.put("longtext", "String");
        javaMap.put("enum", "String");
        javaMap.put("set", "String");
        javaMap.put("date", "java.sql.Date");
        javaMap.put("datetime", "java.sql.Timestamp");
        javaMap.put("timestamp", "java.sql.Timestamp");
        javaMap.put("time", "java.sql.Time");
        javaMap.put("year", "int");
        javaMap.put("geometry", "java.io.InputStream");

        // Returning javaMap to the caller.
        return javaMap;
    }

    /**
     * Function to retrieve a map which will be used for setting values.
     *
     * @return a map where key is the data type in java and value would be the set
     *         function based on that data type.
     */
    private HashMap<String, String> retrieveSetCallMap() {

        // Creating a map which will store data type in java as key and set function as
        // value.
        HashMap<String, String> setMap = new HashMap<>();

        setMap.put("int", "setInt");
        setMap.put("String", "setString");
        setMap.put("long", "setLong");
        setMap.put("double", "setDouble");
        setMap.put("float", "setFloat");
        setMap.put("java.sql.Date", "setDate");
        setMap.put("Year", "setDate");
        setMap.put("short", "setShort");
        setMap.put("boolean", "setBoolean");
        setMap.put("Array", "setArray");
        setMap.put("byte[]", "setBytes");
        setMap.put("byte", "setByte");
        setMap.put("java.sql.Blob", "setBlob");
        setMap.put("java.io.InputStream", "setBlob");
        setMap.put("Reader", "setClob");
        setMap.put("java.sql.Time", "setTime");
        setMap.put("java.sql.Timestamp", "setTimestamp");

        // Returning the map to the caller.
        return setMap;
    }

    /**
     * Function to retrieve a map which will be used for getting values.
     *
     * @return a map where key is the data type in java and value would be the get
     *         function based on that data type.
     */
    private HashMap<String, String> retrieveGetCallMap() {

        // Creating a map which will store data type in java as key and get function as
        // value.
        HashMap<String, String> getMap = new HashMap<>();

        getMap.put("int", "getInt");
        getMap.put("String", "getString");
        getMap.put("long", "getLong");
        getMap.put("double", "getDouble");
        getMap.put("float", "getFloat");
        getMap.put("java.sql.Date", "getDate");
        getMap.put("Year", "getDate");
        getMap.put("short", "getShort");
        getMap.put("boolean", "getBoolean");
        getMap.put("Array", "getArray");
        getMap.put("byte[]", "getBytes");
        getMap.put("byte", "getByte");
        getMap.put("java.sql.Blob", "getBlob");
        getMap.put("java.io.InputStream", "getBlob");
        getMap.put("Reader", "getClob");
        getMap.put("java.sql.Time", "getTime");
        getMap.put("java.sql.Timestamp", "getTimestamp");

        // Returning the map to the caller.
        return getMap;
    }
}
