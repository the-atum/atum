### Summary

Summarize the bug encountered briefly.

### Expected behavior

Summarize the correct working briefly.

---

#### Steps to reproduce

1. How one can reproduce the issue.
2. List them in steps.
3. In the order you first produced the bug.

#### Bug behavior _(tick applicable)_

- [ ] Reproduceable Consistently
    - `(If not Reproducable Consistently)` Mention pattern, if any, due to which the bug occurs.
- [ ] Reproduceable on Mobile App _(Mention UI Screen / Route / Path on which bug occurs)_
- [ ] Reproduceable on Web App _(Mention UI Screen / Route / Path on which bug occurs)_
- [ ] Backend Issue _(Mention API / DB Details if known, along with screenshot of `POSTMAN API Response`)_

#### Bug Logs/Screenshots

- `(If applicable)` Paste any relevant logs/code-snippet/etc. _(use code blocks (```) to format the pasted text)_
- `(If applicable)` Paste code screenshot(s) or video(s) / link(s) to screenshot(s) or video(s) here to display the bug
- `(If applicable)` Paste or link any other artifacts/files/scripts which may be relevant to the bug

---

#### Meta-Data:
- Bug Priority _(tick one)_:
    - [ ] `URGENT`
    - [ ] `HIGH`
    - [ ] `MID`
    - [ ] `LOW`
- Reported By: @`<reported-by>` _(Applicable if reported by someone other than topic creator)_
- Assigned To: @`<developer-to-assign-to>`
- Pull Request: `<link-to-pull-request>`

---